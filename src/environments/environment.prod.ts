export const environment = {
  production: true,
  PAGE_SIZE: 10,
  API_URL: 'http://13.126.176.150:6021/',
  INTEREST_ICON_URL: 'https://roletune-user-bucket.s3.ap-south-1.amazonaws.com/interests/'
};
