export enum CommunityContentStatus {
    open = 'OPEN',
    closed = 'CLOSED'
}

export enum CommunityThreadType {
    discussionThread = 'DT',
    questionAnswer = 'QA'
}
