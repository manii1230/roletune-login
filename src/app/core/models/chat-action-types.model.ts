export enum ChatActionTypes {
    archieve = 'ARCHIEVE',
    delete = 'DELETE',
    mute = 'MUTE',
    report = 'REPORT'
}