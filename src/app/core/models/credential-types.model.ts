export enum CredentialTypes {
  about = 'ABOUT',
  education = 'EDUCATION',
  experience = 'EXPERIENCE',
  certification = 'CERTIFICATION',
  skill = 'SKILL',
  award = 'AWARD',
  patent = 'PATENT',
  project = 'PROJECT',
  publication = 'PUBLICATION'
}

export enum CredentialModes {
  add = 'ADD',
  edit = 'EDIT',
  delete = 'DELETE'
}
