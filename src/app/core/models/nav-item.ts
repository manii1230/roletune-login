export class NavItem {
  constructor(
    private url: string,
    private name: string,
    private active: boolean,
    private children: any[]
  ) {}

  getName = () => this.name;

  getUrl = () => this.url;

  getActive = () => this.active;
}
