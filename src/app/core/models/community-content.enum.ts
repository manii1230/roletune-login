export enum CommunityContentStatus {
  open = 'OPEN',
  closed = 'CLOSED',
  draft = 'DRAFT',
  published = 'PUBLISHED'
}

export enum CommunityContentTypes {
  discussionThread = 'DT',
  questionAnswer = 'QA'
}

export enum CommunityLibraryCategories {
  audioBooks = 'AUDIO-BOOKS',
  books = 'BOOKS',
  blogs = 'BLOGS',
  videos = 'VIDEOS',
  others = 'OTHERS'
}
