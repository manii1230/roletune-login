export enum RoleTypes {
    'siteUser' = 1001,
    'siteModerator' = 1002,
    'siteContentSanitizer' = 1003,
    'siteAdmin' = 1004,
    'communityUser' = 2001,
    'communityModerator' = 2002,
    'communityFounder' = 2003,
    'platformAdmin' = 5000
}

export enum RoleTypeNames {
    '_1001' = 'User',
    '_1002' = 'Site Moderator',
    '_1003' = 'Content Sanitizer',
    '_1004' = 'Site Admin',
    '_2001' = 'Community User',
    '_2002' = 'Community Moderator',
    '_2003' = 'Community Founder',
    '_5000' = 'Platform Admin'
}