export enum StatusTypes {
  approved = 'APPROVED',
  pending = 'PENDING',
  ignored = 'IGNORED',
  delete = 'DELETED',
  unfriend = 'UNFRIEND'
}
