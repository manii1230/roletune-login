export enum RequestTypes {
  approved = 'APPROVED',
  pending = 'PENDING',
  ignored = 'IGNORED',
  delete = 'DELETED',
  unfriend = 'UNFRIEND'
}
