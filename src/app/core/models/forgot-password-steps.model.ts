export enum ForgotPasswordSteps {
    submitEmail = 'SUBMIT_EMAIL',
    verifyCode = 'VERIFY_CODE',
    submitPhone = 'SUBMIT_PHONE',
    verifyOtp = 'VERIFY_OTP',
    updatePassword = 'UPDATE_PASSWORD'
}