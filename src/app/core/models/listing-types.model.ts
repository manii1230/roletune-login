export enum ListingTypes {
    chat = 'CHAT',
    timeline = 'TIMELINE',
    message = 'MESSAGE'
}
