export enum CommunityViewType {
    guestUser = 'GUEST_USER',
    communityUser = 'COMMUNITY_USER'
}
