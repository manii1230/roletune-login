export enum ReactionTypes {
  upVote = 'UPVOTE',
  downVote = 'DOWNVOTE',
  share = 'SHARE',
  hide = 'HIDE',
  save = 'SAVE',
  bookmark = 'BOOKMARK',
  report = 'REPORT'
}
