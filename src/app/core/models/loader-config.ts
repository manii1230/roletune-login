export interface LoaderConfig {
  loading?: boolean;
  loadmore?: boolean;
  scrollPercent?: number;
}
