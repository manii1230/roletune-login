export enum ModalActionTypes {
    add = 'ADD',
    edit = 'EDIT',
    delete = 'DELETE'
}
