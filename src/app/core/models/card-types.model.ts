export enum CardTypes {
  pendingRequests,
  ignoredRequests,
  approvedRequests
}
