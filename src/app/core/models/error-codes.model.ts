export enum ErrorCodes {
    notFound = 404,
    badRequest = 400,
    forbidden = 403,
    internalServerError = 503
}