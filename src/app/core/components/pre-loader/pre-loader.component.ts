import { Component, OnInit, Input } from '@angular/core';
import { PreLoaderConfig } from '../../config/';
import { LoaderTypes } from './../../../shared/models/';

@Component({
  selector: 'app-pre-loader',
  templateUrl: './pre-loader.component.html',
  styleUrls: ['./pre-loader.component.css']
})
export class PreLoaderComponent implements OnInit {
  @Input() loaderType: LoaderTypes = LoaderTypes.medium;
  constructor() { }

  ngOnInit() {
  }

  getStyles() {
    switch (this.loaderType) {
      case LoaderTypes.small:
        return PreLoaderConfig[LoaderTypes.small];
      case LoaderTypes.medium:
        return PreLoaderConfig[LoaderTypes.medium];
      case LoaderTypes.large:
        return PreLoaderConfig[LoaderTypes.large];
    }
  }
}
