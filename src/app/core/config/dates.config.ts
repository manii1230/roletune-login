import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
const today = new Date();

export const todayConfig: NgbDateStruct = {
  year: today.getFullYear(),
  month: today.getMonth() + 1,
  day: today.getDate()
};

export enum DateTypes {
  availableFromDate,
  unavailableFromDate,
  unavailableToDate
}
