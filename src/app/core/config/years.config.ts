export const getYears = (): number[] => {
    const endYear: number = new Date().getFullYear();
    const startYear: number = endYear - 100;
    const yearList: number[] = [];
    for (let i = startYear; i <= endYear; i++) {
      yearList.push(i);
    }
    return yearList;
};
