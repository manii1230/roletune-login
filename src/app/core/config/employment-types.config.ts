export const employmentTypes = [
    {
        label: 'Full-time',
        value: 'FULLTIME'
    },
    {
        label: 'Part-time',
        value: 'PARTTIME'
    },
    {
        label: 'Self-employed',
        value: 'SELFEMPLOYED'
    },
    {
        label: 'Freelance',
        value: 'FREELANCE'
    },
    {
        label: 'Contract',
        value: 'CONTRACT'
    },
    {
        label: 'Internship',
        value: 'INTERNSHIP'
    },
    {
        label: 'Apprenticeship',
        value: 'APPRENTICESHIP'
    }
];
