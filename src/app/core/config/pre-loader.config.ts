import { LoaderTypes } from './../../shared/models/loader-types.enum';
export const PreLoaderConfig = {
  [LoaderTypes.small] : {
    width: LoaderTypes.small,
    height: LoaderTypes.small
  },
  [LoaderTypes.medium] : {
    width: LoaderTypes.medium,
    height: LoaderTypes.medium
  },
  [LoaderTypes.large] : {
    width: LoaderTypes.large,
    height: LoaderTypes.large
  }
};

