import { environment } from './../../../environments/environment';
export interface LoadMoreConfig {
    loadMore: boolean;
    loading: boolean;
}

export class LoadMore {
    config: LoadMoreConfig;
    pageSize: number;
    pageNumber: number;

    constructor() {
        this.config = {
            loadMore: true,
            loading: false
        };
        this.pageSize = environment.PAGE_SIZE;
        this.pageNumber = 0;
    }
}
