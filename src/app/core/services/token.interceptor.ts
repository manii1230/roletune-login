import { CookieService } from 'ngx-cookie-service';
import { environment } from './../../../environments/environment';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  jwtHelperService: JwtHelperService;
  constructor(private cookieService: CookieService, private router: Router) {
    this.jwtHelperService = new JwtHelperService();
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.search(environment.API_URL) === 0) {
      return this.handleApiRequest(request, next);
    } else {
      return next.handle(request);
    }
  }



  handleApiRequest(request, next) {
    // const token = sessionStorage.getItem('token');
    const token = this.cookieService.get('token');
    if (this.jwtHelperService.isTokenExpired(token)) {
      this.router.navigate(['auth/login']);
      return throwError(new Error('Session Expired'));
    }
    request = token ? request.clone({ setHeaders: { Authorization: `Bearer ${token}` }}) : request;

    const handler = next.handle(request).pipe(
      catchError((error, caught) => {
        if ([400, 401, 403, 404, 417].includes(error.status)) {
          // sessionStorage.clear();
          return throwError(error);
        }
        this.cookieService.deleteAll();
        this.router.navigate(['auth/login']);
        return throwError(error);
      })
    );

    return handler;
  }
}
