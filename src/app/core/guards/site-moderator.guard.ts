import { RoleTypes } from '../models/role-types.model';
import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, CanLoad } from '@angular/router';
import { AppService } from 'src/app/app.service';

@Injectable({
  providedIn: 'root'
})
export class SiteModeratorGuard implements CanActivateChild, CanActivate, CanLoad {
  constructor(private router: Router, private appService: AppService) {
  }

  canActivateChild() {
    try {
      const user = this.appService.getLoggedInUserDetails();
      if (user && user.roles && user.roles.includes(RoleTypes.siteModerator)) {
        return true;
      }

      this.router.navigate(['error/403']);
      return false;
    } catch (ex) {
      this.router.navigate(['error/503']);
      return false;
    }
  }

  canActivate() {
    try {
      const user = this.appService.getLoggedInUserDetails();
      if (user && user.roles && user.roles.includes(RoleTypes.siteModerator)) {
        return true;
      }
      this.router.navigate(['error/403']);
      return false;
    } catch (ex) {
      this.router.navigate(['error/503']);
      return false;
    }
  }

  canLoad() {
    try {
      const user = this.appService.getLoggedInUserDetails();
      if (user && user.roles && user.roles.includes(RoleTypes.siteModerator)) {
        return true;
      }
      this.router.navigate(['error/403']);
      return false;
    } catch (ex) {
      this.router.navigate(['error/503']);
      return false;
    }
  }
}
