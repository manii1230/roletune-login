import { CommunitiesService } from '../../features/communities/communities.service';
import { RoleTypes } from '../models/role-types.model';
import { Injectable } from '@angular/core';
import { CanActivateChild, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommunityModeratorGuard implements CanActivateChild {
  constructor(private router: Router, private communitiesService: CommunitiesService) {}
  canActivateChild() {
    try {
      const communityUser = this.communitiesService.activeCommunity.getValue();
      if (communityUser && communityUser.role && communityUser.role.includes(RoleTypes.communityModerator)) {
        return true;
      }

      this.router.navigate(['error/403']);
      return false;
    } catch (ex) {
      this.router.navigate(['error/503']);
      return false;
    }
  }
}
