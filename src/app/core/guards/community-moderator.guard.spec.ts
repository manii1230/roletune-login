import { TestBed, async, inject } from '@angular/core/testing';

import { CommunityModeratorGuard } from './community-moderator.guard';

describe('CommunityModeratorGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommunityModeratorGuard]
    });
  });

  it('should ...', inject([CommunityModeratorGuard], (guard: CommunityModeratorGuard) => {
    expect(guard).toBeTruthy();
  }));
});
