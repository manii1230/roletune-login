import { TestBed, async, inject } from '@angular/core/testing';

import { SiteModeratorGuard } from './site-moderator.guard';

describe('SiteModeratorGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteModeratorGuard]
    });
  });

  it('should ...', inject([SiteModeratorGuard], (guard: SiteModeratorGuard) => {
    expect(guard).toBeTruthy();
  }));
});
