import { TestBed, async, inject } from '@angular/core/testing';

import { SiteAdminGuard } from './site-admin.guard';

describe('SiteAdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SiteAdminGuard]
    });
  });

  it('should ...', inject([SiteAdminGuard], (guard: SiteAdminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
