import { TestBed, async, inject } from '@angular/core/testing';

import { PlatformAdminGuard } from './platform-admin.guard';

describe('PlatformAdminGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlatformAdminGuard]
    });
  });

  it('should ...', inject([PlatformAdminGuard], (guard: PlatformAdminGuard) => {
    expect(guard).toBeTruthy();
  }));
});
