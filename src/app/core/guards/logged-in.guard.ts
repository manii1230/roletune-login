import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';
import { Router, CanActivateChild } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class LoggedInGuard implements CanActivateChild {
  jwtHelperService: JwtHelperService;
  constructor(private router: Router, private cookieService: CookieService) {
    this.jwtHelperService = new JwtHelperService();
  }

  canActivateChild() {
    const token = this.cookieService.get('token');
    if (token && !this.jwtHelperService.isTokenExpired(token)) {
      this.router.navigate(['main/home']);
      return false;
    }
    return true;
  }
}
