import { RoleTypes } from '../models/role-types.model';
import { AppService } from 'src/app/app.service';
import { Injectable } from '@angular/core';
import { Router, CanActivateChild, CanLoad } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SiteAdminGuard implements CanActivateChild, CanLoad {
  constructor(private router: Router, private appService: AppService) {
  }

  canActivateChild() {
    try {
      const user = this.appService.getLoggedInUserDetails();
      if (user && user.roles && user.roles.includes(RoleTypes.siteAdmin)) {
        return true;
      }

      this.router.navigate(['error/403']);
      return false;
    } catch (ex) {
      this.router.navigate(['error/503']);
      return false;
    }
  }

  canLoad() {
    try {
      const user = this.appService.getLoggedInUserDetails();
      if (user && user.roles && user.roles.includes(RoleTypes.siteAdmin)) {
        return true;
      }
      this.router.navigate(['error/403']);
      return false;
    } catch (ex) {
      this.router.navigate(['error/503']);
      return false;
    }
  }
}
