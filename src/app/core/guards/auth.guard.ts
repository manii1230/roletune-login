import { CookieService } from 'ngx-cookie-service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate, CanActivateChild } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {
  jwtHelperService: JwtHelperService;
  constructor(private router: Router, private cookieService: CookieService) {
    this.jwtHelperService = new JwtHelperService();
  }

  canActivate() {
    return true;
  }

  canActivateChild() {
    //const token = sessionStorage.getItem('token');
    const token = this.cookieService.get('token');
    if (token && !this.jwtHelperService.isTokenExpired(token)) {
      return true;
    } else {
      this.router.navigate(['/auth/login']);
      return false;
    }
  }
}
