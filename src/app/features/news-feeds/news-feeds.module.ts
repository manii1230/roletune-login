import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsFeedsRoutingModule } from './news-feeds-routing.module';
import { NewsFeedsComponent } from './news-feeds.component';
import { FrontModule } from '../front.module';

@NgModule({
  declarations: [NewsFeedsComponent],
  imports: [
    CommonModule,
    NewsFeedsRoutingModule,
    FrontModule,
    SharedModule
  ]
})
export class NewsFeedsModule { }
