import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsFeedsComponent } from './news-feeds.component';

const routes: Routes = [
  {
    path: '',
    component: NewsFeedsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsFeedsRoutingModule { }
