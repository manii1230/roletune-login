import { environment } from './../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  getUsers(payload: any) {
    const { pageNumber = 1, pageSize = environment.PAGE_SIZE, isModerator, isAdmin } = payload;
    let params = new HttpParams();
    params = params.set('pageNumber', pageNumber.toString());
    params = params.set('pageSize', pageSize.toString());
    if (isModerator) {
      params = params.set('isModerator', isModerator.toString());
    }
    if (isAdmin) {
      params = params.set('isAdmin', isAdmin.toString());
    }
    return this.http.get(`${environment.API_URL}admin-service/users`, { params });
  }

  updateToModerator(payload: any) {
    return this.http.patch(`${environment.API_URL}admin-service/users/${payload.username}`, payload);
  }

  deleteUser(payload: any) {
    return this.http.delete(`${environment.API_URL}admin-service/users/${payload.username}`);
  }

  getCommunityList() {
    return this.http.get(`${environment.API_URL}community-service/all-communities`);
  }

  getCommunityUsers(payload: any) {
    const { pageNumber = 1, pageSize = environment.PAGE_SIZE, communityId } = payload;
    let params = new HttpParams();
    params = params.set('pageNumber', pageNumber);
    params = params.set('pageSize', pageSize);

    return this.http.get(`${environment.API_URL}admin-service/community-users/${communityId}`, { params });
  }

  updateCommunityUserRole(payload: any) {
    const { communityId } = payload;
    return this.http.patch(`${environment.API_URL}admin-service/community-users/${communityId}`, payload);
  }
}
