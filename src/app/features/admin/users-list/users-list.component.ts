import { Router } from '@angular/router';
import { AppService } from './../../../app.service';
import { RoleTypes, RoleTypeNames } from './../../../core/models/role-types.model';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { AdminService } from './../admin.service';
import { LoadMore } from './../../../core/base/load-more.base';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent extends LoadMore implements OnInit, OnDestroy {
  users: any[];
  message: any;
  unsubscribe = new Subject();

  constructor(private adminService: AdminService, private appService: AppService, private router: Router) {
    super();
  }

  ngOnInit() {
    this.message = null;
    this.users = [];
    this.pageNumber = 1;
    this.loadUsers();
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  loadUsers() {
    this.config.loading = true;
    const payload = {
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };
    this.adminService.getUsers(payload).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.config.loadMore = !(res.result && res.result.length < this.pageSize);;
        this.users = this.users.concat(res.result);
      },
      (err: HttpErrorResponse) => {
        this.config.loading = false;
        this.config.loadMore = false;
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : err.message
        };
      }
    );
  }

  isSiteModerator(user) {
    return user.roles && user.roles.includes(RoleTypes.siteModerator);
  }

  getUserRoles(roles = []) {
    return roles.map(role => {
      return RoleTypeNames[`_${role}`];
    }).join(', ');
  }

  makeModerator(user) {
    const payload = {
      username: user.username,
      roles: user.roles.concat([RoleTypes.siteModerator])
    };
    this.adminService.updateToModerator(payload).subscribe(
      (res: any) => {
        user.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        this.appService.removeMessage(user, 'message');
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : err.message
        };
      }
    );
  }

  deleteUser(user: any, idx: number): void {
    const payload = {
      username: user.username
    };
    this.adminService.deleteUser(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          this.users.splice(idx, 1);
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : err.message
        };
      }
    );
  }
}
