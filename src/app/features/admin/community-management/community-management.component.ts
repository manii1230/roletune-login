import { RoleTypes } from './../../../core/models/role-types.model';
import { AppService } from './../../../app.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AdminService } from './../admin.service';
import { LoadMore } from './../../../core/base/load-more.base';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-community-management',
  templateUrl: './community-management.component.html',
  styleUrls: ['./community-management.component.css'],
})
export class CommunityManagementComponent extends LoadMore implements OnInit {
  message: any;
  communities: any[];
  selectedCommunity: string;
  communityUsers: any[];
  showResults: boolean;

  constructor(
    private adminService: AdminService,
    private appService: AppService,
    private router: Router
  ) {
    super();
  }

  ngOnInit() {
    this.selectedCommunity = null;
    this.showResults = false;
    this.message = null;
    this.communities = [];
    this.communityUsers = [];
    this.loadCommunities();
  }

  loadCommunities() {
    this.config.loading = true;
    this.adminService.getCommunityList().subscribe(
      (res: any) => {
        this.communities = res.result;
        this.config.loading = false;
      },
      (err: HttpErrorResponse) => {
        (this.config.loading = false), (this.communities = []);
        this.message = {
          status: false,
          message:
            err.error && err.error.errorMessage
              ? err.error.errorMessage
              : err.message,
        };
      }
    );
  }

  loadCommunityUsers() {
    this.config.loading = true;
    const payload = {
      communityId: this.selectedCommunity,
      pageSize: this.pageSize,
      pageNumber: this.pageNumber,
    };
    this.adminService.getCommunityUsers(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage,
        };
        this.showResults = true;
        this.communityUsers = this.communityUsers.concat(res.result);
        this.config.loading = false;
        this.config.loadMore = !(
          res.result && res.result.length < this.pageSize
        );
        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        (this.config.loading = false), (this.config.loadMore = false);
        this.showResults = true;
        this.communityUsers = [];
        this.message = {
          status: false,
          message:
            err.error && err.error.errorMessage
              ? err.error.errorMessage
              : err.message,
        };
      }
    );
  }

  isCommunityModerator(flag: boolean, user: any): boolean {
    return flag
      ? !(user.role as any[]).includes(RoleTypes.communityModerator)
      : (user.role as any[]).includes(RoleTypes.communityModerator);
  }

  updateModerator(flag: boolean, user: any, index: number) {
    console.log(flag, user, index);
    const payload = {
      flag,
      userId: user.userId,
      role: user.role,
      communityId: this.selectedCommunity,
    };
    this.adminService.updateCommunityUserRole(payload).subscribe(
      (res: any) => {
        console.log(res.result);
        user.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage,
        };
        if (res.success) {
          user.role = res.result.value
            ? flag
              ? user.role.concat([RoleTypes.communityModerator])
              : user.role.filter(
                  (userRole) => userRole !== RoleTypes.communityModerator
                )
            : user.role;
          this.appService.removeMessage(user, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        user.message = {
          status: false,
          message:
            err.error && err.error.errorMessage
              ? err.error.errorMessage
              : err.message,
        };
      }
    );
  }
}
