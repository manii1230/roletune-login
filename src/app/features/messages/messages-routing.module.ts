import { FolderComponent } from './folder/folder.component';
import { ComposeComponent } from './compose/compose.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { MessagesComponent } from './messages.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: MessagesComponent,
    children: [
      {
        path: '',
        redirectTo: 'inbox',
        pathMatch: 'full'
      },
      {
        path: ':folder',
        component: FolderComponent
      },
      {
        path: ':folder/:messageId',
        component: FolderComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRoutingModule { }
