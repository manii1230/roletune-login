import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-messages-sidebar',
  templateUrl: './messages-sidebar.component.html',
  styleUrls: ['./messages-sidebar.component.css']
})
export class MessagesSidebarComponent implements OnInit {
  showMessagesSidebar: boolean;
  constructor(private router: Router) { }

  ngOnInit() {
    this.showMessagesSidebar = this.router.url.indexOf('message-details') >= 0;
  }

  showComposeForm() {
    document.getElementById('compose-tab').click();
  }
}
