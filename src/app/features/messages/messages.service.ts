import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  private folder: BehaviorSubject<string>;
  public folderObservable: Observable<string>;

  constructor(private http: HttpClient) {
    this.folder = new BehaviorSubject('');
    this.folderObservable = this.folder.asObservable();
  }

  updateFolder(folder: string) {
    this.folder.next(folder);
  }

  getMessages(folder: string, page: number, size: number) {
    return this.http.get(`${environment.API_URL}internet-service/user-messages/${folder}?pageNumber=${page}&pageSize=${size}`);
  }

  fetchMessages() {
    return this.http.get(`${environment.API_URL}internet-service/internet-user`);
  }

  fetchMessageDetails(id: string) {
    return this.http.get(`${environment.API_URL}internet-service/internet-user/${id}`);
  }

  sendMessage(message: any) {
    if (message.attachFile) {
      const blogFormData = new FormData();
      for (const key in message) {
        if (message.hasOwnProperty(key)) {
          blogFormData.append( key, message[key]);
        }
      }
      // message.receiverIds = message.receiverIds.join(',');
      message = blogFormData;
    }
    return this.http.post(`${environment.API_URL}internet-service/internet-user`, message);
  }

  updateMessage(message: any) {
    message.emailObjId = message._id;
    return this.http.patch(`${environment.API_URL}internet-service/internet-user`, message);
  }

  deleteMessage(message: any) {
    return this.http.delete(`${environment.API_URL}internet-service/internet-user/${message._id}`, message);
  }
}
