import { AppService } from './../../app.service';
import { MessagesService } from './messages.service';
import { NavItem } from './../../core/models/nav-item';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  constructor() { }

  ngOnInit() {}
}
