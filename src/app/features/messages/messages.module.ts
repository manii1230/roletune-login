import { SharedModule } from './../../shared/shared.module';
import { MessagesService } from './messages.service';
import { MessagesComponent } from './messages.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessagesRoutingModule } from './messages-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FolderComponent } from './folder/folder.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { ComposeComponent } from './compose/compose.component';
import { MessagesSidebarComponent } from './messages-sidebar/messages-sidebar.component';

@NgModule({
  declarations: [
    MessagesComponent,
    FolderComponent,
    MessageDetailsComponent,
    ComposeComponent,
    MessagesSidebarComponent
  ],
  imports: [
    CommonModule,
    MessagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
    MessagesService
  ]
})
export class MessagesModule { }
