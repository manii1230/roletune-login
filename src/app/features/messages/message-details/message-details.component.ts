import { HttpErrorResponse } from '@angular/common/http';
import { MessageTypes } from './../../../core/models/message-types';
import { Router } from '@angular/router';
import { MessagesService } from './../messages.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.component.html',
  styleUrls: ['./message-details.component.css']
})
export class MessageDetailsComponent implements OnInit, OnChanges {
  @Output() messageOperation = new EventEmitter();
  @Input() user: any;
  @Input() message: any;
  replyForm: FormGroup;
  compose: boolean;
  displayMessage: any;
  showDropdown: boolean;
  userMessageAction: boolean;
  submitted: boolean;

  // tslint:disable-next-line:max-line-length
  constructor(
    private formBuilder: FormBuilder,
    private messagesService: MessagesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userMessageAction = false;
    this.displayMessage = null;
    this.submitted = false;
    this.showDropdown = false;
    this.replyForm = this.formBuilder.group({
      createdAt: [null],
      receiverIds: ['', [Validators.required, Validators.minLength(4)]],
      cc: [''],
      subject: ['', [Validators.required, Validators.minLength(2)]],
      message: ['', [Validators.required, Validators.minLength(2)]],
      attachment: [''],
      attachmentName: ['']
    });
  }

  ngOnChanges() {
    this.userMessageAction = null;
    this.submitted = false;
    this.showDropdown = false;
    this.displayMessage = null;
  }

  get attachment() {
    return this.replyForm.get('attachment');
  }

  get attachmentName() {
    return this.replyForm.get('attachmentName');
  }

  get subject() {
    return this.replyForm.get('subject');
  }

  get messageFields() {
    return this.replyForm.get('message');
  }

  get receiverIds() {
    return this.replyForm.get('receiverIds');
  }

  getAttachmentLink() {
    return this.replyForm.value.attachment;
  }

  messageAction(action: string) {
    this.submitted = false;
    this.showDropdown = false;
    this.displayMessage = null;
    this.userMessageAction = true;
    switch (action) {
      case 'reply':
        this.replyForm.patchValue({
          createdAt: new Date(),
          receiverIds: this.message.fromUserId,
          cc: ``,
          subject: (this.message.message.subject.indexOf(`Re:`) === 0) ?
            `${this.message.message.subject}` : `Re: ${this.message.message.subject}`,
          message: `
            \n\n-------------------Reply To Message------------------\n
            On ${this.message.message.createdAt.substring(0, 10)}
            ${(this.message.placeHolder === 'inbox') ? this.message.fromUserId : this.message.userId} wrote:\n
            ${this.message.message.body }
            `,
          attachment: this.message.attachment ? this.message.attachment : null,
          attachmentName: this.message.attachmentName ? this.message.attachmentName : null
        });
        break;
      case 'reply-all':
        const receivers = this.message.toUserId.filter(username => username !== this.user.username);
        receivers.push(this.message.fromUserId);
        const ccs = this.message.cc.filter(username => username !== this.user.username);
        this.replyForm.patchValue({
          createdAt: new Date(),
          receiverIds: `${receivers.join(', ')}`,
          cc: `${ccs.join(', ')}`,
          subject: (this.message.message.subject.indexOf(`Re:`) === 0) ?
            `${this.message.message.subject}` : `Re: ${this.message.message.subject}`,
          message: `
            \n\n-------------------Reply To Message------------------\n
            On ${this.message.message.createdAt.substring(0, 10)}
            ${(this.message.placeHolder === 'inbox') ? this.message.fromUserId : this.message.userId} wrote:\n
            ${this.message.message.body }
          `,
          attachment: this.message.attachment ? this.message.attachment : null,
          attachmentName: this.message.attachmentName ? this.message.attachmentName : null
        });
        break;
      case 'forward':
        this.replyForm.patchValue({
          createdAt: new Date(),
          receiverIds: ``,
          cc: ``,
          subject: (this.message.message.subject.indexOf(`Fw:`) === 0) ?
            `${this.message.message.subject}` : `Fw: ${this.message.message.subject}`,
          message: `
            \n\n-------------------Forwarded Message------------------\n
            Subject: ${this.message.message.subject}\n
            Date: ${this.message.message.createdAt}\n
            From: ${this.message.fromUserId}\n
            To: ${this.message.toUserId.join(', ')}\n
            CC: ${this.message.cc.join(', ')}\n\n
            ${this.message.message.body }
          `,
          attachment: this.message.attachment ? this.message.attachment : null,
          attachmentName: this.message.attachmentName ? this.message.attachmentName : null
        });
        break;
    }
  }

  deleteMessage() {
    this.messagesService.deleteMessage(this.message).subscribe(
      (res: any) => {
        this.displayMessage = {
          status: res.success,
          message: res.success ? res.message : (res.message.errorMessage ? res.message.errorMessage : MessageTypes.emailMessageDeleteFailed)
        };
        if (res.success) {
          setTimeout(() => {
            this.messageOperation.emit({ action: 'delete' });
          }, 3000);
        }
      },
      (err: HttpErrorResponse) => {
        this.displayMessage = {
          status: false,
          message: err.message
        };
      }
    );
  }

  replyToMessage() {
    this.submitted = true;
    if (this.replyForm.invalid) {
      return console.log(this.replyForm.controls);
    }

    this.messagesService.sendMessage(this.replyForm.value).subscribe(
      (res: any) => {
        this.displayMessage = {
          status: res.success,
          message: res.success ? res.message : (res.message.errorMessage ? res.message.errorMessage : MessageTypes.emailMessageSendFailed)
        };
        if (res.success) {
          setTimeout(() => {
            this.router.navigate(['main/messages/'], { skipLocationChange: true }).then(() => {
              this.router.navigate(['main/messages/sent']);
            });
          }, 3000);
        }
      },
      (err: HttpErrorResponse) => {
         this.displayMessage = {
           status: false,
           message: err.message
         };
      }
    );
  }
}
