import { MessageTypes } from './../../../core/models/message-types';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from './../../../../environments/environment';
import { AppService } from './../../../app.service';
import { MessagesService } from './../messages.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NavItem } from './../../../core/models/nav-item';
import { FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.css']
})
export class FolderComponent implements OnInit {
  user: any;
  folder: string;
  tabs: NavItem[];
  messageFilter: string;
  activeMessage: any;

  showCompose: boolean;
  config: any;
  message: any;
  messages: any[];
  pageSize: number;
  pageNumber: number;
  hideMenu: boolean;

  constructor(
    private route: ActivatedRoute,
    private appService: AppService,
    private messagesService: MessagesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.showCompose = false;
    this.config = {
      loadMore: true,
      loading: true
    };
    this.messages = [];
    this.activeMessage = null;
    this.pageSize = environment.PAGE_SIZE;
    this.hideMenu = this.router.url.indexOf('message-details') >= 0;
    this.tabs = [
      new NavItem('/main/messages/inbox', 'Inbox', (this.folder === 'inbox'), []),
      new NavItem('/main/messages/sent', 'Sent', (this.folder === 'sent'), []),
      new NavItem('/main/messages/draft', 'Draft', (this.folder === 'draft'), []),
      new NavItem('/main/messages/trash', 'Trash', (this.folder === 'trash'), []),
    ];
    this.appService.activeLoggedInUser.subscribe(user => {
      this.user = (user && Object.keys(user).length) ? user : null;
      if (this.user) {
        this.route.params.subscribe(param => {
          if (param.folder) {
            this.folder = param.folder;
            this.pageNumber = 1;
            this.messages = [];
            this.activeMessage = null;
            this.showCompose = false;
            this.loadMessages();
          }
        });
      }
    });
  }

  loadMessages() {
    this.config.loading = true;
    this.messagesService.getMessages(this.folder, this.pageNumber, this.pageSize).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.config.loadMore = !(res.result && res.result.length < this.pageSize);
        this.messages = this.messages.concat(res.result);
      },
      (err: HttpErrorResponse) => {
        this.config.loading = false;
        this.config.loadMore = false;
        this.message = {
          status: false,
          message: err.message
        };
    });
  }

  openMessage(idx: number) {
    if (this.messages[idx].isRead) {
      return this.openMessageDetails(idx);
    }
    this.messages[idx].isRead = true;
    this.messages[idx].emailObjId = this.messages[idx]._id;
    this.messagesService.updateMessage(this.messages[idx]).subscribe(
      (res: any) => {
          res.success ? this.openMessageDetails(idx) : null;
      },
      (err: HttpErrorResponse) => {
    });
  }

  openMessageDetails(idx: number) {
    this.activeMessage = JSON.parse(JSON.stringify(this.messages[idx]));
  }

  updateMessage(idx: number) {
    const message = Object.assign({}, this.messages[idx]);
    message.emailObjId = message._id;
    message.isStar = !message.isStar;
    this.messagesService.updateMessage(message).subscribe(
      (res: any) => {
        this.messages[idx] = res.success ? message : this.messages[idx];
        this.message = {
          status: res.success,
          message: res.success ? res.message : (res.message.errorMessage ? res.message.errorMessage : MessageTypes.emailMessageUpdateFailed)
        };
        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {

      }
    );
  }

  handleMessage(evt: any) {
   switch (evt.action) {
    case 'delete':
      const idx = this.messages.findIndex((message: any) => this.activeMessage._id === message._id);
      if (idx >= 0) {
        this.messages.splice(idx, 1);
        this.activeMessage = null;
      }
      break;
   }
  }

  handleShowCompose(event: boolean) {
    this.showCompose = false;
    if (event) {
      this.router.navigate(['main/messages'], { skipLocationChange: true });
      this.router.navigate(['main/messages/sent'], { skipLocationChange: true });
    }
  }
}
