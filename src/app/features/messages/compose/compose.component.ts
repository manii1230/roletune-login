import { MessageTypes } from './../../../core/models/message-types';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { MessagesService } from './../messages.service';
import { AppService } from './../../../app.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-compose',
  templateUrl: './compose.component.html',
  styleUrls: ['./compose.component.css']
})
export class ComposeComponent implements OnInit {
  @Output() showCompose = new EventEmitter();
  @Input() user: any;
  submitted: boolean;
  displayMessage: any;
  composeForm: FormGroup;
  composeDate: Date;

  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    private messagesService: MessagesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.submitted = false;
    this.displayMessage = null;
    this.composeDate = new Date();
    this.composeForm = this.formBuilder.group({
      receiverIds: ['', Validators.required],
      cc: [''],
      subject: ['', [Validators.required, Validators.minLength(2)]],
      message: ['', [Validators.required, Validators.minLength(2)]],
      fileName: [''],
      attachedFile: [''],
      fileType: ['']
    });
  }

  get fileName() {
    return this.composeForm.get('fileName');
  }

  get subject() {
    return this.composeForm.get('subject');
  }

  get message() {
    return this.composeForm.get('message');
  }

  get receiverIds() {
    return this.composeForm.get('receiverIds');
  }

  uploadFile(event: any): void {
    if (event.target && event.target.files[0]) {
      this.composeForm.patchValue({
        fileName: event.target.files[0].name,
        attachedFile: event.target.files[0],
        fileType: event.target.files[0].type
      });
    }
  }

  sendMessage() {
    this.submitted = true;
    if (this.composeForm.invalid) {
      return;
    }
    this.messagesService.sendMessage(this.composeForm.value).subscribe(
      (res: any) => {
        this.displayMessage = {
          status: res.success,
          message: res.success ? res.message : (res.message.errorMessage ? res.message.errorMessage : MessageTypes.emailMessageSendFailed)
        };
        if (res.success) {
          setTimeout(() => {
            this.hideComposeForm(true);
            (
              this.user && this.user.messageSettings && (
                (this.user.messageSettings.openIn === 'W') || (this.user.messageSettings.openIn === 'H')
              )
            ) ? this.router.navigate(['main/message-details/sent']) : this.router.navigate(['main/messages/sent']);
          }, 3000);
        }
      },
      (err: HttpErrorResponse) => {
        this.displayMessage = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
      }
    );
  }

  hideComposeForm(flag: boolean) {
    this.showCompose.emit(flag);
  }
}
