import { AddInterestComponent } from './add-interest/add-interest.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InterestComponent } from './interest.component';
import { RemoveInterestComponent } from './remove-interest/remove-interest.component';
import { InterestListComponent } from './interest-list/interest-list.component';

const routes: Routes = [
  {
    path: '',
    component: InterestComponent,
    children: [
      {
        path: '',
        component: InterestListComponent
      },
      {
        path: 'add-interest',
        component: AddInterestComponent
      },
      {
        path: 'remove-interest',
        component: RemoveInterestComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterestRoutingModule { }
