import { SharedModule } from './../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RemoveInterestComponent } from './remove-interest/remove-interest.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterestRoutingModule } from './interest-routing.module';
import { AddInterestComponent } from './add-interest/add-interest.component';
import { InterestComponent } from './interest.component';
import { InterestListComponent } from './interest-list/interest-list.component';

@NgModule({
  declarations: [
    InterestComponent,
    AddInterestComponent,
    RemoveInterestComponent,
    InterestListComponent
  ],
  imports: [
    CommonModule,
    InterestRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class InterestModule { }
