import { MessageTypes } from './../../../core/models/message-types';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from './../../../app.service';
import { InterestsService } from './../interests.service';
import { Subject, forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-interest-list',
  templateUrl: './interest-list.component.html',
  styleUrls: ['./interest-list.component.css']
})
export class InterestListComponent implements OnInit, OnDestroy {
  filterValue: string;
  communities: any[];
  unsubscribe = new Subject();
  communitiesObservable: any;
  message: any;

  constructor(private interestsService: InterestsService, private appService: AppService) { }

  ngOnInit() {
    this.filterValue = '';
    this.communities = [];
    this.message = null;
    this.loadInterests();
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  loadInterests() {
    this.appService.communityListObservable.subscribe(communities => {
      if (communities && communities.length) {
        this.communitiesObservable = JSON.parse(JSON.stringify(this.setModifiedFlag(communities)));
        this.communities = JSON.parse(JSON.stringify(this.setModifiedFlag(communities)));
      }
    });
  }

  filterSubscriptions(filter: string) {
    this.filterValue = filter;
  }

  updateInterestStatus(interest: any): void {
    this.interestsService.updateInterest(interest).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          this.appService.updateCommunityListObservable(this.communities);
          interest.modified = false;
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  setModifiedFlag(communities: any[]): any[] {
    return communities.map(community => {
      community.modified = false;
      return community;
    });
  }

  cancelChanges() {
    this.communities = JSON.parse(JSON.stringify(this.communitiesObservable));
  }

  saveAll() {
    const communities: any[] = this.communities.filter(community => community.modified);
    const updateObservable = [];
    communities.forEach(community => {
      updateObservable.push(this.interestsService.updateInterest(community));
    });

    forkJoin(updateObservable).subscribe(
      (res: any) => {
        if (res.every(updateResponse => updateResponse.success)) {
          this.communities = this.setModifiedFlag(this.communities);
          this.appService.updateCommunityListObservable(this.communities);
          this.message = {
            status: true,
            message: MessageTypes.interestUpdateSuccess
          };
          this.appService.removeMessage(this, 'message');
        } else {
          this.message = {
            status: false,
            message: MessageTypes.interestUpdateFailed
          };
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  checkDisabled(): number {
    return this.communities.filter(community => community.modified).length;
  }
}
