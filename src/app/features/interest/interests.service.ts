import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InterestsService {

  constructor(private http: HttpClient) {}

  getCommunityList() {
    return this.http.get(`${environment.API_URL}community-service/communities/user`);
  }

  getInterestList() {
    return this.http.get(`${environment.API_URL}interest-graph-service/interest-tree`);
  }

  searchInterest(data: any) {
    return this.http.get(
      `${environment.API_URL}interest-graph-service/search-interest?keyword=${data.keyword}&matchType=${data.matchType}`
    );
  }

  getSubscriptions() {
    return this.http.get(`${environment.API_URL}community-service/community-user`);
  }

  addInterest(interest: any) {
    return this.http.post(`${environment.API_URL}community-service/initial-community`, interest);
  }

  updateInterest(interest: any) {
    return this.http.patch(`${environment.API_URL}community-service/${interest.communityId}/${interest._id}/user`, interest);
  }
}
