import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveInterestComponent } from './remove-interest.component';

describe('RemoveInterestComponent', () => {
  let component: RemoveInterestComponent;
  let fixture: ComponentFixture<RemoveInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveInterestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
