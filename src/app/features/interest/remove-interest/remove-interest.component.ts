import { MessageTypes } from './../../../core/models/message-types';
import { HttpErrorResponse } from '@angular/common/http';
import { AppService } from './../../../app.service';
import { InterestsService } from './../interests.service';
import { takeUntil } from 'rxjs/operators';
import { forkJoin, Subject } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-remove-interest',
  templateUrl: './remove-interest.component.html',
  styleUrls: ['./remove-interest.component.css']
})
export class RemoveInterestComponent implements OnInit, OnDestroy {
  filterValue: string;
  communities: any[];
  unsubscribe = new Subject();
  communitiesObservable: any;

  message: any;

  constructor(private interestsService: InterestsService, private appService: AppService) { }

  ngOnInit() {
    this.filterValue = '';
    this.communities = [];
    this.message = null;
    this.loadInterests();
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  loadInterests() {
    this.appService.communityListObservable.subscribe(communities => {
      if (communities && communities.length) {
        this.communitiesObservable = JSON.parse(JSON.stringify(this.setModifiedFlag(communities)));
        this.communities = JSON.parse(JSON.stringify(this.setModifiedFlag(communities)));
      }
    });
  }

  updateInterestStatus(interest: any): void {
    this.interestsService.updateInterest(interest).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        interest.modified = res.success ? false : interest.modified;
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };

        if (res.success) {
          this.appService.updateCommunityListObservable(this.communities);
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  setModifiedFlag(communities: any[]): any[] {
    return communities.map(community => {
            community.modified = false;
            return community;
    });
  }

  cancelChanges() {
    this.communities = JSON.parse(JSON.stringify(this.communitiesObservable));
  }

  saveAll() {
    const communities: any[] = this.communities.filter(community => community.modified);
    const updateObservable = [];
    communities.map(community => {
      updateObservable.push(this.interestsService.updateInterest(community));
    });

    forkJoin(updateObservable).subscribe(
      (res: any) => {
        if (res.every(updateResponse => updateResponse.success)) {
          this.communities = this.setModifiedFlag(this.communities);
          this.appService.updateCommunityListObservable(this.communities);
          this.message = {
            status: true,
            message: MessageTypes.interestUpdateSuccess
          };
          this.appService.removeMessage(this, 'message');
        } else {
          this.message = {
            status: false,
            message: MessageTypes.interestUpdateFailed
          };
        }
      },
      (err) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  checkDisabled(): number {
    return this.communities.filter(community => community.modified).length;
  }
}
