import { MessageTypes } from './../../../core/models/message-types';
import { LoadMore } from './../../../core/base/load-more.base';
import { HttpErrorResponse } from '@angular/common/http';
import { Subject } from 'rxjs';
import { InterestsService } from './../interests.service';
import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';

@Component({
  selector: 'app-add-interest',
  templateUrl: './add-interest.component.html',
  styleUrls: ['./add-interest.component.css']
})
export class AddInterestComponent extends LoadMore implements OnInit, OnDestroy {
  user: any;
  interestSearchForm: FormGroup;
  showResults: boolean;
  filterValue: string;
  // interests: any[];
  displayedInterests: any[];
  communities: any[];
  communitiesObservable: any;
  unsubscribe = new Subject();

  totalCount: number;
  message: any;

  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    private interestsService: InterestsService,
    private zone: NgZone
  ) {
    super();
   }

  ngOnInit() {
    this.filterValue = '';
    // this.interests = [];
    this.displayedInterests = [];
    this.communities = [];
    this.showResults = false;
    this.totalCount = null;

    this.message = null;

    this.interestSearchForm = this.formBuilder.group({
      keyword: ['', Validators.required],
      matchType: ['', Validators.required]
    });

    // this.appService.interestListObservable.subscribe(interests => {
    //   this.interests = interests;
    // });

    this.appService.communityListObservable.subscribe(communities => {
      this.communities = communities;
    });
  }

  searchInterests(pageNumber?: number) {
    this.pageNumber = pageNumber ? pageNumber : this.pageNumber;
    this.config.loading = true;
    const payload = {
      keyword: this.interestSearchForm.value.keyword,
      matchType: this.interestSearchForm.value.matchType,
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };
    this.appService.searchInterest(payload).subscribe(
      (res: any) => {
        this.showResults = true;
        this.config.loading = false;
        this.displayedInterests = this.displayedInterests.concat(res.result);
        this.totalCount = res.totalCount;
        this.config.loadMore = this.displayedInterests.length < this.totalCount;
        if (!res.success) {
          this.message = {
            status: res.success,
            message: res.message.errorMessage
          };
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
    });
  }

  addInterest(interest: any) {
    const existingInterest = this.communities.find(
      community => community.community_docs.name.toLowerCase() === interest.name.toLowerCase()
    );
    if (existingInterest) {
      this.message = {
        status: false,
        message: MessageTypes.interestAlreadyJoined
      };
      return;
    }
    this.interestsService.addInterest({ communityName: interest.name }).subscribe(
      (res: any) => {
        if (res.success && res.result && res.result.community && res.result.communityUserResult) {
          res.result.communityUserResult.community_docs = res.result.community;
          res.result.communityUserResult.user_docs = this.user;
          this.appService.updateCommunityListObservable(this.communities.concat([res.result.communityUserResult]));
          this.zone.run(() => {
            this.interestSearchForm.reset();
            this.showResults = false;
            this.displayedInterests = [];
            this.pageNumber = 1;
          });
        }

        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  resetSearchResults() {
    this.showResults = false;
    this.message = null;
    this.pageNumber = 1;
    this.displayedInterests = [];
  }

  getActiveInterests(): number {
    return this.communities.filter((community: any) => community.status === 'ACTIVE').length;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  filterSubscriptions(filter: string) {
    this.filterValue = filter;
  }
}
