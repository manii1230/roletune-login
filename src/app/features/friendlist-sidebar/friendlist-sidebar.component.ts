import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FrontService } from './../front.service';
import { AppService } from './../../app.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ChatActionTypes } from '../../core/models/chat-action-types.model';

@Component({
  selector: 'app-friendlist-sidebar',
  templateUrl: './friendlist-sidebar.component.html',
  styleUrls: ['./friendlist-sidebar.component.css']
})
export class FriendlistSidebarComponent implements OnInit, OnDestroy {
  user: any;
  friendList: any[];
  groupList: any[];
  friendsFilter: string;
  friendsChatForm: FormGroup;
  unsubscribe = new Subject();
  messages: any[];
  showLiveStream: boolean;
  audio: any;
  activeFriends: any[];
  
  videoChatFriend: any;
  
  constructor(
    private appService: AppService,
    private frontService: FrontService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.friendsFilter = '';
    this.audio = new Audio();
    this.audio.src = '../../../assets/sounds/new-message.mp3';
    this.audio.load();
    this.friendsChatForm = this.formBuilder.group({
      message: ['', [Validators.required, Validators.minLength(1)]]
    });
    this.friendList = [];
    this.groupList = [];
    this.messages = [];
    this.videoChatFriend = null;
    this.showLiveStream = false;

    this.activeFriends = [];

    this.frontService.getServerSentUpdates('new-message').subscribe(message => {
      if (!this.user.muted.includes(message.fromUserId)) {
        this.audio.play();
      }
      const idx = this.activeFriends.findIndex(friend => friend.username === message.fromUserId);
      if (idx >= 0) {
        this.activeFriends.splice(idx, 1);
      }
      const friend = this.friendList.find(friendItem =>
        friendItem.fromUserId_user_docs.username === message.fromUserId ||
        friendItem.toUserId_user_docs.username === message.fromUserId
      );
      this.scrollChat();

      if (friend) {
        this.setActiveFriend(friend);
      }
    });

    this.frontService.getServerSentUpdates('online-friends').subscribe((friends: any[]) => {
      const onlineFriends = friends.filter((friend: any) => friend.status).map((friend: any) => friend.username);
      this.friendList.forEach((friendRequest: any) => {
        if (
          onlineFriends.some(onlineFriend =>
            (onlineFriend === friendRequest.toUserId) || (onlineFriend === friendRequest.fromUserId)
          )
        ) {
          if (this.user.username === friendRequest.toUserId) {
            friendRequest.fromUserId_user_docs.status = true;
          } else {
            friendRequest.toUserId_user_docs.status = true;
          }
        }
      });
    });

    this.frontService.getServerSentUpdates('user-status-changed').subscribe((user: any) => {
      const friendRequest = this.friendList.find(
        (request: any) => (request.toUserId === user.username) || (request.fromUserId === user.username)
      );
      if (friendRequest) {
        (this.user.username === friendRequest.toUserId) ?
        (friendRequest.fromUserId_user_docs.status = user.status) : (friendRequest.toUserId_user_docs.status = user.status);
      }
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.frontService.emitAction('register-user', this.user);
        this.appService.friendsListObservable.pipe(takeUntil(this.unsubscribe)).subscribe(friendslist => {
          // tslint:disable-next-line: max-line-length
          this.friendList = (friendslist && friendslist.length) ? friendslist.filter((friend: any) => friend.requestStatus === 'APPROVED') : [];
        });

        this.appService.groupListObservable.pipe(takeUntil(this.unsubscribe)).subscribe(groups => {
          this.groupList = groups ? groups : [];
        });
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  updateFilter(event) {
    this.friendsFilter = event.target.value;
  }

  sendChatMessage(activeFriend: any) {
    if (this.friendsChatForm.valid) {
      const payload = {
        fromUserId: this.user.username,
        toUserId: activeFriend.username,
        message: this.friendsChatForm.value
      };
      this.frontService.emitAction('new-chat-message', payload);
      const today = new Date();
      activeFriend.messages.push({
        fromUserId: this.user.username,
        toUserId: activeFriend.username,
        message: this.friendsChatForm.value.message,
        sentAt:
          `${(today.getDate() < 9) ? '0' : ''}${(today.getDate())}-${((today.getMonth() + 1) < 9)? '0' : ''}
          ${(today.getMonth() + 1)}-${today.getFullYear()}::${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`
      });
      this.scrollChat();
      this.friendsChatForm.reset();
    }
  }

  getOnlineStatus(friend: any): boolean {
    return (this.user.username === friend.toUserId) ?
      friend.fromUserId_user_docs.status :
      friend.toUserId_user_docs.status;
  }

  nudgeFriend(friend: any) {
    const friendDetails = (this.user.username === friend.fromUserId_user_docs.username) ?
      friend.toUserId_user_docs :
      friend.fromUserId_user_docs;
    const payload = {
      toUserId: friendDetails.username,
      fromUserId: this.user.username,
      fromUserFirstName: this.user.firstName,
      fromUserLastName: this.user.lastName,
      alertType: 'NUDGE'
    };
    this.frontService.emitAction('nudge-user', payload);
  }

  callFriend(friend: any) {
    if(this.user.username === friend.toUserId) {
      this.videoChatFriend = {
        firstName: friend.fromUserId_user_docs.firstName,
        lastName: friend.fromUserId_user_docs.lastName,
        username: friend.fromUserId_user_docs.username,
        profileImage: friend.fromUserId_user_docs.profileImage
      };
    } else {
      this.videoChatFriend = {
        firstName: friend.toUserId_user_docs.firstName,
        lastName: friend.toUserId_user_docs.lastName,
        username: friend.toUserId_user_docs.username,
        profileImage: friend.toUserId_user_docs.profileImage
      };
    }
    (document.querySelector('.topbar') as HTMLDivElement).style.display = 'none';
    this.showLiveStream = true;
  }

  navigateScreen(blogType: string) {
    this.router.navigate([`main/home/${this.user.username}/${blogType}`]);
  }

  setActiveFriend(friend: any) {
    const username = (this.user.username !== friend.fromUserId) ? friend.fromUserId_user_docs.username : friend.toUserId_user_docs.username; 
    let activeFriend = this.activeFriends.find(user => user.username === username);
    if (activeFriend) {
      return;
    }
    if (this.user.username !== friend.fromUserId) {
      activeFriend = {
        firstName: friend.fromUserId_user_docs.firstName,
        lastName: friend.fromUserId_user_docs.lastName,
        username: friend.fromUserId_user_docs.username,
        profileImage: friend.fromUserId_user_docs.profileImage,
        showChat: true
      };
    } else {
      activeFriend = {
        firstName: friend.toUserId_user_docs.firstName,
        lastName: friend.toUserId_user_docs.lastName,
        username: friend.toUserId_user_docs.username,
        profileImage: friend.toUserId_user_docs.profileImage,
        showChat: true
      };
    }
    this.frontService.getChatMessages(username).subscribe(
      (res: any) => {
        if (res.success) {
          activeFriend.messages = (res.result && res.result.length) ? res.result : [];
          this.scrollChat();
          if (this.activeFriends.length > 4) {
            this.activeFriends.shift();
          }
          this.activeFriends.unshift(activeFriend);
        }
      },
      (err) => {

    });
  }

  scrollChat() {
    setTimeout(() => {
      const chatListTags = document.querySelectorAll('.chat-list > ul');
      chatListTags.forEach(chatListTag => {
        chatListTag.scrollTop = chatListTag.scrollHeight;
      });
    }, 0);
  }

  getSentDate(dateString: string) {
    const dateParts = dateString.substring(0, 10).split('-');
    return new Date(`${dateParts[1]}-${dateParts[0]}-${dateParts[2]}`);
  }

  getSenderDetails(message: any, prop: string): string {
    if (message.fromUserId === this.user.username) {
      switch (prop) {
        case 'IMAGE':
          return this.user.profileImage ? this.user.profileImage : `assets/images/resources/user.jpg`;
        case 'USERNAME':
          return this.user.username;
        case 'NAME':
          return this.user.firstName ? `${this.user.firstName} ${this.user.lastName}` : `Name`;
      }
    }
    const activeFriend = this.activeFriends.find(friend => message.fromUserId === friend.username);
    switch (prop) {
      case 'IMAGE':
        return activeFriend.profileImage ? activeFriend.profileImage : `assets/images/resources/user.jpg`;
      case 'USERNAME':
        return activeFriend.username;
      case 'NAME':
        return activeFriend.firstName ? `${activeFriend.firstName} ${activeFriend.lastName}` : `Name`;
    }
  }

  openContextMenu(event: Event, friend: any) {
    event.preventDefault();
    this.friendList.forEach(friend => friend.showContentMenu = false);
    friend.showContentMenu = true;
  }

  removeActiveFriend(friend: any) {
    const idx = this.activeFriends.findIndex(friendItem => friendItem.username === friend.username);
    if (idx >= 0) {
      this.activeFriends.splice(idx, 1);
    }
  }

  appendEmoji(activeFriend, emojiCode: number) {
    activeFriend.showIcons = !activeFriend.showIcons;
    //document.querySelector(`#${activeFriend.username.split('.').join('-')}`).innerHTML = `${document.querySelector('#' + activeFriend.username.split('.').join('-')).innerHTML}${String.fromCodePoint(emojiCode)}`;
    this.friendsChatForm.patchValue({
      message: `${this.friendsChatForm.value.message ? this.friendsChatForm.value.message + ' ' : ''}${String.fromCodePoint(emojiCode)}`
    });
  }

  chatAction(activeFriend: any, action: ChatActionTypes) {
    switch (action) {
      case ChatActionTypes.archieve:
      case ChatActionTypes.delete:
        activeFriend.messages = []
        break;
    }
    console.log(activeFriend, action);
  }
}
