import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendlistSidebarComponent } from './friendlist-sidebar.component';

describe('FriendlistSidebarComponent', () => {
  let component: FriendlistSidebarComponent;
  let fixture: ComponentFixture<FriendlistSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FriendlistSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendlistSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
