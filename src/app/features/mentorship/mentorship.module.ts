import { SharedModule } from './../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MentorshipService } from './mentorship.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MentorshipRoutingModule } from './mentorship-routing.module';
import { MentorshipDashboardComponent } from './mentorship-dashboard/mentorship-dashboard.component';
import { MentorshipComponent } from './mentorship.component';
import { MenteeRequestsComponent } from './mentee-requests/mentee-requests.component';
import { MentorRequestsComponent } from './mentor-requests/mentor-requests.component';
import { CurrentMentorProgramsComponent } from './current-mentor-programs/current-mentor-programs.component';
import { CurrentMenteeProgramsComponent } from './current-mentee-programs/current-mentee-programs.component';
import { MenteeHistoryComponent } from './mentee-history/mentee-history.component';
import { MentorHistoryComponent } from './mentor-history/mentor-history.component';
import { MentorResponseComponent } from './mentor-requests/mentor-response/mentor-response.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [
    MentorshipDashboardComponent,
    MentorshipComponent,
    MenteeRequestsComponent,
    MentorRequestsComponent,
    CurrentMentorProgramsComponent,
    CurrentMenteeProgramsComponent,
    MenteeHistoryComponent,
    MentorHistoryComponent,
    MentorResponseComponent
  ],
  imports: [
    CommonModule,
    MentorshipRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  providers: [
    MentorshipService
  ]
})
export class MentorshipModule { }
