import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenteeHistoryComponent } from './mentee-history.component';

describe('MenteeHistoryComponent', () => {
  let component: MenteeHistoryComponent;
  let fixture: ComponentFixture<MenteeHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenteeHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenteeHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
