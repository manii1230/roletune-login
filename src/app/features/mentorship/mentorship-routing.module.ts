import { MenteeHistoryComponent } from './mentee-history/mentee-history.component';
import { CurrentMenteeProgramsComponent } from './current-mentee-programs/current-mentee-programs.component';
import { CurrentMentorProgramsComponent } from './current-mentor-programs/current-mentor-programs.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MentorshipDashboardComponent } from './mentorship-dashboard/mentorship-dashboard.component';
import { MentorshipComponent } from './mentorship.component';
import { MenteeRequestsComponent } from './mentee-requests/mentee-requests.component';
import { MentorRequestsComponent } from './mentor-requests/mentor-requests.component';
import { MentorHistoryComponent } from './mentor-history/mentor-history.component';

const routes: Routes = [
  {
    path: '',
    component: MentorshipComponent,
    children: [
      {
        path: 'dashboard',
        component: MentorshipDashboardComponent
      },
      {
        path: 'mentee-requests',
        component: MenteeRequestsComponent
      },
      {
        path: 'mentor-requests',
        component: MentorRequestsComponent
      },
      {
        path: 'current-programs/mentee',
        component: CurrentMenteeProgramsComponent
      },
      {
        path: 'current-programs/mentor',
        component: CurrentMentorProgramsComponent
      },
      {
        path: 'history/mentee',
        component: MenteeHistoryComponent
      },
      {
        path: 'history/mentor',
        component: MentorHistoryComponent
      },
      {
        path: '**',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MentorshipRoutingModule { }
