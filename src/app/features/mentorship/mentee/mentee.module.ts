import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenteeRoutingModule } from './mentee-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MenteeRoutingModule
  ]
})
export class MenteeModule { }
