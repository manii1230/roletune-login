import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentMenteeProgramsComponent } from './current-mentee-programs.component';

describe('CurrentMenteeProgramsComponent', () => {
  let component: CurrentMenteeProgramsComponent;
  let fixture: ComponentFixture<CurrentMenteeProgramsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentMenteeProgramsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentMenteeProgramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
