import { MentorshipService } from './../mentorship.service';
import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-current-mentee-programs',
  templateUrl: './current-mentee-programs.component.html',
  styleUrls: ['./current-mentee-programs.component.css']
})
export class CurrentMenteeProgramsComponent implements OnInit {
  user: any;
  communitiesList: any[];
  requestMentorshipForm: FormGroup;
  showMentors: boolean;
  mentorsList: any[];
  pendingMentorshipRequests: any[];
  currentMentorshipRequests: any[];

  selectedMentor: any;


  constructor(private appService: AppService, private formBuilder: FormBuilder, private mentorshipService: MentorshipService) { }

  ngOnInit() {
    this.showMentors = false;

    this.mentorsList = [];
    this.pendingMentorshipRequests = [];
    this.currentMentorshipRequests = [];

    this.requestMentorshipForm = this.formBuilder.group({
      communityId: ['', [Validators.required, Validators.minLength(24)]],
      mentorName: ['']
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.appService.communityListObservable.subscribe(communitiesList => {
          this.communitiesList = (communitiesList && communitiesList.length) ? communitiesList : [];
        });
        this.loadMentorshipList();
      }
    });
  }

  loadMentorshipList() {
    this.mentorshipService.getMentorshipRequestList(null, '').subscribe(
      (res: any) => {
        if (res.success && res.result) {
          const communities = [];
          res.result.forEach(request => {
            request.milestoneConfig = {
              startDate: request.programDetails.startDate,
              mileStones: request.mileStones,
              duration: request.programDetails.duration
            };
            if (request.requestStatus === 'IN PROGRESS') {
              if (communities.includes(request.community_docs.name)) {
                const idx = this.currentMentorshipRequests.findIndex(item => item.community === request.community_docs.name);
                this.currentMentorshipRequests[idx].requestList.push(request);
              } else {
                communities.push(request.community_docs.name);
                this.currentMentorshipRequests.push({
                  community: request.community_docs.name,
                  requestList: [ request ]
                });
              }
            }
          });
        }
      },
      (err) => {

      }
    );
  }

  searchMentors() {
    this.mentorshipService.getCommunityMentors(this.requestMentorshipForm.value).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.mentorsList = res.result;
          this.showMentors = true;
        }
      },
      (err) => {

      }
    );
  }

  withdrawRequest(request: any, idx: number, requestIdx: number) {
    this.mentorshipService.deleteMentorshipRequest(request.communityId, request.mentorId).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.pendingMentorshipRequests[idx].requestList.splice(requestIdx, 1);
          if (this.pendingMentorshipRequests[idx].requestList.length === 0) {
            this.pendingMentorshipRequests.splice(idx, 1);
          }
        }
      }
    );
  }

  getEndDate(startDate: string, duration: string) {
    const actualDate = new Date(startDate);
    return new Date(actualDate.setDate(actualDate.getDate() + (parseInt(duration, 10) * 7 ))).toISOString().substring(0, 10);
  }
}
