import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentMentorProgramsComponent } from './current-mentor-programs.component';

describe('CurrentMentorProgramsComponent', () => {
  let component: CurrentMentorProgramsComponent;
  let fixture: ComponentFixture<CurrentMentorProgramsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentMentorProgramsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentMentorProgramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
