import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenteeRequestsComponent } from './mentee-requests.component';

describe('MenteeRequestsComponent', () => {
  let component: MenteeRequestsComponent;
  let fixture: ComponentFixture<MenteeRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenteeRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenteeRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
