import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { MentorshipService } from './../mentorship.service';
import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mentee-requests',
  templateUrl: './mentee-requests.component.html',
  styleUrls: ['./mentee-requests.component.css']
})
export class MenteeRequestsComponent implements OnInit {
  user: any;
  communitiesList: any[];
  requestMentorshipForm: FormGroup;
  showMentors: boolean;
  mentorsList: any[];
  pendingMentorshipRequests: any[];
  underConsiderationRequests: any[];
  rejectedMentorshipRequests: any[];

  showMentorshipForm: boolean;
  selectedMentor: any;

  mentorMessageForm: FormGroup;

  constructor(private appService: AppService, private formBuilder: FormBuilder, private mentorshipService: MentorshipService, private formatter: NgbDateParserFormatter) { }

  ngOnInit() {
    this.showMentors = false;
    this.showMentorshipForm = false;

    this.mentorsList = [];
    this.pendingMentorshipRequests = [];
    this.underConsiderationRequests = [];
    this.rejectedMentorshipRequests = [];

    this.requestMentorshipForm = this.formBuilder.group({
      communityId: ['', [Validators.required, Validators.minLength(24)]],
      mentorName: ['']
    });

    this.mentorMessageForm = this.formBuilder.group({
      user: [''],
      message: ['', [Validators.required, Validators.minLength(2)]]
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.appService.communityListObservable.subscribe(communitiesList => {
          this.communitiesList = (communitiesList && communitiesList.length) ? communitiesList : [];
        });
        this.loadMentorshipList();
      }
    });
  }

  loadMentorshipList() {
    this.mentorshipService.getMentorshipRequestList(null, '').subscribe(
      (res: any) => {
        if (res.success && res.result) {
          const communities = [], comm = [], com = [];
          res.result.forEach(request => {
            if (request.requestStatus === 'PENDING') {
              if (communities.includes(request.community_docs.name)) {
                const idx = this.pendingMentorshipRequests.findIndex(item => item.community === request.community_docs.name);
                this.pendingMentorshipRequests[idx].requestList.push(request);
              } else {
                communities.push(request.community_docs.name);
                this.pendingMentorshipRequests.push({
                  community: request.community_docs.name,
                  requestList: [ request ]
                });
              }
            }

            if (request.requestStatus === 'UNDER CONSIDERATION') {
              if (comm.includes(request.community_docs.name)) {
                const idx = this.underConsiderationRequests.findIndex(item => item.community === request.community_docs.name);
                this.underConsiderationRequests[idx].requestList.push(request);
              } else {
                comm.push(request.community_docs.name);
                this.underConsiderationRequests.push({
                  community: request.community_docs.name,
                  requestList: [ request ]
                });
              }
            }

            if (request.requestStatus === 'REJECTED') {
              if (com.includes(request.community_docs.name)) {
                const idx = this.rejectedMentorshipRequests.findIndex(item => item.community === request.community_docs.name);
                this.rejectedMentorshipRequests[idx].requestList.push(request);
              } else {
                com.push(request.community_docs.name);
                this.rejectedMentorshipRequests.push({
                  community: request.community_docs.name,
                  requestList: [ request ]
                });
              }
            }
          });
        }
      },
      (err) => {

      }
    );
  }

  searchMentors() {
    this.mentorshipService.getCommunityMentors(this.requestMentorshipForm.value).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.mentorsList = res.result;
          this.showMentors = true;
        }
      },
      (err) => {

      }
    );
  }

  withdrawRequest(request: any, idx: number, requestIdx: number) {
    this.mentorshipService.deleteMentorshipRequest(request.communityId, request.mentorId).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.pendingMentorshipRequests[idx].requestList.splice(requestIdx, 1);
          if (this.pendingMentorshipRequests[idx].requestList.length === 0) {
            this.pendingMentorshipRequests.splice(idx, 1);
          }
        }
      }
    );
  }

  getAvailableDate(availableFrom: NgbDateStruct): string {
    return this.formatter.format(availableFrom);
  }

  getEndDate(startDate: string, duration: string) {
    const actualDate = new Date(startDate);
    return new Date(actualDate.setDate(actualDate.getDate() + (parseInt(duration, 10) * 7 ))).toISOString().substring(0, 10);
  }

  updateMentorship(request: any, flag?: string) {
    this.mentorMessageForm.patchValue({
      user: this.user.username
    });
    const payload = JSON.parse(JSON.stringify(request));
    payload.isMentor = false;
    payload.toUserId = request.mentorId;

    if (this.mentorMessageForm.value.message && this.mentorMessageForm.value.message.length) {
      payload.message.push(this.mentorMessageForm.value);
    }

    if (flag) {
      payload.requestStatus = flag;
    }

    this.mentorshipService.updateMentorshipRequest(payload).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          request.requestStatus = payload.requestStatus;
          if (this.mentorMessageForm.value.message && this.mentorMessageForm.value.message.length) {
            request.message.push(this.mentorMessageForm.value);
          }
          this.mentorMessageForm.reset();
        }
      },
      (err) => {
      }
    );
  }

  requestMentorshipHandle(response: any) {
    this.showMentorshipForm = false;
    this.selectedMentor = null;
    console.log(`Response: `, response);
  }
}
