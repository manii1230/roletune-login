import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MentorshipService {

  constructor(private http: HttpClient) { }

  getCommunityMentors(formData: any) {
    // tslint:disable-next-line: max-line-length
    return formData.mentorName ? this.http.get(`${environment.API_URL}mentorship-service/mentors-list/${formData.communityId}/?isMentor=true&&name=${formData.mentorName}&&level=`) : this.http.get(`${environment.API_URL}mentorship-service/mentors-list/${formData.communityId}/?isMentor=true&&name=&&level=`);
  }

  getMentorshipAvailabilitySettings(communityId: string) {
    return this.http.get(`${environment.API_URL}mentorship-service/mentorship-availability/${communityId}`);
  }

  updateMentorshipAvailabilitySettings(mentorshipAvailabilitySettings: any) {
    return this.http.patch(`${environment.API_URL}mentorship-service/mentorship-availability/${mentorshipAvailabilitySettings.communityId}`, { mentorshipAvailabilitySettings });
  }

  sendMentorshipRequest(payload: any) {
    return this.http.post(`${environment.API_URL}mentorship-service/send-mentorship-request`, payload);
  }

  getMentorshipRequestList(mentor?: any, status?: string) {
    // tslint:disable-next-line: max-line-length
    return mentor ? this.http.get(`${environment.API_URL}mentorship-service/mentorship-request-list?isMentor=true&status=${status}&userId=${mentor.username}`) : this.http.get(`${environment.API_URL}mentorship-service/mentorship-request-list?isMentor=false&status=${status}&userId=`);
  }

  deleteMentorshipRequest(communityId: string, mentorId: string) {
    // tslint:disable-next-line: max-line-length
    return this.http.delete(`${environment.API_URL}mentorship-service/mentorship-request?isMentor=false&communityId=${communityId}&toUserId=${mentorId}`);
  }

  updateMentorshipRequest(mentorshipRequest: any) {
    return this.http.patch(`${environment.API_URL}mentorship-service/update-mentorship-request`, mentorshipRequest);
  }
}
