import { MentorshipService } from './../mentorship.service';
import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mentor-history',
  templateUrl: './mentor-history.component.html',
  styleUrls: ['./mentor-history.component.css']
})
export class MentorHistoryComponent implements OnInit {
  user: any;
  communitiesList: any[];
  requestMentorshipForm: FormGroup;
  showMentors: boolean;
  mentorsList: any[];
  pendingMentorshipRequests: any[];
  mentorshipRequests: any[];

  selectedMentor: any;


  constructor(private appService: AppService, private formBuilder: FormBuilder, private mentorshipService: MentorshipService) { }

  ngOnInit() {
    this.showMentors = false;

    this.mentorsList = [];
    this.pendingMentorshipRequests = [];
    this.mentorshipRequests = [];

    this.requestMentorshipForm = this.formBuilder.group({
      communityId: ['', [Validators.required, Validators.minLength(24)]],
      mentorName: ['']
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.appService.communityListObservable.subscribe(communitiesList => {
          this.communitiesList = (communitiesList && communitiesList.length) ? communitiesList : [];
        });
        this.loadMentorshipList();
      }
    });
  }

  loadMentorshipList() {
    this.mentorshipService.getMentorshipRequestList(this.user, '').subscribe(
      (res: any) => {
        if (res.success && res.result) {
          const communities = [];
          res.result.forEach(request => {
            if (request.requestStatus === 'COMPLETED') {
              if (communities.includes(request.community_docs.name)) {
                const idx = this.mentorshipRequests.findIndex(item => item.community === request.community_docs.name);
                this.mentorshipRequests[idx].requestList.push(request);
              } else {
                communities.push(request.community_docs.name);
                this.mentorshipRequests.push({
                  community: request.community_docs.name,
                  requestList: [ request ]
                });
              }
            }
          });
        }
      },
      (err) => {

      }
    );
  }

  searchMentors() {
    this.mentorshipService.getCommunityMentors(this.requestMentorshipForm.value).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.mentorsList = res.result;
          this.showMentors = true;
        }
      },
      (err) => {

      }
    );
  }

  withdrawRequest(request: any, idx: number, requestIdx: number) {
    this.mentorshipService.deleteMentorshipRequest(request.communityId, request.mentorId).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.pendingMentorshipRequests[idx].requestList.splice(requestIdx, 1);
          if (this.pendingMentorshipRequests[idx].requestList.length === 0) {
            this.pendingMentorshipRequests.splice(idx, 1);
          }
        }
      }
    );
  }

  getEndDate(startDate: string, duration: string) {
    const actualDate = new Date(startDate);
    return new Date(actualDate.setDate(actualDate.getDate() + (parseInt(duration, 10) * 7 ))).toISOString().substring(0, 10);
  }
}
