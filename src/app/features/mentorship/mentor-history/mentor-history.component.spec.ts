import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorHistoryComponent } from './mentor-history.component';

describe('MentorHistoryComponent', () => {
  let component: MentorHistoryComponent;
  let fixture: ComponentFixture<MentorHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
