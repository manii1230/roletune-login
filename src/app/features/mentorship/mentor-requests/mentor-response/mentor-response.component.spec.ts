import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorResponseComponent } from './mentor-response.component';

describe('MentorResponseComponent', () => {
  let component: MentorResponseComponent;
  let fixture: ComponentFixture<MentorResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
