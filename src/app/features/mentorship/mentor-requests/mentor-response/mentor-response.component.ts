import { MentorshipService } from './../../mentorship.service';
import { AppService } from './../../../../app.service';
import { Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mentor-response',
  templateUrl: './mentor-response.component.html',
  styleUrls: ['./mentor-response.component.css']
})
export class MentorResponseComponent implements OnInit {

  @Input() mentorshipRequest;
  @Output() hideForm = new EventEmitter();

  mentorshipForm: FormGroup;

  changeDuration: boolean;

  constructor(private appService: AppService, private formBuilder: FormBuilder, private mentorshipService: MentorshipService) { }

  ngOnInit() {
    this.changeDuration = false;

    this.mentorshipForm = this.formBuilder.group({
      topics: ['', Validators.required],
      duration: [null, Validators.required],
      startDate: [null, Validators.required],
      endDate: [null, Validators.required],
      message: [''],
      milestones: this.formBuilder.array([])
    });

    this.updateMentorshipForm();
    // tslint:disable-next-line: max-line-length
    this.mentorshipRequest.request.milestones = (this.mentorshipRequest && this.mentorshipRequest.request && this.mentorshipRequest.request.milestones) ? [] : [];
  }

  addFormGroup(): FormGroup {
    return this.formBuilder.group({
      title: ['', Validators.required ],
      date: ['', Validators.required ]
    });
  }

  addMilestone(): void {
    (this.mentorshipForm.get('milestones') as FormArray).push(this.addFormGroup());
  }

  toggleForm(flag: boolean) {
    this.hideForm.emit(flag);
  }

  updateMentorship() {
    const payload: any = {};
    payload.requestStatus = this.mentorshipRequest.action;
    payload.isMentor = true;
    payload.communityId = this.mentorshipRequest.request.communityId;
    payload.programDetails = {
      topics: this.mentorshipForm.get('topics').value,
      duration: this.mentorshipForm.get('duration').value,
      startDate: this.mentorshipForm.get('startDate').value
    };
    payload.message = this.mentorshipForm.get('message').value ? [
      {
        user: this.mentorshipRequest.request.mentorId,
        message: this.mentorshipForm.get('message').value
      }
    ] : [];
    payload.toUserId = this.mentorshipRequest.request.menteeId;
    payload.mileStones = this.mentorshipForm.get('milestones').value;
    payload.isRead = true;

    this.mentorshipService.updateMentorshipRequest(payload).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.toggleForm(false);
        }
      },
      (err) => {

      }
    );
  }

  getMinDate() {
    return new Date();
  }

  updateMentorshipForm() {
    this.mentorshipForm.patchValue({
      topics: this.mentorshipRequest.request.programDetails.topics,
      duration: this.mentorshipRequest.request.programDetails.duration,
      startDate: this.mentorshipRequest.request.programDetails.startDate
    });
    this.updateEndDate();
  }

  updateEndDate(): void {
    const actualDate = new Date(this.mentorshipRequest.request.programDetails.startDate);
    const endDate = new Date(actualDate.setDate(actualDate.getDate() + (parseInt(this.mentorshipForm.value.duration, 10) * 7 )));
    this.mentorshipForm.patchValue({
      endDate: endDate.toISOString().substring(0, 10)
    });
  }
}
