import { MentorshipService } from './../mentorship.service';
import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mentor-requests',
  templateUrl: './mentor-requests.component.html',
  styleUrls: ['./mentor-requests.component.css']
})
export class MentorRequestsComponent implements OnInit {
  user: any;
  communitiesList: any[];
  requestMentorshipForm: FormGroup;
  showMentors: boolean;
  mentorsList: any[];
  pendingMentorshipRequests: any[];
  underConsiderationRequests: any[];

  mentorshipRequestConfig: any;
  showResponseForm: boolean;

  showMentorshipForm: boolean;
  selectedMentor: any;

  mentorMessageForm: FormGroup;

  constructor(private appService: AppService, private formBuilder: FormBuilder, private mentorshipService: MentorshipService) { }

  ngOnInit() {
    this.showResponseForm = false;
    this.mentorshipRequestConfig = {};
    this.pendingMentorshipRequests = [];
    this.underConsiderationRequests = [];

    this.mentorMessageForm = this.formBuilder.group({
      user: [''],
      message: ['', [Validators.required, Validators.minLength(2)]]
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.loadMentorRequests();
      }
    });
  }

  loadMentorRequests() {
    this.mentorshipService.getMentorshipRequestList(this.user, '').subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.pendingMentorshipRequests = res.result.filter(request => request.requestStatus === 'PENDING');
          const communities = [];

          res.result.forEach(request => {
            if (request.requestStatus === 'UNDER CONSIDERATION') {
              if (communities.includes(request.community_docs.name)) {
                const idx = this.underConsiderationRequests.findIndex(item => item.community === request.community_docs.name);
                this.underConsiderationRequests[idx].requestList.push(request);
              } else {
                communities.push(request.community_docs.name);
                this.underConsiderationRequests.push({
                  community: request.community_docs.name,
                  requestList: [ request ]
                });
              }
            }
          });
        }
      },
      (err) => {

      }
    );
  }

  respondMentorshipRequest(idx: number, status: string) {
    this.mentorshipRequestConfig = {
      action: status,
      request: this.pendingMentorshipRequests[idx]
    };
    this.showResponseForm = true;
  }

  handleMentorshipResponse(event: any) {
    console.log(`Event: `, event);
    this.showResponseForm = false;
  }

  getEndDate(startDate: string, duration: string) {
    const actualDate = new Date(startDate);
    return new Date(actualDate.setDate(actualDate.getDate() + (parseInt(duration, 10) * 7 ))).toISOString().substring(0, 10);
  }

  updateMentorMessage(request: any) {
    this.mentorMessageForm.patchValue({
      user: this.user.username
    });
    const payload = JSON.parse(JSON.stringify(request));
    payload.isMentor = true;
    payload.toUserId = request.menteeId;
    payload.message.push(this.mentorMessageForm.value);
    this.mentorshipService.updateMentorshipRequest(payload).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          request.message.push(this.mentorMessageForm.value);
          this.mentorMessageForm.reset();
        }
      },
      (err) => {
      }
    );
  }
}
