import { AppService } from './../../../app.service';
import { MentorshipService } from './../mentorship.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mentorship-dashboard',
  templateUrl: './mentorship-dashboard.component.html',
  styleUrls: ['./mentorship-dashboard.component.css']
})
export class MentorshipDashboardComponent implements OnInit {
  user: any;
  communityId: string;
  mentorshipAvailabilityForm: FormGroup;
  mentorshipData: any;
  colors: any;
  communitiesList: any[];
  showAvailabilityDetails: boolean;

  constructor(private appService: AppService, private formBuilder: FormBuilder, private mentorshipService: MentorshipService) { }

  ngOnInit() {
    this.communityId = null;
    this.communitiesList = [];
    this.showAvailabilityDetails = false;

    this.mentorshipAvailabilityForm = this.formBuilder.group({
      communityId: [null],
      available: ['true', Validators.required],
      unavailableFrom: [''],
      unavailableTo: [''],
      availableFrom: [''],
      concurrentMentorships: [null, Validators.required]
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;

        this.appService.communityListObservable.subscribe(communities => {
          this.communitiesList = (communities && communities.length) ? communities : [];
        });

        this.mentorshipData = {
          username: this.user.username ? this.user.username : '',
          mentor: (this.user.mentorshipAvailabilitySettings && this.user.mentorshipAvailabilitySettings.mentor) ? this.user.mentorshipAvailabilitySettings.mentor : '',
          mentees: (this.user.mentorshipAvailabilitySettings && this.user.mentorshipAvailabilitySettings.mentees) ? this.user.mentorshipAvailabilitySettings.mentees : []
        };

        this.colors = {
          lines: '#505050', names: '#505050', circles: '#505050'
        };
        this.updateForm();
      }
    });
  }

  updateForm() {
    this.mentorshipAvailabilityForm.patchValue({
      available: (this.user.mentorshipAvailabilitySettings && this.user.mentorshipAvailabilitySettings.available) ? 'true' : 'false',
      availableFrom: (this.user.mentorshipAvailabilitySettings && this.user.mentorshipAvailabilitySettings.availableFrom) ? this.user.mentorshipAvailabilitySettings.availableFrom : '',
      concurrentMentorships: (this.user.mentorshipAvailabilitySettings && this.user.mentorshipAvailabilitySettings.concurrentMentorships) ? this.user.mentorshipAvailabilitySettings.concurrentMentorships : null,
      mentees: (this.user.mentorshipAvailabilitySettings && this.user.mentorshipAvailabilitySettings.mentees) ? this.user.mentorshipAvailabilitySettings.mentees : [],
      mentor: (this.user.mentorshipAvailabilitySettings && this.user.mentorshipAvailabilitySettings.mentor) ? this.user.mentorshipAvailabilitySettings.mentor : '',
      unavailableFrom: (this.user.mentorshipAvailabilitySettings && this.user.mentorshipAvailabilitySettings.unavailableFrom) ? this.user.mentorshipAvailabilitySettings.unavailableFrom : '',
      unavailableTo: (this.user.mentorshipAvailabilitySettings && this.user.mentorshipAvailabilitySettings.unavailableTo) ? this.user.mentorshipAvailabilitySettings.unavailableTo : ''
    });
  }

  updateAvailability(fields?: string) {
    const payload: any = {};
    payload.communityId = this.mentorshipAvailabilityForm.value.communityId;
    payload.available = this.mentorshipAvailabilityForm.value.available;
    switch (fields) {
      case 'available':
        payload.availableFrom = this.mentorshipAvailabilityForm.value.availableFrom;
        payload.concurrentMentorships = this.mentorshipAvailabilityForm.value.concurrentMentorships;
        payload.unavailableFrom = '';
        payload.unavailableTo = '';
        break;
      case 'unavailable':
        payload.availableFrom = '';
        payload.unavailableFrom = this.mentorshipAvailabilityForm.value.unavailableFrom;
        payload.unavailableTo = this.mentorshipAvailabilityForm.value.unavailableTo;
        break;
      default:
        if (payload.available === 'true') {
          payload.availableFrom = this.mentorshipAvailabilityForm.value.availableFrom;
          payload.concurrentMentorships = this.mentorshipAvailabilityForm.value.concurrentMentorships;
          payload.unavailableFrom = '';
          payload.unavailableTo = '';
        } else {
          payload.availableFrom = '';
          payload.unavailableFrom = this.mentorshipAvailabilityForm.value.unavailableFrom;
          payload.unavailableTo = this.mentorshipAvailabilityForm.value.unavailableTo;
        }
    }

    this.mentorshipService.updateMentorshipAvailabilitySettings(payload).subscribe(
      (res: any) => {
        if (res.success) {
          this.user.mentorshipAvailabilitySettings = (res.user && res.user.mentorshipAvailabilitySettings) ? res.user.mentorshipAvailabilitySettings : this.user.mentorshipAvailabilitySettings;
          this.appService.updateUser(this.user);
          this.updateForm();
        }
      },
      (err) => {

    });
  }

  getCurrentDate(): string {
    const date = new Date();
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }

  checkToDate(flag: boolean) {
      this.mentorshipAvailabilityForm.patchValue({
        unavailableTo: flag ? (((new Date(this.mentorshipAvailabilityForm.value.unavailableTo).valueOf() - new Date(this.mentorshipAvailabilityForm.value.unavailableFrom).valueOf()) < 0) ? '' : this.mentorshipAvailabilityForm.value.unavailableTo) : ''
      });
  }

  selectCommunity() {
    if (!this.communityId) {
      return;
    }
    this.mentorshipService.getMentorshipAvailabilitySettings(this.communityId).subscribe(
      (res: any) => {
        if (res.success) {
          this.showAvailabilityDetails = true;
          this.mentorshipAvailabilityForm.patchValue({
            communityId: this.communityId
          });
        }
      },
      (err) => {

    });
  }
}
