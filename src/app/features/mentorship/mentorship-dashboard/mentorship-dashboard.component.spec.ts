import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorshipDashboardComponent } from './mentorship-dashboard.component';

describe('MentorshipDashboardComponent', () => {
  let component: MentorshipDashboardComponent;
  let fixture: ComponentFixture<MentorshipDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorshipDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorshipDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
