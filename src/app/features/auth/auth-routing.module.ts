import { LoggedInGuard } from './../../core/guards/logged-in.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { AuthComponent } from './auth.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    canActivateChild: [ LoggedInGuard ],
    children: [
      {
        path: 'login',
        component:  LoginComponent
      },
      {
        path: 'sign-up',
        component:  SignUpComponent
      },
      {
        path: 'forgot-password',
        component:  ForgetPasswordComponent
      },
      {
        path: '**',
        redirectTo: 'login',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
