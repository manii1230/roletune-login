import { NavItem } from './../../../core/models/nav-item';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-auth-header',
  templateUrl: './auth-header.component.html',
  styleUrls: ['./auth-header.component.css']
})
export class AuthHeaderComponent implements OnInit {
  @Input() menu: NavItem[];
  constructor() { }

  ngOnInit() {
  }

}
