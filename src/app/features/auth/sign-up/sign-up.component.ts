import { HttpErrorResponse } from '@angular/common/http';
import { MessageTypes } from '../../../core/models/message-types';
import { AppService } from '../../../app.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from './register.service';

declare var M:any;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  showTwoFactorForm: boolean;
  usernameVerified: boolean;
  usernameTaken: boolean;
  message: any;
  locations: string[];

  constructor(
    private formBuilder: FormBuilder,
    private registerService: RegisterService,
    private appService: AppService
  ) { }

  ngOnInit() {
    this.showTwoFactorForm = false;
    this.usernameVerified = false;
    this.usernameTaken = false;
    this.locations = this.appService.getLocations();
    this.registerForm  =  this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      middleName: [''],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      role: ['B2USER'],
      dateOfBirth: ['', [Validators.pattern(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/)]],
      location: [''],
      gender: ['male'],
      username: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
      password: ['',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(128),
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!#%*?&._]{8,128}$/)
        ]
      ],
      confirmPassword: ['',
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(128),
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!#%*?&._]{8,128}$/)
        ]
      ],
      email: ['',
        [
          Validators.required,
          Validators.email,
          Validators.pattern(/^([a-z A-Z 0-9 _.-])+@(([a-z A-Z 0-9-])+.)+([a-z A-z 0-9]{3,3})+$/)
        ]
      ],
      stdCode: ['+91'],
      phone: [null],
      otp: [''],
      otpToken: [''],
      twoFactorAuth: [false],
      termsAndConditions: [false]
    });
    this.setupValidations('twoFactorAuth', 'phone', [Validators.required, Validators.pattern(/^\d{10}$/)]);
  }

  get f() { return this.registerForm.controls; }

  get firstName() {
    return this.registerForm.get('firstName');
  }

  get lastName() {
    return this.registerForm.get('lastName');
  }

  get dateOfBirth() {
    return this.registerForm.get('dateOfBirth');
  }

  get location() {
    return this.registerForm.get('location');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

  get email() {
    return this.registerForm.get('email');
  }

  get username() {
    return this.registerForm.get('username');
  }

  get phone() {
    return this.registerForm.get('phone');
  }

  get twoFactorAuth() {
    return this.registerForm.get('twoFactorAuth');
  }

  setupValidations(source: string, target: string, validators: any[]) {
    const formControl = this.registerForm.get(target);
    if (source) {
      this.registerForm.get(source).valueChanges.subscribe(control => {
        formControl.setValidators(control ? validators : null);
        formControl.updateValueAndValidity();
      });
    } else {
      formControl.setValidators(validators);
      formControl.updateValueAndValidity();
    }
  }

  checkDateValidator() {
    this.registerForm.value.dateOfBirth
    ? this.setupValidations(null, 'dateOfBirth', [Validators.pattern(/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/)])
    : this.setupValidations(null, 'dateOfBirth', null);
  }

  register() {
    /* console.warn('hello'); */
    this.submitted = true;
    /* if (this.registerForm.invalid) {
      return;
    } */
    const user = Object.assign({}, this.registerForm.value);
    user.password = btoa(user.password);

    if (!user.middleName) {
      delete user.middleName;
    }
    if (!user.otp) {
      delete user.otp;
    }
    if (!user.dateOfBirth) {
      delete user.dateOfBirth;
    }
    if (!user.otpToken) {
      delete user.otpToken;
    }
    if (!user.phone) {
      delete user.phone;
    }
    if (!user.gender) {
      delete user.gender;
    }
    if (!user.location) {
      delete user.location;
    }
    this.registerService.registerUser(this.registerForm.value).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? MessageTypes.registerSuccess : res.message.errorMessage
        };

        if (res.success) {
          this.resetForm('message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
        console.warn('successful api connection');
       /*  M.toast({html:'Registation done succesfully',classes: 'rounded'}); */
      }
    );
  }

  resetForm(flag?: string) {
    this.submitted = false;
    this.usernameVerified = false;
    this.usernameTaken = false;
    this.showTwoFactorForm = false;
    this.registerForm.reset();
    if (!flag) {
      this.message = null;
    }
    this.registerForm.patchValue({
      role: 'B2USER',
      stdCode: '+91',
      gender: 'male'
    });
  }

  setUsername() {
    if ((this.firstName.value && this.firstName.invalid) || (this.lastName.value && this.lastName.invalid)) {
      return;
    }
    let username = this.registerForm.value.firstName ? this.registerForm.value.firstName.substring(0, 5) : '';
    username += this.registerForm.value.middleName ? ('.' + this.registerForm.value.middleName.substring(0, 2)) : '';
    username += this.registerForm.value.lastName ? ('.' + this.registerForm.value.lastName.substring(0, 5)) : '';

    if (username && username.length && (username.length < 4)) {
      return;
    }
    this.registerService.getUsername(username.toLowerCase()).subscribe(
      (res: any) => {
        if (res.success) {
          this.registerForm.patchValue({
            username: (res.result && res.result.username) ? res.result.username : ''
          });
        }
        this.usernameVerified = res.success;
      },
      (err) => {
        this.usernameVerified = false;
    });
  }

  checkUsernameAvailability() {
    if (this.registerForm.value.username.length > 3) {
      this.registerService.checkUsername(this.registerForm.value).subscribe(
        (res: any) => {
          this.usernameTaken = !res.success;
          this.usernameVerified = res.success;
        },
        (err) => {
          this.usernameVerified = false;
          this.usernameTaken = true;
        }
      );
    } else {
      this.usernameVerified = false;
      this.usernameTaken = false;
    }
  }

  verifyMobileNumber() {
    if (this.registerForm.value.phone && (this.registerForm.value.phone.length === 10)) {
      this.registerService.sendOtp(this.registerForm.value.phone).subscribe(
        (res: any) => {
          if (res.success) {
            this.setupValidations(null, 'otpToken', [Validators.required]);
            res.tempToken = res.tempToken ? res.tempToken : res.result.tempToken;
            this.showTwoFactorForm = true;
            this.registerForm.patchValue({ otpToken: res.tempToken });
          }
          this.message = {
            status: res.success,
            message: res.success ? MessageTypes.otpSentSuccess : res.message.errorMessage
          };

          if (res.success) {
            this.appService.removeMessage(this, 'message');
          }
        },
        (err) => {
          this.message = {
            status: false,
            message: (err.message && err.message.errorMessage) ? err.message.errorMessage : MessageTypes.otpSentFailed
          };
        }
      );
    } else {
      this.message = {
        status: false,
        message: MessageTypes.phoneInvalid
      };
    }
  }

}
