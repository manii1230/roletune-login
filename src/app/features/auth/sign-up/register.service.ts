import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  readonly baseURL = 'http://localhost:3000/Persion';
  constructor(private http: HttpClient) { }

  getUsername(username: string) {
    return this.http.get(`${environment.API_URL}user-service/username-suggestion/${username}`);
  }

  checkUsername(user: any) {
    return this.http.get(`${environment.API_URL}user-service/username/${user.username}`);
  }

  sendOtp(phone: string) {
    return this.http.get(`${environment.API_URL}user-service/send-OTP/${phone}/false`);
  }

  registerUser(user: any) {
    console.warn('success');
    return this.http.post(this.baseURL, user);
  }
}
