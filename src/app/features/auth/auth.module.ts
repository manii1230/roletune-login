import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordService } from './forget-password/forgot-password.service';
import { RegisterService } from './sign-up/register.service';
import { LoginService } from './login/login.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { AuthComponent } from './auth.component';
import { AuthHeaderComponent } from './auth-header/auth-header.component';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
    LoginComponent,
    SignUpComponent,
    ForgetPasswordComponent,
    AuthComponent,
    AuthHeaderComponent
  ],
  providers: [LoginService, RegisterService, ForgotPasswordService]
})
export class AuthModule { }
