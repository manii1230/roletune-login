import { HttpErrorResponse } from '@angular/common/http';
import { MessageTypes } from '../../../core/models/message-types';
import { forkJoin } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from '../../../app.service';
import { LoginService } from './login.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  showOtpForm: boolean;
  loginForm: FormGroup;
  otpForm: FormGroup;
  token: string;
  otpSent: boolean;
  showLoader: boolean;
  message: any;
  registeredNumber: string;
  rememberMe: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    private appService: AppService,
    private cookieService: CookieService
  ) {
    // if (sessionStorage.getItem('token')) {
    if (this.cookieService.get('token')) {
      this.router.navigate(['main/home']);
    }
  }

  ngOnInit() {
    this.showOtpForm = false;
    this.otpSent = false;
    this.rememberMe = false;
    this.registeredNumber = '';
    const savedPassword = localStorage.getItem('password') ? atob(localStorage.getItem('password')) : null;
    this.loginForm = this.formBuilder.group({
      userId: [localStorage.getItem('userId'), Validators.required],
      role: ['B2USER', Validators.required],
      password: [savedPassword, [Validators.required, Validators.minLength(6)]],
    });
    this.otpForm = this.formBuilder.group({
      phone: ['', [Validators.required, Validators.minLength(10)]],
      otpToken: ['', Validators.required],
      otp: ['', Validators.required]
    });
    this.showLoader = false;
  }

  get userId() { return this.loginForm.get('userId'); }
  get password() { return this.loginForm.get('password'); }

  login() {
    //console.warn(this.loginForm.value);
    this.showLoader = true;
    const user = Object.assign({}, this.loginForm.value);
    user.password = btoa(user.password);
    user.email = user.userId;/* userId */
    this.loginService.login(user).subscribe(
      (res: any) => {
        if (res=='2') {
          console.warn('hello');
        }else{
          console.warn('hello1');
          if (this.rememberMe) {
            localStorage.setItem('userId', user.email);
            localStorage.setItem('password', user.password);
            console.warn(localStorage);
            this.showLoader = false;
          }
        }
        /* if (res.success) {
          console.warn(res);
          if (this.rememberMe) {
            localStorage.setItem('userId', user.email);
            localStorage.setItem('password', user.password);
          }

          if (res.result && res.result.twoFactorAuth) {
            this.showLoader = false;
            this.showOtpForm = true;
            this.otpSent = false;
            this.token = (res.result && res.result.otpToken) ? res.result.otpToken : null;
            this.registeredNumber = (res.result && res.result.phone) ? res.result.phone : '**********';
          } else {
            // sessionStorage.setItem('token', (res.result && res.result.token) ? res.result.token : '');
            this.cookieService.set('token', (res.result && res.result.token) ? res.result.token : null);
            this.setUserData(res.result);
          }
        } else {
          this.showLoader = false;
          this.message = {
            status: res.success,
            message: res.message.errorMessage
          };
          this.appService.removeMessage(this, 'message');
        } */
      },
      (err: HttpErrorResponse) => {
        // this.appService.showToast('ERROR', err.message, 'error');
        this.showLoader = false;
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  sendOtp() {
    if (
      !this.otpForm.value.phone ||
      (this.otpForm.value.phone.length !== 10) ||
      (this.registeredNumber.substring(6, 10) !== this.otpForm.value.phone.substring(6, 10))
    ) {
      this.message = {
        status: false,
        message: MessageTypes.otpFormRequired
      };
      return;
    }
    this.showLoader = true;
    this.loginService.sendOtp(this.otpForm.value.phone).subscribe(
      (res: any) => {
        this.showLoader = false;
        this.message = {
          status: res.success,
          message: res.success ?
            MessageTypes.otpSentSuccess :
            (res.message.errorMessage ? res.message.errorMessage : MessageTypes.otpSentFailed)
        };
        if (res.success) {
          this.appService.removeMessage(this, 'message');
          this.otpSent = true;
          this.otpForm.patchValue({ otpToken: res.result ?  res.result.tempToken : null });
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  submitOtp() {
    if (this.otpForm.invalid) {
      return;
    }
    this.showLoader = true;
    const payload = Object.assign({}, this.loginForm.value);
    payload.username = this.loginForm.value.userId;
    payload.otpToken = this.otpForm.value.otpToken;
    payload.otp = this.otpForm.value.otp;
    payload.phone = this.otpForm.value.phone;
    this.loginService.submitOtp(payload).subscribe(
      (res: any) => {
        if (!res.success) {
          this.showLoader = false;
          this.message = {
            status: res.success,
            message: MessageTypes.otpInvalid
          };
          return;
        }

        this.cookieService.set('token', (res.result && res.result.token) ? res.result.token : null);
        this.setUserData(res.result);
      },
      (err: HttpErrorResponse) => {
        this.showLoader = false;
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  setUserData(result: any) {
    this.appService.updateUser(result);

    const communityListObservable = this.appService.getCommunityList();
    const friendListObservable =  this.appService.getFriendsList();
    const getGroupListObservable =  this.appService.getGroupList();

    forkJoin([friendListObservable, communityListObservable, getGroupListObservable ]).subscribe(
      (response: any) => {

        if (response[0] && response[0].success) {
          this.appService.updateFriendListObservable(response[0].result ? response[0].result : []);
        }

        if (response[1] && response[1].success) {
          this.appService.updateCommunityListObservable(response[1].result ? response[1].result : []);
        }

        if (response[2] && response[2].success) {
          this.appService.updateGroupListObservable(response[2].result ? response[2].result : []);
        }

        this.router.navigate(['main/home']);

      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }
}
