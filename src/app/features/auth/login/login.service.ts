import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  readonly baseURL = 'http://localhost:3000/Persion/login';
  constructor(private http: HttpClient) { }
  
  login(user: any): Observable<any> {
    //return this.http.post(`${environment.API_URL}user-service/login`, user);
    return this.http.post(this.baseURL, user);
  }

  sendOtp(mobile: string): Observable<any> {
    return this.http.get(`${environment.API_URL}user-service/send-OTP/${mobile}/true`);
  }

  submitOtp(otpDetails: any): Observable<any> {
    return this.http.post(`${environment.API_URL}user-service/two-factor-login`, otpDetails);
  }
}
