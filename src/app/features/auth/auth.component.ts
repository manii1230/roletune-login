import { NavItem } from './../../core/models/nav-item';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnDestroy {
  showHeader: boolean;
  navMenu: NavItem[];
  subscriptions: any[];

  constructor(private router: Router) {
    this.showHeader = false;
    this.navMenu = [];
    this.subscriptions = [];
    this.setMenu();
  }

  ngOnDestroy() {

  }

  setMenu() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (this.router.url.indexOf('login') >= 0) {
          this.showHeader = false;
          this.navMenu = [];
        }

        if (this.router.url.indexOf('forgot-password') >= 0) {
          this.showHeader = true;
          this.navMenu = [
            new NavItem(`/auth/sign-up`, 'Sign Up', false, []),
            new NavItem(`/auth/login`, 'Sign In', false, [])
          ];
        }

        if (this.router.url.indexOf('sign-up') >= 0) {
          this.showHeader = true;
          this.navMenu = [
            new NavItem(`/auth/login`, 'Sign In', false, []),
          ];
        }
      }
    });
  }

}
