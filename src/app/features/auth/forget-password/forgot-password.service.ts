import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForgotPasswordService {

  constructor(private http: HttpClient) { }

  submitEmail(user: any) {
    return this.http.get(`${environment.API_URL}user-service/forgot-password/email/${user.email}`);
  }

  submitVerificationCode(data: any) {
    return this.http.post(`${environment.API_URL}user-service/forgot-password/verify`, data);
  }

  submitOtp(user: any) {
    return this.http.get(`${environment.API_URL}user-service/forgot-password/phone/${user.phone}`);
  }

  forgotPassword(payload: any) {
    return this.http.post(`${environment.API_URL}user-service/forgot-user-password`, payload);
  }
}
