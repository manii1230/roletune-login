import { HttpErrorResponse } from '@angular/common/http';
import { MessageTypes } from '../../../core/models/message-types';
import { ForgotPasswordSteps } from '../../../core/models/forgot-password-steps.model';
import { AppService } from '../../../app.service';
import { ForgotPasswordService } from './forgot-password.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  submitted: boolean;
  message: any;
  step: ForgotPasswordSteps;
  forgotPasswordSteps = ForgotPasswordSteps;
  twoFactorAuthEnabled: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private forgotPasswordService: ForgotPasswordService,
    private appService: AppService
  ) { }

  ngOnInit() {
    this.submitted = false;
    this.twoFactorAuthEnabled = false;
    this.step = ForgotPasswordSteps.submitEmail;
    this.message = null;
    this.forgotPasswordForm = this.formBuilder.group({
      email: [null, [
          Validators.required,
          Validators.email,
          Validators.pattern(/^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.(([a-zA-Z]{2,})|([a-zA-Z]+\.[a-zA-Z]{2,100}))$/)
        ]
      ],
      verificationCode: [null, [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(6)
        ]
      ],
      phone: [null, [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10)
        ]
      ],
      otp: [null, [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(6)
        ]
      ],
      password: [null, [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(128),
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!#%*?&._]{8,128}$/)
        ]
      ],
      confirmPassword: [null]
    });
  }

  checkVisibitity(step: ForgotPasswordSteps) {
    let steps = [];
    switch (step) {
      case ForgotPasswordSteps.submitEmail:
        steps = [
          ForgotPasswordSteps.submitEmail, ForgotPasswordSteps.verifyCode, ForgotPasswordSteps.submitPhone, ForgotPasswordSteps.verifyOtp, ForgotPasswordSteps.updatePassword
        ];
        break;
      case ForgotPasswordSteps.verifyCode:
        steps = [
          ForgotPasswordSteps.verifyCode, ForgotPasswordSteps.submitPhone, ForgotPasswordSteps.verifyOtp, ForgotPasswordSteps.updatePassword
        ];
        break;
      case ForgotPasswordSteps.submitPhone:
        steps = [
          ForgotPasswordSteps.submitPhone, ForgotPasswordSteps.verifyOtp, ForgotPasswordSteps.updatePassword
        ];
        break;
      case ForgotPasswordSteps.verifyOtp:
        steps = [ForgotPasswordSteps.verifyOtp, ForgotPasswordSteps.updatePassword];
        break;
      case ForgotPasswordSteps.updatePassword:
        steps = [ForgotPasswordSteps.updatePassword];
        break;
      default:
        steps = [];
    }
    return steps.includes(this.step);
  }

  submitEmail() {
    this.submitted = true;
    if (this.forgotPasswordForm.get('email').invalid) {
      return;
    }
    const payload = { step: this.step, ...this.forgotPasswordForm.value };
    this.forgotPasswordService.forgotPassword(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.result : res.message.errorMessage
        };

        if (res.success) {
          this.submitted = false;
          this.step = ForgotPasswordSteps.verifyCode;
        }
        this.appService.removeMessage(this, 'message');
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : MessageTypes.verificationCodeSentFailed
        };
      }
    );
  }

  submitVerificationCode() {
    this.submitted = true;
    if (this.forgotPasswordForm.get('verificationCode').invalid) {
      return;
    }
    const payload = { step: this.step, ...this.forgotPasswordForm.value };
    this.forgotPasswordService.forgotPassword(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success && res.result && res.result.message ? res.result.message : res.message.errorMessage
        };

        if (res.success) {
          this.submitted = false;
          this.step = res.result && res.result.twoFactorAuth ? ForgotPasswordSteps.submitPhone : ForgotPasswordSteps.updatePassword;
          this.twoFactorAuthEnabled = res.result && res.result.twoFactorAuth ? res.result.twoFactorAuth : false;
        }
        this.appService.removeMessage(this, 'message');
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : MessageTypes.verificationCodeVerificationFailed
        };
      }
    );
  }

  submitPhone() {
    this.submitted = true;
    if (this.forgotPasswordForm.get('phone').invalid) {
      return;
    }
    const payload = { step: this.step, ...this.forgotPasswordForm.value };
    this.forgotPasswordService.forgotPassword(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.result : res.message.errorMessage
        };

        if (res.success) {
          this.submitted = false;
          this.step = ForgotPasswordSteps.verifyOtp;
        }
        this.appService.removeMessage(this, 'message');
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : MessageTypes.verificationCodeSentFailed
        };
      }
    );
  }

  submitOTP() {
    this.submitted = true;
    if (this.forgotPasswordForm.get('otp').invalid) {
      return;
    }
    const payload = { step: this.step, ...this.forgotPasswordForm.value };
    this.forgotPasswordService.forgotPassword(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.result : res.message.errorMessage
        };

        if (res.success) {
          this.submitted = false;
          this.step = ForgotPasswordSteps.updatePassword;
        }
        this.appService.removeMessage(this, 'message');
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : MessageTypes.verificationCodeSentFailed
        };
      }
    );
  }

  updatePassword(): void {
    this.submitted = true;
    if (
      (
        this.twoFactorAuthEnabled && this.forgotPasswordForm.invalid
      ) ||
      (
        !this.twoFactorAuthEnabled &&
        (
          this.forgotPasswordForm.get('email').invalid ||
          this.forgotPasswordForm.get('verificationCode').invalid ||
          this.forgotPasswordForm.get('password').invalid
        )
      )
    ) {
      return;
    }
    const payload = { step: this.step, ...this.forgotPasswordForm.value, password: btoa(this.forgotPasswordForm.value.password) };
    this.forgotPasswordService.forgotPassword(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success && res.result && res.result.n ? res.message : res.message.errorMessage
        };

        if (res.success) {
          this.submitted = false;
          this.step = ForgotPasswordSteps.submitEmail;
          this.forgotPasswordForm.reset();
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : MessageTypes.verificationCodeSentFailed
        };
      }
    );
  }

}
