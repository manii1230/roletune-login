import { SiteModeratorGuard } from './../core/guards/site-moderator.guard';
import { AuthGuard } from './../core/guards/auth.guard';
import { EmptyLayoutComponent } from './../shared/components/app-layouts/empty-layout/empty-layout.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SiteAdminGuard } from '../core/guards/site-admin.guard';
import { MainLayoutComponent } from '../shared/components/app-layouts';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivateChild: [ AuthGuard ],
    children: [
      {
        path: 'home',
        component:  HomeComponent
      },
      {
        path: 'home/:username',
        component:  HomeComponent
      },
      {
        path: 'home/:username/:blogType',
        component:  HomeComponent
      },
      {
        path: 'communities',
        loadChildren: './communities/communities.module#CommunitiesModule'
      },
      {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule'
      },
      {
        path: 'interest',
        loadChildren: './interest/interest.module#InterestModule'
      },
      {
        path: 'mentorship',
        loadChildren: './mentorship/mentorship.module#MentorshipModule'
      },
      {
        path: 'shopping-complex',
        loadChildren: './shopping-complex/shopping-complex.module#ShoppingComplexModule'
      },
      {
        path: 'huddle-room',
        loadChildren: './huddle-room/huddle-room.module#HuddleRoomModule'
      },
      {
        path: 'notifications',
        loadChildren: './notifications/notifications.module#NotificationsModule'
      },
      {
        path: 'news-feeds',
        loadChildren: './news-feeds/news-feeds.module#NewsFeedsModule'
      },
      {
        path: 'settings',
        loadChildren: './settings/settings.module#SettingsModule'
      },
      {
        path: 'messages',
        data: {
          showMessagesSidebar: false
        },
        loadChildren: './messages/messages.module#MessagesModule'
      },
      {
        path: 'communities',
        loadChildren: './communities/communities.module#CommunitiesModule'
      },
      {
        path: 'moderator',
        canActivateChild: [ SiteModeratorGuard ],
        loadChildren: './moderator/moderator.module#ModeratorModule'
      },
      {
        path: 'admin',
        canActivateChild: [ SiteAdminGuard ],
        loadChildren: './admin/admin.module#AdminModule'
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: 'message-details',
    component: EmptyLayoutComponent,
    canActivateChild: [ AuthGuard ],
    children: [
      {
        path: '',
        data: {
          showMessagesSidebar: true
        },
        loadChildren: './messages/messages.module#MessagesModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/error/404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FrontRoutingModule { }
