import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) { }

  getFriendsList() {
    return this.http.get(`${environment.API_URL}user-service/friend-request-list`);
  }

  getUserDetails(username: string) {
    return this.http.get(`${environment.API_URL}user-service/username/${username}`);
  }
}
