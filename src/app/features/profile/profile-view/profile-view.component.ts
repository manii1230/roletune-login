import { FrontService } from '../../front.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from '../../../app.service';
import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.css']
})
export class ProfileViewComponent implements OnInit {
  user: any;
  showList: string;
  activeTab: string;
  friendList: any[];
  groupList: any[];
  loading: boolean;
  guestUser: any;
  message: any;
  constructor(private appService: AppService, private frontService: FrontService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.loading = true;
    this.message = null;
    this.showList = 'friends';
    this.activeTab = 'credentials';
    this.friendList = [];
    this.groupList = [];

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.route.paramMap.subscribe(params => {
          if (params.get('username') && user.username !== params.get('username')) {
            this.guestUser = user;
            this.loadUser(this.route.snapshot.paramMap.get('username'));
            return;
          }
          this.router.navigate(['main/profile']);
        });
      }
    });
  }

  loadUser(username: string): void {
    this.loading = true;
    const userPromise =  this.frontService.getUserDetails(username);
    const friendsPromise =  this.frontService.getFriendsList(username);
    const groupsPromise = this.frontService.getGroupList(username);

    forkJoin([userPromise, friendsPromise, groupsPromise]).subscribe(
      (res: any) => {
        if (res[0] && res[0].success) {
          this.user = res[0].result;
          this.user.createdAt = (this.user.createdAt) ? new Date(this.user.createdAt.substring(0, 10)) : '';
        }

        if (res[1] && res[1].result && res[1].result.length) {
          this.friendList = [];
          res[1].result.map(request => {
            if (request.requestStatus === 'APPROVED') {
              this.friendList.push(request);
            }
          });
        } else {
          this.friendList = [];
        }

        if (res[2] && res[2].result && res[2].result.length) {
          this.groupList = res[2].result;
        } else {
          this.groupList = [];
        }

        this.loading = false;
      },
      (err) => {
        this.loading = false;
        this.router.navigate(['auth/login']);
      }
    );
  }
}
