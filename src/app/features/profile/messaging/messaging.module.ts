import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessagingRoutingModule } from './messaging-routing.module';
import { MessagingComponent } from './messaging.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    MessagingComponent
  ],
  imports: [
    CommonModule,
    MessagingRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class MessagingModule { }
