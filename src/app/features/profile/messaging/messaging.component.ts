import { MessageTypes } from './../../../core/models/message-types';
import { Subject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { FrontService } from './../../front.service';
import { LoadMore } from './../../../core/base/load-more.base';
import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-messaging',
  templateUrl: './messaging.component.html',
  styleUrls: ['./messaging.component.css']
})
export class MessagingComponent extends LoadMore implements OnInit, OnDestroy {
  user: any;
  activeFriend: any;
  friendsFilter: string;
  friendsList: any[];
  audio: any;
  message: any;
  loadMoreDetails: any;
  friendsChatForm: FormGroup;
  unsubscribe = new Subject();

  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    private frontService: FrontService
  ) {
    super();
  }

  ngOnInit() {
    this.friendsFilter = '';
    this.friendsList = [];
    this.message = null;
    this.activeFriend = null;
    this.loadMoreDetails = {
      condition: 'LESSER',
      value: 5
    };
    this.audio = new Audio();
    this.audio.src = '../../../../assets/sounds/new-message.mp3';
    this.audio.load();

    this.friendsChatForm = this.formBuilder.group({
      message: ['', [Validators.required, Validators.minLength(1)]],
      fileName: [''],
      filePath: [''],
      attachedFile: [null]
    });

    this.appService.activeLoggedInUser.subscribe((user: any) => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.frontService.emitAction('register-user', this.user);
        this.loadFriendsList();

        this.frontService.getServerSentUpdates('new-message').subscribe(message => {
          if (!this.activeFriend || this.activeFriend.username !== message.fromUserId) {
            return;
          }
          if (!this.user.muted.includes(message.fromUserId)) {
            this.audio.play();
          }
          this.activeFriend.messages.push(message);
          this.scrollMessages('FULL');
        });
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  loadFriendsList() {
    this.config.loading = true;
    this.frontService.getFriendsWithCommunities(this.user.username).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        this.friendsList = res.result ?
          res.result.filter(reqItem => reqItem.request.requestStatus === 'APPROVED').concat(this.friendsList) : this.friendsList;
       // this.friendsList = this.friendsList.concat(this.friendsList, this.friendsList, this.friendsList);
      },
      (err: HttpErrorResponse) => {
        this.config.loading = false;
        this.friendsList = [];
        this.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
      }
    );
  }

  setActiveFriend(friend: any) {
    if (this.activeFriend && friend.username === this.activeFriend.username) {
      return;
    }

    this.config.loadMore = true;
    this.config.loading = false;
    this.activeFriend =  friend;
    this.activeFriend.messages = [];
    this.pageNumber = 1;
    this.fetchFriendMessages();
  }

  fetchFriendMessages() {
    this.config.loading = true;
    const payload = {
      username: this.activeFriend.username,
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };
    this.frontService.getAllChatMessages(payload).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.config.loadMore = !(res.result && res.result.length < this.pageSize);

        if (!res.success) {
          this.activeFriend.message = {
            status: res.success,
            message: res.success ? res.message : res.message.errorMessage
          };
        }
        this.activeFriend.messages = res.result ? res.result.concat(this.activeFriend.messages) : this.activeFriend.messages.concat;

        this.scrollMessages();
      },
      (err: HttpErrorResponse) => {
        this.activeFriend.message = {
          status: false,
          message: err.error && err.error.message ? err.error.message : err.message
        };
        this.config.loading = false;
        this.config.loadMore = false;
        this.activeFriend.messages = [];
      });
  }

  loadMoreEvent(event) {
    if (!this.config.loadMore || this.config.loading) {
      return;
    }
    
    this.config.loading = true;
    setTimeout(() => {
      this.pageNumber += 1;
      this.fetchFriendMessages();
    }, 2000);
  }

  scrollMessages(scrollType?: string) {
    setTimeout(() => {
      const messagesList = document.querySelector('.user-messages');
      messagesList.scrollTop = (this.pageNumber === 1 || scrollType) ? messagesList.scrollHeight : this.pageSize * 20;
    }, 0);
  }

  appendEmoji(activeFriend, emojiCode: number) {
    activeFriend.showIcons = !activeFriend.showIcons;
    this.friendsChatForm.patchValue({
      message: `${this.friendsChatForm.value.message ? this.friendsChatForm.value.message + ' ' : ''}${String.fromCodePoint(emojiCode)}`
    });
  }

  uploadFile(event: any): void {
    this.activeFriend.message = null;
    if (event.target && event.target.files.length) {
      const [fileToLoad] = event.target.files;
      if ((fileToLoad.size / 1024) < 10000) {
        this.friendsChatForm.patchValue({
          fileName: fileToLoad.name,
          attachedFile: fileToLoad
        });
      } else {
        this.activeFriend.message = {
          status: false,
          message: MessageTypes.chatFileSizeFailed
        };
      }
    }
  }

  sendChatMessage() {
    if (this.friendsChatForm.valid) {
      const payload = {
        fromUserId: this.user.username,
        toUserId: this.activeFriend.username,
        message: this.friendsChatForm.value,
        fileName: this.friendsChatForm.value.fileName,
        filePath: this.friendsChatForm.value.filePath
      };
      this.frontService.emitAction('new-chat-message', payload);
      const today = new Date();
      this.activeFriend.messages.push({
        fromUserId: this.user.username,
        toUserId: this.activeFriend.username,
        message: this.friendsChatForm.value.message,
        fileName: this.friendsChatForm.value.fileName,
        filePath: this.friendsChatForm.value.filePath,
        sentAt:
        // tslint:disable-next-line: max-line-length
        `${(today.getDate() < 9) ? '0' : ''}${(today.getDate())}-${((today.getMonth() + 1) < 9) ? '0' : ''}${(today.getMonth() + 1)}-${today.getFullYear()}::${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}::${today.getHours() > 12 ? 'PM' : 'AM'}`
      });
      this.scrollMessages('FULL');
      this.friendsChatForm.reset();
    }
  }

  checkMessageFile() {
    if (this.friendsChatForm.value.attachedFile) {
      this.frontService.uploadFile(this.friendsChatForm.value).subscribe(
        (res: any) => {
          if (res.result) {
            this.friendsChatForm.patchValue({
              fileName: res.result.fileName,
              filePath: res.result.filePath,
            });
          }
          return this.sendChatMessage();
        },
        (err: HttpErrorResponse) => {
          return this.activeFriend.message = {
            status: false,
            message: err.message
          };
        }
      );
      return;
    }
    this.sendChatMessage();
  }

}
