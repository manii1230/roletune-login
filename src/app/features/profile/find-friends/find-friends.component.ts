import { LoaderConfig } from './../../../core/models/loader-config';
import { AppService } from '../../../app.service';
import { FrontService } from '../../front.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-find-friends',
  templateUrl: './find-friends.component.html',
  styleUrls: ['./find-friends.component.css']
})
export class FindFriendsComponent implements OnInit {
  user: any;
  findFriendsForm: FormGroup;
  showResults: boolean;
  searchResults: any[];
  communities: any[];
  locations: string[];

  pageNumber: number;
  pageSize: number;
  config: LoaderConfig = {};

  friendRequests: any[];

  activeRequest: any;
  friendRequestForm: FormGroup;
  message: any;

  constructor(private appService: AppService, private formBuilder: FormBuilder, private frontService: FrontService) { }

  ngOnInit() {
    this.pageNumber = 1;
    this.pageSize = this.appService.pageSize;
    this.config.loading = false;
    this.config.loadmore = true;

    this.showResults = false;

    this.searchResults = [];
    this.findFriendsForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      location: [''],
      community: ['']
    });

    this.friendRequestForm = this.formBuilder.group({
      toUserId: ['', Validators.required],
      message: ['', [Validators.required, Validators.minLength(3)]]
    });

    this.locations = this.appService.getLocations();

    this.appService.activeLoggedInUser.subscribe((user: any) => {
      this.user = (user && Object.keys(user).length) ? user : {};
    });

    this.appService.communityListObservable.subscribe(communities => {
      this.communities = (communities && communities.length) ? communities : [];
    });

    this.appService.friendsListObservable.subscribe(friends => {
      this.friendRequests = (friends && friends.length) ? friends : [];
    });
  }

  findFriends() {
    this.config.loading = true;
    this.frontService.searchFriends(this.findFriendsForm.value, this.pageNumber, this.pageSize).subscribe(
        (res: any) => {
          this.showResults = true;
          this.config.loading = false;
          this.searchResults = this.searchResults.concat(res.result ? res.result : []);
          this.config.loadmore = (this.pageSize > res.result.length) ? false : true;
        },
        (err) => {

        }
      );
  }

  loadMoreFriends() {
    this.pageNumber += 1;
    this.findFriends();
  }

  sendRequest() {
    this.friendRequestForm.patchValue({
      toUserId: this.activeRequest.username
    });
    this.frontService.friendRequestAction(this.friendRequestForm.value).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        this.appService.removeMessage(this, 'message');

        if(res.success) {
          this.friendRequests.push({
            fromUserId: res.result.fromUserId,
            toUserId: res.result.toUserId,
            requestStatus: res.result.requestStatus,
            isRead: res.result.isRead,
            sendeddAt: res.result.sendeddAt,
            fromUserId_user_docs: this.user,
            toUserId_user_docs: {
              firstName: this.activeRequest.firstName,
              lastName: this.activeRequest.lastName,
              username: this.activeRequest.userame,
              profileImage: this.activeRequest.profileImage,
              location: this.activeRequest.location
            }
          });
          this.appService.updateFriendListObservable(this.friendRequests);
          setTimeout(() => {
            this.friendRequestForm.reset();
            this.activeRequest = null;
          }, 3000);
        }
      },
      (err) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  checkFriend(username: string) {
    return this.friendRequests.find(request => ((request.toUserId === username) || (request.fromUserId === username)));
  }

  resetRequest() {

  }
}
