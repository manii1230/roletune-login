import { ReactiveFormsModule } from '@angular/forms';
import { FindFriendsComponent } from './find-friends.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FindFriendsRoutingModule } from './find-friends-routing.module';

@NgModule({
  declarations: [
    FindFriendsComponent
  ],
  imports: [
    CommonModule,
    FindFriendsRoutingModule,
    ReactiveFormsModule
  ]
})
export class FindFriendsModule { }
