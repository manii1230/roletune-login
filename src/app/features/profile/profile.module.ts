import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { ProfileComponent } from './profile.component';
import { ShowcaseComponent } from './showcase/showcase.component';
import { ShowcaseViewComponent } from './showcase/showcase-view/showcase-view.component';
import { CredentialsComponent } from './showcase/credentials/credentials.component';
import { RecommendationsComponent } from './showcase/recommendations/recommendations.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { AddCredentialComponent } from './showcase/add-credential/add-credential.component';
import { ShowcaseService } from './showcase/showcase.service';
import { ProfileService } from './profile.service';
import { UserProfileDetailsComponent } from './user-profile-details/user-profile-details.component';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProfileViewComponent,
    ProfileComponent,
    ShowcaseComponent,
    ShowcaseViewComponent,
    CredentialsComponent,
    RecommendationsComponent,
    AddCredentialComponent,
    UserProfileDetailsComponent
  ],
  providers: [
    ProfileService,
    ShowcaseService
  ]
})
export class ProfileModule { }
