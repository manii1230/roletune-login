import { takeUntil } from 'rxjs/operators';
import { FrontService } from './../front.service';
import { Router, NavigationEnd } from '@angular/router';
import { AppService } from './../../app.service';
import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, AfterViewInit, OnDestroy {
  user: any;
  status: boolean;
  interval: any;
  showList: string;
  groupList: any[];
  friendList: any[];
  requestList: any[];
  carouselConfig: any;
  friendsCarousel: any[];
  loading: boolean;
  unsubscribe = new Subject();

  constructor(
    private appService: AppService,
    private frontService: FrontService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loading = true;
    this.status = true;
    this.interval = setInterval(() => {
      this.status = navigator.onLine;
    }, 1000);
    this.showList = 'friends';
    this.friendList = [];
    this.requestList = [];
    this.groupList = [];
    this.subscribeToRouter();
    this.appService.activeLoggedInUser.subscribe((user: any) => {
      this.user = user;
      if (user && Object.keys(user).length) {
        // tslint:disable-next-line:max-line-length
        this.user.createdAt = (user.createdAt && typeof(user.createdAt) !== 'string') ? new Date(user.createdAt.substring(0, 10)) : user.createdAt;

        this.appService.friendsListObservable.subscribe(friendslist => {

          // tslint:disable-next-line:max-line-length
          this.friendList = (friendslist && friendslist.length) ? friendslist.filter((friend: any) => friend.requestStatus === 'APPROVED') : [];

          this.friendsCarousel = this.groupSliderItems(this.friendList, 'friends');

          // tslint:disable-next-line:max-line-length
          this.requestList = (friendslist && friendslist.length) ? friendslist.filter((friend: any) => friend.requestStatus === 'PENDING') : [];
        });

        this.appService.groupListObservable.subscribe(groups => {
          this.groupList = groups ? groups : [];
        });
      }
    });
  }

  ngAfterViewInit() {
    this.hideLoader();
  }

  subscribeToRouter() {
    this.router.events.pipe(takeUntil(this.unsubscribe)).subscribe(event => {
      this.loading = true;
      if (event instanceof NavigationEnd) {
        this.hideLoader();
      }
    });
  }

  hideLoader() {
    setTimeout(() => {
      this.loading = false;
    }, 1000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  getRoleDetails() {
    const accessList = this.user.accessList;
    let roleDetails = '';
    if (accessList) {
      if (accessList.b2user && accessList.b2user.some(role => role === true)) {
        roleDetails += 'User';
      }
      if (accessList.c2moderator && accessList.c2moderator.some(role => role === true)) {
        roleDetails += ', Community Moderator';
      }
      if (accessList.s2moderator && accessList.s2moderator.some(role => role === true)) {
        roleDetails += ', Site Moderator';
      }
      if (accessList.superadmin && accessList.superadmin.some(role => role === true)) {
        roleDetails += ', Site Admin';
      }
    }
    return roleDetails;
  }

  respondRequest(action: string, userIdx: number) {
    switch (action) {
      case 'confirm':
        this.frontService.confirmRequest(this.requestList[userIdx].fromUserId).subscribe(
          (res: any) => {
            if (res.success) {
              this.requestList[userIdx].requestStatus = 'APPROVED';
              this.friendList.push(this.requestList[userIdx]);
              this.requestList.splice(userIdx, 1);
            }
          },
          (error) => {

          }
        );
        break;
      case 'delete':
        this.frontService.deleteRequest(this.requestList[userIdx].fromUserId).subscribe(
          (res: any) => {
            this.requestList.splice(userIdx, 1);
          },
          (error) => {

          }
        );
        break;
      case 'unfriend':
        this.frontService.unfriendRequest(this.friendList[userIdx].fromUserId).subscribe(
          (res: any) => {
            this.friendList.splice(userIdx, 1);
          },
          (error) => {

          }
        );
        break;
    }
  }

  groupSliderItems(list: any[], key: string): any[] {
    const sliderList = [];
    for ( let i = 0; i <= list.length - 1; i += 2) {
      const sliderObj = {};
      if (list[ i + 1 ]) {
        sliderObj[key] = [list[i], list[ i + 1 ]];
        sliderList.push(sliderObj);
      } else {
        sliderObj[key] = [list[i]];
        sliderList.push(sliderObj);
      }
    }
    return sliderList;
  }

  scrollSlider(scrollType: string, id: string) {
    const sliderTag = document.querySelector(`#${id}`) as HTMLDivElement;
    if (sliderTag.childElementCount <= 5) {
      return;
    }
    const slideTag = document.querySelector(`#${id} > div`) as HTMLDivElement;
    const slideWidth = (slideTag.clientWidth * 100) / sliderTag.clientWidth;
    switch (scrollType) {
      case 'right':
        if (Math.round((parseInt(sliderTag.style.marginLeft, 10) ? parseInt(sliderTag.style.marginLeft, 10) : 0 ) - slideWidth * 5) >= -100) {
          sliderTag.style.marginLeft = ((parseInt(sliderTag.style.marginLeft, 10) ? parseInt(sliderTag.style.marginLeft, 10) : 0 ) - slideWidth * 5) + '%';
        }
        break;
      case 'left':
        if (Math.round(parseInt(sliderTag.style.marginLeft, 10) ? parseInt(sliderTag.style.marginLeft, 10) : 0 ) < 0) {
          sliderTag.style.marginLeft = ((parseInt(sliderTag.style.marginLeft, 10) ? parseInt(sliderTag.style.marginLeft, 10) : 0 ) + slideWidth * 5) + '%';
        }
        break;
    }
  }
}
