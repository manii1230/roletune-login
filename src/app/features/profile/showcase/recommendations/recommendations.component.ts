import { StatusTypes } from './../../../../core/models/status-types.enum';
import { ShowcaseService } from './../showcase.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FrontService } from './../../../front.service';
import { AppService } from './../../../../app.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-recommendations',
  templateUrl: './recommendations.component.html',
  styleUrls: ['./recommendations.component.css']
})
export class RecommendationsComponent implements OnInit {
  @Input() user: any;
  guestUser: any;
  showEdit: boolean;
  communitiesList: any[];
  recommendationList: any[];
  recommendations: any[];
  categories: string[];
  recommendationsForm: FormGroup;
  activeRecommendationCommunity: number;
  message: any;
  loading: boolean;
  submitting: boolean;
  statusTypes = StatusTypes;

  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    private frontService: FrontService,
    private router: Router,
    private showcaseService: ShowcaseService
  ) { }

  ngOnInit() {
    this.message = null;
    this.loading = true;
    this.submitting = false;
    this.activeRecommendationCommunity = null;
    this.recommendationList = [];
    this.recommendationsForm = this.formBuilder.group({
      toUserId: [''],
      message: ['', [Validators.required, Validators.minLength(2)]],
      recommenderCommunity: ['']
    });

    this.showEdit = this.router.url.indexOf('recommendations') > 0;

    this.appService.activeLoggedInUser.subscribe((user: any) => {
        this.guestUser = this.user ? { ...this.user } : { ...user };
        if (user && Object.keys(user).length) {
          this.user = user;
          this.getUserRecommendations();
        }
    });
  }

  getUserRecommendations() {
    const [ , username] = this.router.url.split('view/');
    this.showcaseService.getRecommendations(username ? username : this.guestUser.username).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.formatRecommendations(res.result);
        } else {
          this.recommendationList = [];
        }
        this.loadCommunities(username ? username : null);
      },
      (err) => {
        this.recommendationList = [];
        this.loadCommunities(username ? username : null);
      }
    );
  }

  submitRecommendation() {
    this.submitting = true;
    this.recommendationsForm.patchValue({
      toUserId: this.guestUser.username,
      recommenderCommunity: this.recommendationsForm.value.recommenderCommunity ?
        this.recommendationsForm.value.recommenderCommunity : 'all'
    });
    this.showcaseService.addRecommendations(this.recommendationsForm.value).subscribe(
      (res: any) => {
        this.submitting = false;
        this.message = {
          type: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          this.recommendationsForm.reset();
          this.appService.removeMessage(this, 'message');
        }
      },
      (err) => {
        this.submitting = false;
        this.message = {
          type: false,
          message: err.message
        };
      }
    );
  }

  formatRecommendations(result: any[] = []) {
    const categories = [];
    result.forEach((item: any) => {
      if (categories.includes(item.recommenderCommunity)) {
        const idx = this.recommendationList.findIndex(i => i.community === item.recommenderCommunity);
        this.recommendationList[idx].recommendations.push(item);
      } else {
        categories.push(item.recommenderCommunity);
        this.recommendationList.push({
          community: item.recommenderCommunity,
          recommendations: [ item ]
        });
      }
    });
  }

  toggleActiveRecommendation(index: number): void {
    this.activeRecommendationCommunity = this.activeRecommendationCommunity === index ? null : index;
  }

  loadCommunities(username: string): void {
    this.appService.getCommunityList(username ? username : null).subscribe(
      (res: any) => {
        this.loading = false;
        const guestUserCommunities = this.appService.communityList.getValue();
        const communitiesList = res.result;
        this.communitiesList = guestUserCommunities.filter(
          community => communitiesList.find(com => community.communityId === com.communityId)
        );
      },
      (err) => {
        this.loading = false;
        this.communitiesList = [];
      }
    );
  }

  updateRecommendations(index: number, recommendationIndex: number, status: StatusTypes): void {
    this.submitting = true;
    const { recommendations } = this.recommendationList[index];
    const payload = { ...recommendations[recommendationIndex], recommenderStatus: status };
    this.showcaseService.updateRecommendations(payload).subscribe(
        (res: any) => {
          this.submitting = false;
          recommendations[recommendationIndex] = res.success ? payload : recommendations[recommendationIndex];
          this.message = {
            type: res.success,
            message: res.success ? res.message : res.message.errorMessage
          };
          this.appService.removeMessage(this, 'message');
        },
        (err) => {
          this.submitting = false;
          this.message = {
            type: false,
            message: err.message
          };
    });
  }

  deleteRecommendation(index: number, recommendationIndex: number): void {
    this.submitting = true;
    const { recommendations } = this.recommendationList[index];
    const { _id } = recommendations[recommendationIndex];
    this.showcaseService.deleteRecommendations(_id).subscribe(
        (res: any) => {
          this.submitting = false;
          if (res.success) {
            recommendations.splice(recommendationIndex, 1);
          }
          this.message = {
            type: res.success,
            message: res.success ? res.message : res.message.errorMessage
          };
          this.appService.removeMessage(this, 'message');
        },
        (err) => {
          this.submitting = false;
          this.message = {
            type: false,
            message: err.message
          };
    });
  }
}
