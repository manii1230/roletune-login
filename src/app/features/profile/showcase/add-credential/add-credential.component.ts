import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CredentialForms } from '../credential-forms/credential-form.base';
import { CredentialTypes, CredentialModes } from './../../../../core/models/credential-types.model';
import { AppService } from './../../../../app.service';
import { ShowcaseService } from '../showcase.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-credential',
  templateUrl: './add-credential.component.html',
  styleUrls: ['./add-credential.component.css']
})
export class AddCredentialComponent extends CredentialForms implements OnInit {
  @Input() credentialType: CredentialTypes;
  @Input() credentialMode: CredentialModes;
  @Input() credential: any;
  @Output() hideForm = new EventEmitter();
  @Output() updateUserCredentials: EventEmitter<any> = new EventEmitter<any>();

  message: any;
  credentialModes = CredentialModes;
  credentialTypes = CredentialTypes;
  submitted: boolean;

  constructor(public formBuilder: FormBuilder, private appService: AppService, private showcaseService: ShowcaseService) {
    super(formBuilder);
  }

  ngOnInit() {
    this.message = null;
    this.submitted = false;
    if (this.credentialMode === CredentialModes.edit && this.credential) {
      this.populateForm();
    }
  }

  resetCredentials(credentialType?: any) {
    this.credentialType = credentialType ? credentialType : this.credentialType;
  }

  toggleForm(data: any): void {
    data = data ? { ...data, credentialMode: this.credentialMode, credentialType: this.credentialType } : {};
    this.hideForm.emit(data);
  }

  getCredentialHeading() {
    switch (this.credentialType) {
      case CredentialTypes.about:
        return `${this.credentialMode} ${CredentialTypes.about}`;
      case CredentialTypes.education:
        return `${this.credentialMode} ${CredentialTypes.education}`;
      case CredentialTypes.experience:
        return `${this.credentialMode} ${CredentialTypes.experience}`;
      case CredentialTypes.certification:
        return `${this.credentialMode} ${CredentialTypes.certification}`;
      case CredentialTypes.skill:
        return `${this.credentialMode} ${CredentialTypes.skill}`;
      case CredentialTypes.award:
        return `${this.credentialMode} ${CredentialTypes.award}`;
      case CredentialTypes.patent:
        return `${this.credentialMode} ${CredentialTypes.patent}`;
      case CredentialTypes.project:
        return `${this.credentialMode} ${CredentialTypes.project}`;
      case CredentialTypes.publication:
        return `${this.credentialMode} ${CredentialTypes.publication}`;
    }
  }

  addCredential() {
    this.submitted = true;
    let payload = null;
    switch (this.credentialType) {
      case CredentialTypes.about:
        if (this.aboutDetailsForm.valid) {
          payload = { credentialType: this.credentialType, ...this.aboutDetailsForm.value };
          this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
        }
        break;
      case CredentialTypes.experience:
        if (this.experienceDetailsForm.valid) {
          if (!this.experienceDetailsForm.get('currentCompany').value) {
            if(
              (this.experienceDetailsForm.get('startYear').value < this.experienceDetailsForm.get('endYear').value) ||
              (
                this.experienceDetailsForm.get('startYear').value === this.experienceDetailsForm.get('endYear').value &&
                this.experienceDetailsForm.get('startMonth').value < this.experienceDetailsForm.get('endMonth').value
              )
            ) {
              payload = { credentialType: this.credentialType, ...this.experienceDetailsForm.value };
              this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
            }
          } else {
            payload = { credentialType: this.credentialType, ...this.experienceDetailsForm.value };
            this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
          }
        }
        break;
      case CredentialTypes.education:
        if (
          this.educationDetailsForm.valid &&
          (this.educationDetailsForm.get('start').value < this.educationDetailsForm.get('end').value) 
        ) {
          payload = { credentialType: this.credentialType, ...this.educationDetailsForm.value };
          this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
        }
        break;
      case CredentialTypes.certification:
        if (this.certificationDetailsForm.valid) {
          payload = { credentialType: this.credentialType, ...this.certificationDetailsForm.value };
          this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
        }
        break;
      case CredentialTypes.skill:
        if (this.skillDetailsForm.valid) {
          payload = { credentialType: this.credentialType, ...this.skillDetailsForm.value };
          this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
        }
        break;
      case CredentialTypes.award:
        if (this.awardsDetailsForm.valid) {
          payload = { credentialType: this.credentialType, ...this.awardsDetailsForm.value };
          this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
        }
        break;
      case CredentialTypes.patent:
        if (this.patentDetailsForm.valid) {
          payload = { credentialType: this.credentialType, ...this.patentDetailsForm.value };
          this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
        }
        break;
      case CredentialTypes.project:
        if(this.projectDetailsForm.valid) {
          if (this.projectDetailsForm.get('startYear').value && this.projectDetailsForm.get('startMonth').value) {
            if (this.projectDetailsForm.get('endYear').value && this.projectDetailsForm.get('endMonth').value) {
              if(
                  (this.projectDetailsForm.get('startYear').value < this.projectDetailsForm.get('endYear').value) ||
                  (
                    (this.projectDetailsForm.get('startYear').value === this.projectDetailsForm.get('endYear').value) &&
                    (this.projectDetailsForm.get('startMonth').value < this.projectDetailsForm.get('endMonth').value)
                  )
              ) {
                payload = { credentialType: this.credentialType, ...this.projectDetailsForm.value };
                this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
              }
            } else {
              payload = { credentialType: this.credentialType, ...this.projectDetailsForm.value };
              this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
            }
          } else {
            payload = { credentialType: this.credentialType, ...this.projectDetailsForm.value };
            this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
          }
        }
        break;
      case CredentialTypes.publication:
        if (this.publicationDetailsForm.valid) {
          payload = { credentialType: this.credentialType, ...this.publicationDetailsForm.value };
          this.credential ? this.updateCredential(payload) : this.saveCredential(payload);
        }
        break;
      default:
        
    }
  }

  populateForm() {
    switch (this.credentialType) {
      case CredentialTypes.about:
        this.aboutDetailsForm.patchValue({
          summary: this.credential.summary
        });
        break;
      case CredentialTypes.education:
        this.educationDetailsForm.patchValue({
          school: this.credential.school,
          degree: this.credential.degree,
          field: this.credential.field,
          start: this.credential.start,
          end: this.credential.end,
          grade: this.credential.grade,
          description: this.credential.description,
          attachedFile: this.credential.attachedFile,
          fileName: this.credential.fileName,
          fileType: this.credential.fileType,
        });
        break;
      case CredentialTypes.experience:
        this.experienceDetailsForm.patchValue({
          title: this.credential.title,
          type: this.credential.type,
          company: this.credential.company,
          location: this.credential.location,
          currentCompany: this.credential.currentCompany,
          startMonth: this.credential.startMonth,
          startYear: this.credential.startYear,
          endMonth: this.credential.endMonth,
          endYear: this.credential.endYear,
          industry: this.credential.industry,
          headline: this.credential.headline,
          description: this.credential.description,
          attachedFile: this.credential.attachedFile,
          fileName: this.credential.fileName,
          fileType: this.credential.fileType,
        });
        break;
      case CredentialTypes.certification:
        this.certificationDetailsForm.patchValue({
          name: this.credential.name,
          organization: this.credential.organization,
          expire: this.credential.expire,
          issueMonth: this.credential.issueMonth,
          issueYear: this.credential.issueYear,
          endMonth: this.credential.endMonth,
          endYear: this.credential.endYear,
          credentialId: this.credential.credentialId,
          credentialUrl: this.credential.credentialUrl
        });
        break;
      case CredentialTypes.skill:
        const skillGroup = this.formBuilder.group({
          name: [this.credential.name, [Validators.required, Validators.minLength(1)]]
        });
        this.skillDetailsForm.patchValue({
          skills: this.formBuilder.array([skillGroup])
        });
        break;
      case CredentialTypes.award:
        this.awardsDetailsForm.patchValue({
          title: this.credential.title,
          associated: this.credential.associated,
          issuer: this.credential.issuer,
          issueMonth: this.credential.issueMonth,
          issueYear: this.credential.issueYear,
          description: this.credential.description
        });
        break;
      case CredentialTypes.patent:
        this.patentDetailsForm.patchValue({
          title: this.credential.title,
          office: this.credential.office,
          number: this.credential.number,
          status: this.credential.status,
          month: this.credential.month,
          year: this.credential.year,
          url: this.credential.url,
          description: this.credential.description,
          attachedFile: this.credential.attachedFile,
          fileName: this.credential.fileName,
          fileType: this.credential.fileType
        });
        break;
      case CredentialTypes.project:
        this.projectDetailsForm.patchValue({
          name: this.credential.name,
          current: this.credential.current,
          startMonth: this.credential.startMonth,
          startYear: this.credential.startYear,
          endMonth: this.credential.endMonth,
          endYear: this.credential.endYear,
          associated: this.credential.associated,
          url: this.credential.url,
          description: this.credential.description,
        });
        break;
      case CredentialTypes.publication:
        this.publicationDetailsForm.patchValue({
          title: this.credential.title,
          publisher: this.credential.publisher,
          month: this.credential.month,
          year: this.credential.year,
          url: this.credential.url,
          description: this.credential.description,
        });
        break;
      default:
        
    }
  }

  uploadFile(event: any, credential: CredentialTypes) {
    if (event.target && event.target.files[0]) {
      const file = {
        fileName: event.target.files[0].name,
        attachedFile: event.target.files[0],
        fileType: event.target.files[0].type
      };
      switch (credential) {
        case CredentialTypes.education:
          this.educationDetailsForm.patchValue(file);
          break;
        case CredentialTypes.experience:
          this.experienceDetailsForm.patchValue(file);
          break;
      }
    }
  }

  saveCredential(payload: any) {
    this.showcaseService.addCredential(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };

        if (res.success) {
          setTimeout(() => {
            this.toggleForm(res.result);
          }, 3000);
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : err.message
        };
      }
    );
  }

  updateCredential(payload: any) {
    payload._id = this.credential._id;
    this.showcaseService.updateCredential(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };

        if (res.success) {
          setTimeout(() => {
            this.toggleForm(res.result);
          }, 3000);
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : err.message
        };
      }
    );
  }

  deleteCredential() {
    this.showcaseService.deleteCredential(this.credential).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };

        if (res.success) {
          setTimeout(() => {
            this.credentialMode = CredentialModes.delete;
            this.toggleForm(res.result);
          }, 3000);
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : err.message
        };
      }
    );
  }
}
