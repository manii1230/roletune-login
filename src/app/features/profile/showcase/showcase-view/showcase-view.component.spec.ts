import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcaseViewComponent } from './showcase-view.component';

describe('ShowcaseViewComponent', () => {
  let component: ShowcaseViewComponent;
  let fixture: ComponentFixture<ShowcaseViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowcaseViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowcaseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
