import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showcase-view',
  templateUrl: './showcase-view.component.html',
  styleUrls: ['./showcase-view.component.css']
})
export class ShowcaseViewComponent implements OnInit {
  activeTab: string;
  constructor() { }

  ngOnInit() {
    this.activeTab = 'credentials';
  }

}
