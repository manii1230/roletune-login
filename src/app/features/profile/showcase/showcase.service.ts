import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShowcaseService {

  constructor(private http: HttpClient) { }

  getFormData(form: any) {
    const formData =  new FormData();
    for (const key in form) {
      if (form.hasOwnProperty(key)) {
        formData.append( key, form[key]);
      }
    }
    return formData;
  }

  addRecommendations(recommendation: any) {
    return this.http.post(`${environment.API_URL}user-service/user-recommendation`, recommendation);
  }

  getRecommendations(username?: string) {
    let params = new HttpParams();
    if (username) {
      params = params.set('userId', username);
    }
    return this.http.get(`${environment.API_URL}user-service/user-recommendation/`, { params });
  }

  updateRecommendations(recommendation: any) {
    return this.http.patch(`${environment.API_URL}user-service/user-recommendation`, recommendation);
  }

  deleteRecommendations(id: string) {
    let params = new HttpParams();
    params = params.set('_id', id);
    return this.http.delete(`${environment.API_URL}user-service/user-recommendation`, { params });
  }

  getCredentials(user?: any) {
    const queryString = user ? `userId=${user.username}` : ``;
    return this.http.get(`${environment.API_URL}user-credentials-service/credentials?${queryString}`);
  }

  addCredential(credential) {
    const credentialData = credential.attachedFile ? this.getFormData(credential) : credential;
    return this.http.post(`${environment.API_URL}user-credentials-service/credentials`, credentialData);
  }

  updateCredential(credential: any) {
    const credentialData = credential.attachedFile ? this.getFormData(credential) : credential;
    return this.http.patch(`${environment.API_URL}user-credentials-service/credentials/${credential._id}`, credential);
  }

  deleteCredential(credential: any) {
    return this.http.delete(`${environment.API_URL}user-credentials-service/credentials/${credential._id}`);
  }
}
