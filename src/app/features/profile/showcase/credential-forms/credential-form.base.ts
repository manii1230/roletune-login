import { getYears, getMonths, employmentTypes, Month } from './../../../../core/config/';
import { CredentialTypes } from './../../../../core/models/credential-types.model';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';

export class CredentialForms {
    formType: CredentialTypes;
    showLoader: boolean;
    years: number[];
    months: Month[];
    employmentTypes: any[];

    skillsList: FormArray;
    aboutDetailsForm: FormGroup;
    educationDetailsForm: FormGroup;
    experienceDetailsForm: FormGroup;
    certificationDetailsForm: FormGroup;
    skillDetailsForm: FormGroup;
    awardsDetailsForm: FormGroup;
    patentDetailsForm: FormGroup;
    projectDetailsForm: FormGroup;
    publicationDetailsForm: FormGroup;

    constructor(public formBuilder: FormBuilder) {
        this.showLoader = true;
        this.skillsList = null;
        this.employmentTypes = employmentTypes;
        this.years = getYears().reverse();
        this.months = getMonths();
        this.initializeForm();
    }

    initializeForm() {
        this.aboutDetailsForm = this.formBuilder.group({
            summary: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(500)]]
        });

        this.educationDetailsForm = this.formBuilder.group({
            school: [null, Validators.required],
            degree: [null, Validators.required],
            field: [null],
            start: [null, Validators.required],
            end: [null, Validators.required],
            grade: [null],
            description: [null],
            attachedFile: [null],
            fileName: [null],
            fileType: [null],
        });

        this.experienceDetailsForm = this.formBuilder.group({
            title: [null, Validators.required],
            type: [null],
            company: [null, Validators.required],
            location: [null],
            currentCompany: [null],
            startMonth: [null, Validators.required],
            startYear: [null, Validators.required],
            endMonth: [null],
            endYear: [null],
            industry: [null, Validators.required],
            headline: [null, Validators.required],
            description: [null],
            attachedFile: [null],
            fileName: [null],
            fileType: [null],
        });

        this.certificationDetailsForm = this.formBuilder.group({
            name: [null, Validators.required],
            organization: [null, Validators.required],
            expire: [null],
            issueMonth: [null, Validators.required],
            issueYear: [null, Validators.required],
            endMonth: [null],
            endYear: [null],
            credentialId: [null],
            credentialUrl: [null]
        });

        this.skillDetailsForm = this.formBuilder.group({
            skills: this.formBuilder.array([])
        });

        this.awardsDetailsForm = this.formBuilder.group({
            title: [null, Validators.required],
            associated: [null],
            issuer: [null],
            issueMonth: [null, Validators.required],
            issueYear: [null, Validators.required],
            description: [null]
        });

        this.patentDetailsForm = this.formBuilder.group({
            title: [null, Validators.required],
            office: [null, Validators.required],
            number: [null, Validators.required],
            status: [null],
            month: [null],
            year: [null],
            url: [null],
            description: [null],
            attachedFile: [null],
            fileName: [null],
            fileType: [null],
        });

        this.projectDetailsForm = this.formBuilder.group({
            name: [null, Validators.required],
            current: [null],
            startMonth: [null],
            startYear: [null],
            endMonth: [null],
            endYear: [null],
            associated: [null],
            url: [null],
            description: [null],
        });

        this.publicationDetailsForm = this.formBuilder.group({
            title: [null, Validators.required],
            publisher: [null],
            month: [null, Validators.required],
            year: [null, Validators.required],
            url: [null],
            description: [null]
        });


        this.showLoader = false;
    }

    addSkill() {
        this.skillsList = this.skillDetailsForm.get('skills') as FormArray;
        this.skillsList.push(this.createSkill());
    }

    createSkill() {
        return this.formBuilder.group({
            name: [null, [Validators.required, Validators.minLength(1)]]
        });
    }

    removeSkill(idx: number) {
        this.skillsList.removeAt(idx);
    }
}
