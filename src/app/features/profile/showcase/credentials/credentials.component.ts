import { CredentialTypes, CredentialModes } from './../../../../core/models/credential-types.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppService } from './../../../../app.service';
import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { ShowcaseService } from '../showcase.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Month, getMonths } from 'src/app/core/config';

@Component({
  selector: 'app-credentials',
  templateUrl: './credentials.component.html',
  styleUrls: ['./credentials.component.css']
})
export class CredentialsComponent implements OnInit {
  @Input() user: any;
  modifyCredentials: boolean;
  showAddCredentialForm: boolean;

  message: any;
  credentialMessage: any;
  months: Month[];
  guestView: boolean;
  userCredentials: any[];
  selectedCredential: any;
  credentialType: any;
  credentialMode: any;
  credentialTypes = CredentialTypes;
  credentialModes = CredentialModes;

  constructor(
    private router: Router,
    private appService: AppService,
    private showcaseService: ShowcaseService
  ) { }

  ngOnInit() {
    this.guestView = this.user ? true : false;
    this.modifyCredentials = (this.router.url.indexOf('credentials') > 0) ? true : false;
    this.months = getMonths();
    this.credentialType = null;
    this.selectedCredential = null;
    this.message = null;
    this.credentialMessage = null;
    this.showAddCredentialForm = false;
    this.userCredentials = [];

    if (!this.user) {
      this.appService.activeLoggedInUser.subscribe((user: any) => {
        this.user = user ? user : {};
        this.loadCredentials();
      });
    } else {
      this.loadCredentials(this.user);
    }
  }

  openCredentialPopup(credentialType: any, credential?: any) {
    this.selectedCredential = this.credentialMode === this.credentialModes.edit ? credential : null;
    this.credentialType = credentialType;
    this.showAddCredentialForm = true;
  }

  deleteCredential(credentialType: any, credential: any) {
    this.credentialMessage  = null;
    this.showcaseService.deleteCredential(credential).subscribe(
      (res: any) => {
        this.credentialMessage = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };

        if (res.success) {
          const event = {
            ...credential,
            credentialMode: CredentialModes.delete,
            credentialType: credentialType
          };
          this.updateUserCredentials(event);
          this.appService.removeMessage(this, 'credentialMessage');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : err.message
        };
      }
    );
  }

  getCredentials(type: CredentialTypes): any[] {
    const credentialDetails: any =  this.userCredentials.find(credential => credential.credentialType === type);
    return credentialDetails && credentialDetails.credentials ? credentialDetails.credentials : [];
  }

  loadCredentials(user?: any) {
    this.showcaseService.getCredentials(user? user: null).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage 
        };
        this.userCredentials = res.result;
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.errorMessage ? err.error.errorMessage : err.message
        };
      }
    );
  }

  getMonthName(value: number): string {
    const month = this.months.find(monthItem => monthItem.value === value);
    return month && month.label ? month.label : ''; 
  }

  updateUserCredentials(event: any): void {
    this.credentialType = null;
    this.credentialMode = null;
    this.showAddCredentialForm = false;
    let userCredentials =  this.userCredentials.find(userCredential => userCredential.credentialType === event.credentialType);
    if (!userCredentials) {
      userCredentials = {
        credentialType: event.credentialType,
        credentials: []
      };      
      this.userCredentials.push(userCredentials);
    }
    switch(event.credentialMode) {
      case this.credentialModes.add:
        if (event.credentialType === this.credentialTypes.skill) {
          const skills = Object.values(event).filter(item => typeof(item) !== typeof(''))
          userCredentials.credentials.push(...skills);
          break;
        } 
        userCredentials.credentials.push(event);
        break;
      case this.credentialModes.edit:
        const idx = userCredentials.credentials.findIndex(userCredential => userCredential._id === event._id);
        userCredentials.credentials[idx] = event;
        break;
      case this.credentialModes.delete:
        const index = userCredentials.credentials.findIndex(userCredential => userCredential._id === event._id);
        userCredentials.credentials.splice(index, 1);  
      }
  }

  credentialsModifiable() : boolean {
    if (this.guestView) {
      return false;
    }
    return this.modifyCredentials;
  }
}
