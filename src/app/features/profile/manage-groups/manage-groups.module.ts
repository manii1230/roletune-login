import { ReactiveFormsModule } from '@angular/forms';
import { ManageGroupsComponent } from './manage-groups.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageGroupsRoutingModule } from './manage-groups-routing.module';

@NgModule({
  declarations: [
    ManageGroupsComponent
  ],
  imports: [
    CommonModule,
    ManageGroupsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ManageGroupsModule { }
