import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { FrontService } from '../../front.service';
import { AppService } from '../../../app.service';
import { Component, OnInit, AfterViewInit, OnDestroy, AfterContentChecked } from '@angular/core';
declare const $: any;

@Component({
  selector: 'app-manage-groups',
  templateUrl: './manage-groups.component.html',
  styleUrls: ['./manage-groups.component.css']
})
export class ManageGroupsComponent implements OnInit, OnDestroy {
  user: any;
  showGroupForm: boolean;
  groupForm: FormGroup;
  groupList: any[];
  friendList: any[];
  activeGroup: any;
  activeFriend: any;
  groupCreationMessage: any;
  groupActionMessage: any;
  activeMember: any;
  friendsWithCommunities: any[];
  unsubscribe = new Subject();

  constructor(private appService: AppService, private frontService: FrontService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.showGroupForm = false;
    this.friendList = [];
    this.groupList = [];

    this.groupCreationMessage = null;
    this.groupActionMessage = null;

    this.activeMember = null;
    this.friendsWithCommunities = [];

    this.groupForm = this.formBuilder.group({
      groupName: ['', [Validators.required, Validators.minLength(3)]],
      members:  this.formBuilder.array([])
    });

    this.appService.activeLoggedInUser.pipe(takeUntil(this.unsubscribe)).subscribe((user: any) => {
      this.user = user;

      if (user && Object.keys(user).length) {
        this.appService.friendsListObservable.pipe(takeUntil(this.unsubscribe)).subscribe(friends => {
          this.friendList = friends ? friends.filter(request => request.requestStatus === 'APPROVED') : [];
        });

        this.appService.groupListObservable.pipe(takeUntil(this.unsubscribe)).subscribe(groups => {
          this.groupList = groups ? groups.map(group => { group.showAccordion = false; return group; }) : [];
        });

        this.frontService.getFriendsWithCommunities(this.user.username).pipe(takeUntil(this.unsubscribe)).subscribe(
          (res: any) => {
            if (res.success) {
              this.friendsWithCommunities = this.formatResponse(res.result, this.groupList);
            }
          },
          (err: HttpErrorResponse) => {
            this.groupActionMessage = {
              status: false,
              message: err.message
            };
            this.appService.removeMessage(this, 'groupActionMessage');
          }
        );
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  formatResponse(friendsList: any[], groupsList: any[]): any[] {
    if (!groupsList) {
      return friendsList.map(friend => {
        friend.groups = [];
        return friend;
      });
    }
    return friendsList.map(friend => {
      friend.groups = groupsList
        .filter(group => group.members && group.members.find(member => member.username === friend.username))
        .map(group => group.groupName);
      return friend;
    });
  }

  getRoleDetails() {
    const accessList = this.user.accessList;
    let roleDetails = '';
    if (accessList) {
      if (accessList.b2user && accessList.b2user.some(role => role === true)) {
        roleDetails += 'User';
      }
      if (accessList.c2moderator && accessList.c2moderator.some(role => role === true)) {
        roleDetails += ', Community Moderator';
      }
      if (accessList.s2moderator && accessList.s2moderator.some(role => role === true)) {
        roleDetails += ', Site Moderator';
      }
      if (accessList.superadmin && accessList.superadmin.some(role => role === true)) {
        roleDetails += ', Site Admin';
      }
    }
    return roleDetails;
  }

  createGroup() {
    this.frontService.createGroup(this.groupForm.value).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        if (res.result) {
          this.appService.updateGroupListObservable(this.groupList.concat(res.result.group));
          this.groupForm.reset();
          this.showGroupForm = false;
        }
        this.groupCreationMessage = {
          type: res.result,
          message: res.result ? res.message : res.message.errorMessage
        };

        setTimeout(() => {
          this.groupCreationMessage = null;
        }, 5000);
      },
      (err) => {

      }
    );
  }

  groupAction(action: string, groupIdx: number) {
    switch (action) {
      case 'remove':
        this.frontService.deleteGroup(this.groupList[groupIdx]._id).pipe(takeUntil(this.unsubscribe)).subscribe(
          (res: any) => {
            if (res.success) {
              this.friendsWithCommunities.forEach(friend => {
                friend.groups = friend.groups.filter(group => group !== this.groupList[groupIdx].groupName);
              });
              this.groupList.splice(groupIdx, 1);
              this.appService.updateGroupListObservable(this.groupList);
              this.activeGroup = null;
            }

            this.updateGroupActionMessage(res);
          },
          err => {

        });
        break;
      case 'add':
        this.activeGroup = this.groupList[groupIdx];
        break;
    }
  }

  addGroupMember(action: string, idx: number) {
    this.activeFriend = this.friendList[idx];
    const friend = (this.user.username === this.activeFriend.fromUserId_user_docs.username) ? this.activeFriend.toUserId_user_docs : this.activeFriend.fromUserId_user_docs;
    this.activeGroup.members.push({
      firstName: friend.firstName,
      lastName: friend.lastName,
      username: friend.username,
      profileImage: friend.profileImage
    });

    const groupIdx = this.groupList.findIndex(group => group.groupName === this.activeGroup.groupName);
    this.groupList[groupIdx].members = this.activeGroup.members;
    this.frontService.updateGroup(this.activeGroup).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        if (res.success) {
          const friendItemIndex = this.friendsWithCommunities.findIndex(friendItem => friendItem.username === friend.username);
          if (friendItemIndex >= 0) {
            this.friendsWithCommunities[friendItemIndex].groups.push(this.activeGroup.groupName);
          }
          this.appService.updateGroupListObservable(this.groupList);
          this.activeGroup = null;
          this.activeMember = null;
        }

        this.updateGroupActionMessage(res);
      },
      err => {

    });
  }

  removeMember(groupIdx: number, memberIdx: number) {
    const group = this.groupList[groupIdx];
    const groupMember = group.members.splice(memberIdx, 1);
    this.frontService.updateGroup(group).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        if (res.success) {
          const friendItemIndex = this.friendsWithCommunities.findIndex(friendItem => friendItem.username === groupMember.username);
          if (friendItemIndex >= 0) {
            this.friendsWithCommunities[friendItemIndex].groups =
              this.friendsWithCommunities[friendItemIndex].groups.filter(
                (groupItem: string) => groupItem !== this.groupList[groupIdx].groupName
              );
          }
          this.groupList[groupIdx] = group;
          this.appService.updateGroupListObservable(this.groupList);
          this.activeMember = null;
        }
        this.updateGroupActionMessage(res);
      },
      err => {

    });
  }

  resetActive() {
    this.activeFriend = null;
    this.activeGroup = null;
    this.activeMember = null;
  }

  setActiveMember(groupIndex: number, memberIndex: number, action: string) {
    this.activeMember = {
      action,
      groupIndex,
      groupName: ((groupIndex !== null) && this.groupList[groupIndex]) ? this.groupList[groupIndex].groupName : this.activeGroup.groupName,
      memberIndex,
      member: (
        (groupIndex !== null) && this.groupList[groupIndex] && (memberIndex !== null) && this.groupList[groupIndex].members[memberIndex])
          ? this.groupList[groupIndex].members[memberIndex].firstName + ' ' + this.groupList[groupIndex].members[memberIndex].lastName
          : ( (this.friendList[memberIndex].fromUserId === this.user.username)
            ? (this.friendList[memberIndex].toUserId_user_docs.firstName + ' ' + this.friendList[memberIndex].toUserId_user_docs.lastName)
            : (this.friendList[memberIndex].fromUserId_user_docs.firstName + ' ' + this.friendList[memberIndex].fromUserId_user_docs.lastName))
    };
  }

  isUserGroupMember(i) {
    const friend = (this.user.username === this.friendList[i].fromUserId_user_docs.username) ? this.friendList[i].toUserId_user_docs : this.friendList[i].fromUserId_user_docs;
    return this.activeGroup.members.findIndex(member => member.username === friend.username) >= 0;
  }

  toggleAccordion(idx: number): void {
    this.groupList.forEach((group, i) => {
      if (i === idx) {
        group.showAccordion = !group.showAccordion;
      } else {
        group.showAccordion = false;
      }
    });
  }

  updateGroupActionMessage(res: any) {
    this.groupActionMessage = {
      type: res.success,
      message: res.message
    };

    setTimeout(() => {
      this.groupActionMessage = null;
    }, 3000);
  }

  getFriendAssociatedList(username: string, prop: string): string[] {
    const friendItem = this.friendsWithCommunities.find(friend => friend.username === username);
    return (friendItem && friendItem[prop] && friendItem[prop].length) ? friendItem[prop] : ['None'];
  }
}
