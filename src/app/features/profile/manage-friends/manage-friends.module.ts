import { ReactiveFormsModule } from '@angular/forms';
import { ManageFriendsComponent } from './manage-friends.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageFriendsRoutingModule } from './manage-friends-routing.module';

@NgModule({
  declarations: [
    ManageFriendsComponent
  ],
  imports: [
    CommonModule,
    ManageFriendsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ManageFriendsModule { }
