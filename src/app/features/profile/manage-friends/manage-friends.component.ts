import { CardTypes } from './../../../core/models/card-types.model';
import { HttpErrorResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FrontService } from '../../front.service';
import { AppService } from '../../../app.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { RequestTypes } from 'src/app/core/models/request-types.model';
import { ReactionTypes } from 'src/app/core/models/reaction-types.model';

@Component({
  selector: 'app-manage-friends',
  templateUrl: './manage-friends.component.html',
  styleUrls: ['./manage-friends.component.css']
})
export class ManageFriendsComponent implements OnInit, OnDestroy {
  user: any;
  requestList: any[];
  unsubscribe = new Subject();
  cardTypes: any;
  requestTypes: any;
  activeCard: CardTypes;

  confirmAction: any;

  message: any;
  groupList: any[];
  friendsList: any[];

  constructor(private appService: AppService, private frontService: FrontService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.requestList = [];
    this.message = null;
    this.confirmAction = null;
    this.cardTypes = CardTypes;
    this.requestTypes = RequestTypes;
    this.groupList = [];
    this.friendsList = [];

    this.appService.activeLoggedInUser.subscribe((user: any) => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.appService.friendsListObservable.pipe(takeUntil(this.unsubscribe)).subscribe(friendsList => {
          this.friendsList = friendsList ? friendsList : [];
        });
        this.appService.groupListObservable.pipe(takeUntil(this.unsubscribe)).subscribe(groupsList => {
          this.groupList = groupsList ? groupsList : [];
        });
        this.getFriendsAndGroups();
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  getFriendsAndGroups() {
    this.frontService.getFriendsWithCommunities(this.user.username).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        if (res.success) {
          this.requestList = this.formatResponse(res.result, this.groupList);
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  formatResponse(friendsList: any[], groupsList: any[]): any[] {
    if (!groupsList) {
      return friendsList.map(friend => {
        friend.groups = [];
        return friend;
      });
    }
    return friendsList.map(friend => {
      friend.groups = groupsList
        .filter(group => group.members && group.members.find(member => member.username === friend.username))
        .map(group => group.groupName);
      return friend;
    });
  }

  getNumberOfRequests(status: string, length?: boolean) {
    let filteredRequests = [];

    switch ( status ) {
      case RequestTypes.approved:
        filteredRequests = this.requestList.filter(request => (request.request && (request.request.requestStatus === status)));
        break;
      case RequestTypes.pending:
      case RequestTypes.ignored:
        filteredRequests = this.requestList.filter(request => (request.request && (request.request.requestStatus === status) && (request.request.toUserId === this.user.username)));
        break;
    }
    return length ? filteredRequests.length : filteredRequests;
  }

  setActiveCard(cardType: CardTypes) {
    this.activeCard = (this.activeCard !== cardType) ? cardType : null;
  }

  respondRequest(action: RequestTypes, request: any) {
    this.frontService.friendRequestResponse(action, request).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        this.confirmAction = null;
        this.appService.removeMessage(this, 'message');
        if (res.success) {
          if (action === RequestTypes.unfriend) {
            this.groupList.forEach(group => {
              const memberIdx = group.members.findIndex((member: any) => member.username === request.username);
              if (memberIdx >= 0) {
                group.members.splice(memberIdx, 1);
              }
            });
            this.appService.updateGroupListObservable(this.groupList);
          }
          this.updateFriendListObservable(request, action);
          this.getFriendsAndGroups();
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
        this.appService.removeMessage(this, 'message');
    });
  }

  confirmRequest(action: RequestTypes, request: any) {
    this.confirmAction = { action, ...request };
  }

  updateFriendListObservable(request: any, action: RequestTypes) {
    const friendIdx = this.friendsList.findIndex(friend =>
      (friend.toUserId === request.username) || (friend.fromUserId === request.username)
    );
    if (friendIdx >= 0) {
      switch (action) {
        case RequestTypes.approved:
        case RequestTypes.ignored:
          this.friendsList[friendIdx].requestStatus = action;
          break;
        case RequestTypes.unfriend:
        case RequestTypes.delete:
          this.friendsList.splice(friendIdx, 1);
          break;
      }
      this.appService.updateFriendListObservable(this.friendsList);
    }
  }
}
