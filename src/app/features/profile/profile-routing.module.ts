import { ProfileLayoutComponent } from './../../shared/components/app-layouts/profile-layout/profile-layout.component';
import { ProfileViewComponent } from './profile-view/profile-view.component';
import { ProfileComponent } from './profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowcaseComponent } from './showcase/showcase.component';
import { ShowcaseViewComponent } from './showcase/showcase-view/showcase-view.component';
import { CredentialsComponent } from './showcase/credentials/credentials.component';
import { RecommendationsComponent } from './showcase/recommendations/recommendations.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileLayoutComponent,
    children: [
      {
        path: '',
        component:  ProfileComponent
      },
      {
        path: 'manage-friends',
        loadChildren:  './manage-friends/manage-friends.module#ManageFriendsModule'
      },
      {
        path: 'find-friends',
        loadChildren:  './find-friends/find-friends.module#FindFriendsModule'
      },
      {
        path: 'manage-groups',
        loadChildren:  './manage-groups/manage-groups.module#ManageGroupsModule'
      },
      {
        path: 'account',
        loadChildren: './account/account.module#AccountModule'
      },
      {
        path: 'messaging',
        loadChildren: './messaging/messaging.module#MessagingModule'
      }
    ]
  }
      ,
      {
        path: 'view/:username',
        component:  ProfileViewComponent
      }
      ,
      {
        path: 'showcase',
        component: ShowcaseComponent,
        data: {
          showTabs: true,
          showEdit: false
        },
        children: [
          {
            path: '',
            component: ShowcaseViewComponent,
            data: {
              showTabs: false,
              showEdit: true
            }
          },
          {
            path: 'credentials',
            component: CredentialsComponent,
            data: {
              showTabs: false,
              showEdit: true
            }
          },
          {
            path: 'recommendations',
            component: RecommendationsComponent,
            data: {
              showTabs: false,
              showEdit: true
            }
          }
        ]
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
