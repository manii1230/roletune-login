import { LoaderTypes } from './../../../shared/models/loader-types.enum';
import { StatusTypes } from './../../../core/models/status-types.enum';
import { Component, OnInit, OnChanges, Input, SimpleChanges } from '@angular/core';
import { FrontService } from '../../front.service';
import { AppService } from '../../../app.service';

@Component({
  selector: 'app-user-profile-details',
  templateUrl: './user-profile-details.component.html',
  styleUrls: ['./user-profile-details.component.css']
})
export class UserProfileDetailsComponent implements OnInit {
  @Input() user: any;
  @Input() guestUser: any;
  @Input() friendList: any[] = [];
  message: any;
  submitting: boolean;
  loaderType = LoaderTypes.xsmall;
  constructor(private appService: AppService, private frontService: FrontService) { }
  ngOnInit() {
    this.message = null;
    this.submitting = false;
  }

  filterFriendRequests(friendList = []): any[] {
    return friendList.filter(request => request.requestStatus === StatusTypes.approved);
  }

  getRoleDetails() {
    const accessList = this.user.accessList;
    let roleDetails = '';
    if (accessList) {
      if (accessList.b2user && accessList.b2user.some(role => role === true)) {
        roleDetails += 'User';
      }
      if (accessList.c2moderator && accessList.c2moderator.some(role => role === true)) {
        roleDetails += ', Community Moderator';
      }
      if (accessList.s2moderator && accessList.s2moderator.some(role => role === true)) {
        roleDetails += ', Site Moderator';
      }
      if (accessList.superadmin && accessList.superadmin.some(role => role === true)) {
        roleDetails += ', Site Admin';
      }
    }
    return roleDetails;
  }

  sendRequest(): void {
    this.submitting = true;
    this.appService.sendRequest(this.user.username).subscribe(
      (res: any) => {
        this.submitting = false;
        this.message = {
          type: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          const friendRequest = { ...res.result, fromUserId_user_docs: this.guestUser, toUserId_user_docs: this.user };
          this.friendList.push(friendRequest);
          this.appService.updateFriendListObservable(this.friendList);
          this.appService.removeMessage(this, 'message');
        }
      },
      (err) => {
        this.submitting = false;
        this.message = {
          type: false,
          message: err.message
        };
      }
    );
  }

  isFriend(username: string) {
    const friendRequests = this.appService.friendsList.getValue();
    const isFriend = friendRequests.some(request => (
      (request.fromUserId === username && request.toUserId === this.guestUser.username) ||
      (request.fromUserId === this.guestUser.username && request.toUserId === username)
    ));
    return isFriend;
  }

  getFriendName(request: any): string {
    if (request.fromUserId === this.user.username) {
      return `${request.toUserId_user_docs.firstName} ${request.toUserId_user_docs.lastName}`.toLowerCase();
    }
    return `${request.fromUserId_user_docs.firstName} ${request.fromUserId_user_docs.lastName}`.toLowerCase();
  }

  getFriendLink(request: any): string {
    let link = `/main/profile/view/`;
    link += (request.fromUserId === this.user.username) ?  request.toUserId : request.fromUserId;
    return link;
  }
}
