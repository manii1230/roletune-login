import { ErrorLayoutComponent } from './../../shared/components/app-layouts/error-layout/error-layout.component';
import { ErrorComponent } from './error.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ErrorLayoutComponent,
    children: [
      {
        path: ':errorCode',
        component: ErrorComponent
      },
      {
        path: '',
        redirectTo: '404',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorRoutingModule { }
