import { ErrorCodes } from './../../core/models/error-codes.model';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { switchMap, tap, map } from 'rxjs/operators';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  errorCodes = ErrorCodes;
  errorCode: string | number;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.pipe(map(params => params.get('errorCode'))).subscribe(errorCode => {
      this.errorCode = errorCode ? errorCode : ErrorCodes.notFound;
    });
  }

}
