import { SiteModeratorGuard } from './../../core/guards/site-moderator.guard';
import { ModeratorComponent } from './moderator.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivateChild } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ModeratorComponent,
    // canActivateChild: [SiteModeratorGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: './moderator-dashboard/moderator-dashboard.module#ModeratorDashboardModule'
      },
      {
        path: 'list-view',
        loadChildren: './moderator-listing-page/moderator-listing-page.module#ModeratorListingPageModule'
      },
      {
        path: 'details-view',
        loadChildren: './moderator-view-page/moderator-view-page.module#ModeratorViewPageModule'
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '**',
    redirectTo: '/error/404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModeratorRoutingModule { }
