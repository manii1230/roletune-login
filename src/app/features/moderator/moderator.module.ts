import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModeratorRoutingModule } from './moderator-routing.module';
import { ModeratorComponent } from './moderator.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ModeratorComponent],
  imports: [
    CommonModule,
    ModeratorRoutingModule,
    SharedModule
  ]
})
export class ModeratorModule { }
