import { ListingTypes } from './../../../core/models/listing-types.model';
import { ModeratorListingPageComponent } from './moderator-listing-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: ':type',
    component: ModeratorListingPageComponent
  },
  {
    path: '',
    redirectTo: ListingTypes.chat,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModeratorListingPageRoutingModule { }
