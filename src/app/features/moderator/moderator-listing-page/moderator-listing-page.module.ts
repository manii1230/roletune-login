import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModeratorListingPageRoutingModule } from './moderator-listing-page-routing.module';
import { ModeratorListingPageComponent } from './moderator-listing-page.component';

@NgModule({
  declarations: [ModeratorListingPageComponent],
  imports: [
    CommonModule,
    ModeratorListingPageRoutingModule,
    SharedModule
  ]
})
export class ModeratorListingPageModule { }
