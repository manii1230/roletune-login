import { takeUntil } from 'rxjs/operators';
import { ListingTypes } from './../../../core/models/listing-types.model';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { LoadMore } from './../../../core/base/load-more.base';
import { ModeratorService } from './../moderator.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { pipe, Subject } from 'rxjs';

@Component({
  selector: 'app-moderator-listing-page',
  templateUrl: './moderator-listing-page.component.html',
  styleUrls: ['./moderator-listing-page.component.css']
})
export class ModeratorListingPageComponent extends LoadMore implements OnInit, OnDestroy {
  message: any;
  listingTypes = ListingTypes;
  listingType: string;
  reportedChats: any[];
  reportedTimeline: any[];
  reportedMessages: any[];
  unsubscribe = new Subject();

  constructor(private route: ActivatedRoute, private router: Router, private moderatorService: ModeratorService) {
    super();
  }

  ngOnInit() {
    this.message = null;
    this.reportedChats = [];
    this.reportedTimeline = [];
    this.reportedMessages = [];
    this.route.paramMap.pipe(takeUntil(this.unsubscribe)).subscribe(params => {
      this.pageNumber = 1;
      if (params.has('type')) {
        this.listingType = params.get('type');
        switch (params.get('type')) {
          case ListingTypes.chat:
            this.loadReportedChats();
            break;
          case ListingTypes.timeline:
            this.loadReportedTimeline();
            break;
          default:
            this.router.navigate(['/error/404']);
        }
      } else {
        this.router.navigate(['/main/moderator/dashboard']);
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  loadReportedChats() {
    this.config.loading = true;
    const payload = {
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };
    this.moderatorService.getReportedChats(payload).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.config.loadMore = !(res.result && res.result.length < this.pageSize);
        this.reportedChats = this.reportedChats.concat(res.result ? res.result : []);
      },
      (err: HttpErrorResponse) => {
        this.config.loading = false;
        this.config.loadMore = false;
        this.message = {
          status: false,
          message: err.error && err.error.message ? err.error.message : err.message
        };
      }
    );
  }

  loadReportedTimeline() {
    this.config.loading = true;
    const payload = {
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };
    this.moderatorService.getReportedTimeline(payload).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.config.loadMore = !(res.result && res.result.length < this.pageSize);
        this.reportedTimeline = this.reportedTimeline.concat(res.result ? res.result : []);
      },
      (err: HttpErrorResponse) => {
        this.config.loading = false;
        this.config.loadMore = false;
        this.message = {
          status: false,
          message: err.error && err.error.message ? err.error.message : err.message
        };
      }
    );
  }
}
