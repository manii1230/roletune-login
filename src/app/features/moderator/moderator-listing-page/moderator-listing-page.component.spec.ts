import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeratorListingPageComponent } from './moderator-listing-page.component';

describe('ModeratorListingPageComponent', () => {
  let component: ModeratorListingPageComponent;
  let fixture: ComponentFixture<ModeratorListingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModeratorListingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeratorListingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
