import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-reported-timeline-card',
  templateUrl: './reported-timeline-card.component.html',
  styleUrls: ['./reported-timeline-card.component.css']
})
export class ReportedTimelineCardComponent implements OnInit {
  @Input() blog: any;

  constructor() { }

  ngOnInit() {
    if (this.blog && this.blog.comments && this.blog.comments.length === 1) {
      if (Object.keys(this.blog.comments[0]).length === 0) {
        this.blog.comments = [];
      }
    }
  }

}
