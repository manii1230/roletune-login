import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportedTimelineCardComponent } from './reported-timeline-card.component';

describe('ReportedTimelineCardComponent', () => {
  let component: ReportedTimelineCardComponent;
  let fixture: ComponentFixture<ReportedTimelineCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportedTimelineCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportedTimelineCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
