import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportedMessageCardComponent } from './reported-message-card.component';

describe('ReportedMessageCardComponent', () => {
  let component: ReportedMessageCardComponent;
  let fixture: ComponentFixture<ReportedMessageCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportedMessageCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportedMessageCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
