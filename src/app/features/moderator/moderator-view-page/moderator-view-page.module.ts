import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModeratorViewPageRoutingModule } from './moderator-view-page-routing.module';
import { ModeratorViewPageComponent } from './moderator-view-page.component';
import { ReportedChatCardComponent } from './reported-chat-card/reported-chat-card.component';
import { ReportedTimelineCardComponent } from './reported-timeline-card/reported-timeline-card.component';
import { ReportedMessageCardComponent } from './reported-message-card/reported-message-card.component';

@NgModule({
  declarations: [ModeratorViewPageComponent, ReportedChatCardComponent, ReportedTimelineCardComponent, ReportedMessageCardComponent],
  imports: [
    CommonModule,
    ModeratorViewPageRoutingModule,
    SharedModule
  ]
})
export class ModeratorViewPageModule { }
