import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeratorViewPageComponent } from './moderator-view-page.component';

describe('ModeratorViewPageComponent', () => {
  let component: ModeratorViewPageComponent;
  let fixture: ComponentFixture<ModeratorViewPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModeratorViewPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeratorViewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
