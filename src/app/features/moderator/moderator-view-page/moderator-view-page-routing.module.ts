import { ModeratorViewPageComponent } from './moderator-view-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: ':contentId',
    component: ModeratorViewPageComponent
  },
  {
    path: '',
    redirectTo: '/main/moderator/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModeratorViewPageRoutingModule { }
