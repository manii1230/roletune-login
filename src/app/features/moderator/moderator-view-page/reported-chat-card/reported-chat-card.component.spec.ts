import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportedChatCardComponent } from './reported-chat-card.component';

describe('ReportedChatCardComponent', () => {
  let component: ReportedChatCardComponent;
  let fixture: ComponentFixture<ReportedChatCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportedChatCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportedChatCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
