import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-reported-chat-card',
  templateUrl: './reported-chat-card.component.html',
  styleUrls: ['./reported-chat-card.component.css']
})
export class ReportedChatCardComponent implements OnInit {
  @Input() chat: any;
  constructor() { }

  ngOnInit() {
  }

}
