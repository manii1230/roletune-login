import { AppService } from './../../../app.service';
import { ListingTypes } from './../../../core/models/listing-types.model';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { ModeratorService } from './../moderator.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-moderator-view-page',
  templateUrl: './moderator-view-page.component.html',
  styleUrls: ['./moderator-view-page.component.css']
})
export class ModeratorViewPageComponent implements OnInit {
  message: any;
  contentId: string;
  listingTypes = ListingTypes;
  reportedContent: any;
  reportedContentDetails: any;
  constructor(
    private moderatorService: ModeratorService,
    private route: ActivatedRoute,
    private router: Router,
    private appService: AppService
  ) { }

  ngOnInit() {
    this.message = null;
    this.reportedContentDetails = null;
    this.reportedContent = null;
    this.contentId = this.route.snapshot.params.contentId;
    this.loadReportedContent();
  }

  loadReportedContent() {
    this.moderatorService.getReportedContent(this.contentId).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        this.reportedContentDetails = res.result;
        this.reportedContent = res.reportedContent;
        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.message ? err.error.message : err.message
        };
        this.router.navigate([`/error/${err.status}`]);
      }
    );
  }
}
