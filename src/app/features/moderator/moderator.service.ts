import { environment } from './../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModeratorService {

  constructor(private http: HttpClient) { }

  getReportedChats(payload: any) {
    const { pageNumber = 1, pageSize = environment.PAGE_SIZE } = payload;
    let params = new HttpParams();
    params = params.set('pageNumber', pageNumber.toString());
    params = params.set('pageSize', pageSize.toString());
    return this.http.get(`${environment.API_URL}site-moderator-service/reported-chats`, { params });
  }

  getReportedContent(contentId: any) {
    return this.http.get(`${environment.API_URL}site-moderator-service/reported-content-details/${contentId}`);
  }

  getReportedTimeline(payload: any) {
    const { pageNumber = 1, pageSize = environment.PAGE_SIZE } = payload;
    let params = new HttpParams();
    params = params.set('pageNumber', pageNumber.toString());
    params = params.set('pageSize', pageSize.toString());
    return this.http.get(`${environment.API_URL}site-moderator-service/reported-timeline`, { params });
  }
}
