import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-moderator-dashboard',
  templateUrl: './moderator-dashboard.component.html',
  styleUrls: ['./moderator-dashboard.component.css']
})
export class ModeratorDashboardComponent implements OnInit {
  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  navigateToListing(type: string): void {
    this.router.navigate([`/main/moderator/list-view/${type}`]);
  }
}
