import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModeratorDashboardRoutingModule } from './moderator-dashboard-routing.module';
import { ModeratorDashboardComponent } from './moderator-dashboard.component';

@NgModule({
  declarations: [ModeratorDashboardComponent],
  imports: [
    CommonModule,
    ModeratorDashboardRoutingModule
  ]
})
export class ModeratorDashboardModule { }
