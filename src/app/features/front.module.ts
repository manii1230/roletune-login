import { SlickModule } from 'ngx-slick';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrontRoutingModule } from './front-routing.module';
import { HomeComponent } from './home/home.component';
import { FriendlistSidebarComponent } from './friendlist-sidebar/friendlist-sidebar.component';
import { FrontService } from './front.service';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FrontRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    SlickModule.forRoot()
  ],
  declarations: [
    HomeComponent,
    FriendlistSidebarComponent
  ],
  providers: [
    FrontService,
  ]
})
export class FrontModule { }

