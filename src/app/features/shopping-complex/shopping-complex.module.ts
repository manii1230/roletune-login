import { SharedModule } from './../../shared/shared.module';
import { ShoppingComplexComponent } from './shopping-complex.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShoppingComplexRoutingModule } from './shopping-complex-routing.module';
import { FrontModule } from '../front.module';

@NgModule({
  declarations: [
    ShoppingComplexComponent
  ],
  imports: [
    CommonModule,
    ShoppingComplexRoutingModule,
    FrontModule,
    SharedModule
  ]
})
export class ShoppingComplexModule { }
