import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoppingComplexComponent } from './shopping-complex.component';

describe('ShoppingComplexComponent', () => {
  let component: ShoppingComplexComponent;
  let fixture: ComponentFixture<ShoppingComplexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoppingComplexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingComplexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
