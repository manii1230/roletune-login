import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivacySettingsRoutingModule } from './privacy-settings-routing.module';
import { PrivacySettingsComponent } from './privacy-settings.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PrivacySettingsComponent],
  imports: [
    CommonModule,
    PrivacySettingsRoutingModule,
    ReactiveFormsModule
  ]
})
export class PrivacySettingsModule { }
