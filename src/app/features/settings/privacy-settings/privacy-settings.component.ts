import { HttpErrorResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SettingsBase } from './../settings.base';
import { AppService } from './../../../app.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-privacy-settings',
  templateUrl: './privacy-settings.component.html',
  styleUrls: ['./privacy-settings.component.css']
})
export class PrivacySettingsComponent extends SettingsBase implements OnInit, OnDestroy {
  user: any;
  privacySettingsForm: FormGroup;
  communityList: any[];
  step: number;
  unsubscribe = new Subject();

  constructor(private formBuilder: FormBuilder, private appService: AppService, private settingsService: SettingsService) {
    super();
  }

  ngOnInit() {
    this.communityList = [];
    this.step = null;
    this.privacySettingsForm = this.formBuilder.group({
      otherFeatures: ['S', Validators.required],
      showcase: [],
      notifications: ['P', Validators.required],
      friendslist: ['P', Validators.required]
    });

    this.appService.activeLoggedInUser.pipe(takeUntil(this.unsubscribe)).subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.updateForm();
        this.settingsService.getUserCommunityList().pipe(takeUntil(this.unsubscribe)).subscribe(
          (res: any) => {
            if (res.success) {
              this.communityList = res.result ? this.formatCommunities(res.result) : [];
            }
          },
          (err: HttpErrorResponse) => {
            this.message = {
              status: false,
              message: err.message
            };
          }
        );
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  updateForm() {
    this.privacySettingsForm.patchValue({
      otherFeatures: this.user.privacySettings.otherFeatures,
      notifications: this.user.privacySettings.notifications ? this.user.privacySettings.notifications : 'P',
      friendslist: this.user.privacySettings.friendslist ? this.user.privacySettings.friendslist : 'P',
    });
  }

  formatCommunities(commmunities: any[]) {
    return commmunities.map(community => {
      const showcaseItem = (this.user.privacySettings && this.user.privacySettings.showcase) ?
        this.user.privacySettings.showcase.find(item => item._id === community.communityUser.communityDetails._id) : null;
      community.showcase = (showcaseItem && showcaseItem.showcase) ? showcaseItem.showcase : 'P';
      return community;
    });
  }

  updateCommunityPrivacy(event: any, i: number): void {
    this.communityList[i].showcase = event.target.value;
  }

  updatePrivacySettings(fields?: string): void {
    this.formSection = fields;
    this.message = null;
    const payload: any = {};
    switch (fields) {
      case 'other-features':
        payload.otherFeatures = this.privacySettingsForm.value.otherFeatures;
        break;
      case 'showcase':
        // tslint:disable-next-line: max-line-length
        payload.showcase = this.communityList.map(community => ({ _id: community.communityUser.communityDetails._id, showcase: community.showcase ? community.showcase : 'P' }));
        break;
      case 'notifications':
        payload.notifications = this.privacySettingsForm.value.notifications ? this.privacySettingsForm.value.notifications : 'P';
        break;
      case 'friendslist':
        payload.friendslist = this.privacySettingsForm.value.friendslist ? this.privacySettingsForm.value.friendslist : 'P';
    }

    this.settingsService.updatePrivacySettings(payload).subscribe(
      (res: any) => {
        if (res.success) {
          this.user.privacySettings = (res.user && res.user.privacySettings) ? res.user.privacySettings : this.user.privacySettings;
          this.updateForm();
          if (this.user.privacySettings.otherFeatures === 'C' && !this.step) {
            this.step = 1;
          }
        }
        this.message = {
          status: res.success,
          message: res.success ? this.getMessage() : res.message.errorMessage
        };
        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
    });
  }
}
