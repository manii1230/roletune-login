import { SettingsBase } from './../settings.base';
import { MessageTypes } from './../../../core/models/message-types';
import { AppService } from './../../../app.service';
import { SettingsService } from './../settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.css']
})
export class ProfileSettingsComponent extends SettingsBase implements OnInit {
  user: any;
  profileSettingsForm: FormGroup;
  profilePreview: string;

  constructor(private formBuilder: FormBuilder, private appService: AppService, private settingsService: SettingsService) {
    super();
  }

  ngOnInit() {
    this.submitted = false;
    this.formSection = null;

    this.profileSettingsForm = this.formBuilder.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      profileImage: [''],
      password: ['', [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(128),
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!#%*?&._]{8,128}$/)
        ]
      ],
      confirmpassword: ['', [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(128),
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!#%*?&._]{8,128}$/)
        ]
      ],
      oldpassword: ['', [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(128),
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!#%*?&._]{8,128}$/)
        ]
      ],
      twoFactorAuth: ['', Validators.required],
    });
    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.updateForm();
      }
    });
  }

  get password() {
    return this.profileSettingsForm.get('password');
  }

  get confirmpassword() {
    return this.profileSettingsForm.get('confirmpassword');
  }

  get oldpassword() {
    return this.profileSettingsForm.get('oldpassword');
  }

  get twoFactorAuth() {
    return this.profileSettingsForm.get('twoFactorAuth');
  }

  updateForm() {
    this.profileSettingsForm.patchValue({
      name: this.user.firstName + ' ' + this.user.lastName,
      username: this.user.username,
      twoFactorAuth: this.user.twoFactorAuth ? 'true' : 'false'
    });
  }

  uploadImage(event: any, field: string): void {
    this.formSection = field;
    if (event.target && event.target.files[0]) {
      const fileToLoad = event.target.files[0];
      if ((fileToLoad.size / 1024) < 50) {
        const fileReader = new FileReader();
        fileReader.onload = (fileLoadedEvent: any) => {
          // tslint:disable-next-line: max-line-length
          this.profileSettingsForm.patchValue({ profileImage: (fileLoadedEvent.target && fileLoadedEvent.target.result) ? fileLoadedEvent.target.result : '' });
          this.updateProfileSettings('profile-image');
        };
        fileReader.readAsDataURL(fileToLoad);
      } else {
        this.message = {
          status: false,
          message: MessageTypes.imageSizeFailed
        };
      }
    }
  }

  updateProfileSettings(fields?: any): void {
    this.formSection = fields;
    this.submitted = true;
    this.message = null;
    const profile: any = {};
    switch (fields) {
      case 'profile-image':
        profile.profileImage = this.profileSettingsForm.value.profileImage;
        break;
      case 'password':
        if ((this.password.invalid || this.oldpassword.invalid) || (this.password.value !== this.confirmpassword.value)) {
          return;
        }
        profile.password = btoa(this.profileSettingsForm.value.password);
        profile.oldpassword = btoa(this.profileSettingsForm.value.oldpassword);
        break;
      case 'authentication':
        profile.twoFactorAuth = this.profileSettingsForm.value.twoFactorAuth;
    }

    this.settingsService.updateProfileSettings(profile).subscribe(
      (res: any) => {
        if (res.success) {
          this.user.profileImage = (res.user && res.user.profileImage) ? res.user.profileImage : this.user.profileImage;
          this.user.twoFactorAuth = (res.user && res.user.twoFactorAuth) ? res.user.twoFactorAuth : '';
          this.appService.updateUser(this.user);
          this.updateForm();
          this.submitted = false;
        }
        this.message = {
          status: res.success,
          message: res.success ? this.getMessage() : res.message.errorMessage
        };
        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err) => {
        this.message = {
          status: false,
          message: err.message.errorMessage
        };
    });
  }
}
