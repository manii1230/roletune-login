import { HttpErrorResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SettingsBase } from './../settings.base';
import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-home-settings',
  templateUrl: './home-settings.component.html',
  styleUrls: ['./home-settings.component.css']
})
export class HomeSettingsComponent extends SettingsBase implements OnInit, OnDestroy {
  user: any;
  homeSettingsForm: FormGroup;
  newsFeedList: any[];
  communityFeedList: any[];
  otherList: any[];
  unsubscribe = new Subject();

  constructor(private formBuilder: FormBuilder, private appService: AppService, private settingsService: SettingsService) {
    super();
  }

  ngOnInit() {
    this.newsFeedList = [];
    this.communityFeedList = [];

    this.otherList = [
      { _id: 'news', label: 'News', feedSelected: false },
      { _id: 'sports', label: 'Sports', feedSelected: false },
      { _id: 'science', label: 'Science', feedSelected: true }
    ];

    this.homeSettingsForm = this.formBuilder.group({
      clearAlerts: ['', Validators.required],
      clearAlertsAfter: [7],
      theme: ['#008572', Validators.required],
      newsFeedList: []
    });

    this.appService.activeLoggedInUser.pipe(takeUntil(this.unsubscribe)).subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.updateForm();
        this.settingsService.getUserCommunityList().pipe(takeUntil(this.unsubscribe)).subscribe(
          (res: any) => {
            if (res.success) {
              this.newsFeedList = (res.result && res.result.length) ? this.formatNewsFeeds(res.result, 3) : [];
              this.otherList.forEach(other => {
                other.feedSelected = this.getSelectedNewsFeeds(other);
              });
            }
          },
          (err: HttpErrorResponse) => {
            this.message = {
              status: false,
              message: err.message
            };
        });
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  updateForm() {
    this.homeSettingsForm.patchValue({
      clearAlerts: this.user.homeSettings.clearAlerts ? 'true' : 'false',
      clearAlertsAfter: this.user.homeSettings.clearAlertsAfter ? this.user.homeSettings.clearAlertsAfter : 7,
      theme: this.user.homeSettings.theme
    });
  }

  formatNewsFeeds(newsFeeds: any[], num: number) {
    this.communityFeedList = newsFeeds;
    const formattedNewsFeeds = [];
    for (let i = 0, j = 0; i < newsFeeds.length; i++) {
        if (i >= num && i % num === 0) {
          j++;
        }
        newsFeeds[i].feedSelected = this.getSelectedNewsFeeds(newsFeeds[i].communityUser.communityDetails);
        formattedNewsFeeds[j] = formattedNewsFeeds[j] || [];
        formattedNewsFeeds[j].push(newsFeeds[i]);
    }
    return formattedNewsFeeds;
  }

  updateHomepageSettings(fields?: any): void {
    this.formSection = fields;
    this.message = null;
    const payload: any = {};
    switch (fields) {
      case 'alerts':
        payload.clearAlerts = this.homeSettingsForm.value.clearAlerts;
        payload.clearAlertsAfter = parseInt(this.homeSettingsForm.value.clearAlertsAfter, 10);
        break;
      case 'theme':
        payload.theme = this.homeSettingsForm.value.theme;
        break;
      case 'newsfeed':
        const communityNewsFeeds = this.communityFeedList.filter(communityUser => communityUser.feedSelected).map(userDetails => userDetails.communityUser.communityDetails._id);
        const otherNewsFeeds = this.otherList.filter(other => other.feedSelected).map(other => other._id);
        payload.newsFeedList = communityNewsFeeds.concat(otherNewsFeeds);
    }
    this.settingsService.updateHomepageSettings(payload).subscribe(
      (res: any) => {
        if (res.success) {
          this.user.homeSettings = (res.user && res.user.homeSettings) ? res.user.homeSettings : this.user.homeSettings;
          this.appService.updateUser(this.user);
          localStorage.setItem('theme', this.user.homeSettings.theme);
          document.documentElement.style.setProperty('--background-color', this.user.homeSettings.theme);
          document.documentElement.style.setProperty('--link-color', this.user.homeSettings.theme);
          this.updateForm();
        }
        this.message = {
          status: res.success,
          message: res.success ? this.getMessage() : res.message.errorMessage
        };

        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
    });
  }

  getSelectedNewsFeeds(newsfeedObj: any) {
    const newsFeed = (this.user.homeSettings && this.user.homeSettings.newsFeedList) ?
      this.user.homeSettings.newsFeedList.find(item => item === newsfeedObj._id) : null;
    return newsFeed ? true : false;
  }
}
