import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeSettingsRoutingModule } from './home-settings-routing.module';
import { HomeSettingsComponent } from './home-settings.component';

@NgModule({
  declarations: [HomeSettingsComponent],
  imports: [
    CommonModule,
    HomeSettingsRoutingModule,
    ReactiveFormsModule
  ]
})
export class HomeSettingsModule { }
