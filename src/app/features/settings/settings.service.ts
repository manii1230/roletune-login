import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private http: HttpClient) { }

  updateProfileSettings(settings) {
    return this.http.patch(`${environment.API_URL}user-service/user-details/`, settings);
  }

  updatePrivacySettings(settings) {
    return this.http.patch(`${environment.API_URL}user-service/user-details/`, { privacySettings: settings });
  }

  updateHiringSettings(settings) {
    return this.http.patch(`${environment.API_URL}user-service/user-details/`, { hiringSettings: settings });
  }

  updateHomepageSettings(settings: any) {
    return this.http.patch(`${environment.API_URL}user-service/user-details/`, { homeSettings: settings });
  }

  updateHuddleRoomSettings(settings) {
    return this.http.patch(`${environment.API_URL}user-service/user-details/`, { huddleRoomSettings: settings });
  }

  updateMessageSettings(settings) {
    return this.http.patch(`${environment.API_URL}user-service/user-details/`, { messageSettings: settings });
  }

  getUserCommunityList() {
    return this.http.get(`${environment.API_URL}community-service/user-community-list`);
  }
}
