import { SettingsComponent } from './settings.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: '',
        redirectTo: 'profile',
        pathMatch: 'full'
      },
      {
        path: 'profile',
        loadChildren: './profile-settings/profile-settings.module#ProfileSettingsModule'
      },
      {
        path: 'privacy',
        loadChildren: './privacy-settings/privacy-settings.module#PrivacySettingsModule'
      },
      {
        path: 'hiring-readiness',
        loadChildren: './hiring-settings/hiring-settings.module#HiringSettingsModule'
      },
      {
        path: 'home-page',
        loadChildren: './home-settings/home-settings.module#HomeSettingsModule'
      },
      {
        path: 'huddle-room',
        loadChildren: './huddle-room-settings/huddle-room-settings.module#HuddleRoomSettingsModule'
      },
      {
        path: 'messages',
        loadChildren: './message-settings/message-settings.module#MessageSettingsModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
