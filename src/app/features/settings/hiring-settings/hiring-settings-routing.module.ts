import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HiringSettingsComponent } from './hiring-settings.component';

const routes: Routes = [
  {
    path: '',
    component: HiringSettingsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HiringSettingsRoutingModule { }
