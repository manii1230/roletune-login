import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HiringSettingsRoutingModule } from './hiring-settings-routing.module';
import { HiringSettingsComponent } from './hiring-settings.component';

@NgModule({
  declarations: [HiringSettingsComponent],
  imports: [
    CommonModule,
    HiringSettingsRoutingModule
  ]
})
export class HiringSettingsModule { }
