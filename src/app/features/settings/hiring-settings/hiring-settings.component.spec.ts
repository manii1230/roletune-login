import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HiringSettingsComponent } from './hiring-settings.component';

describe('HiringSettingsComponent', () => {
  let component: HiringSettingsComponent;
  let fixture: ComponentFixture<HiringSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HiringSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HiringSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
