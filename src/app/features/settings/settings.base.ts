import { MessageTypes } from './../../core/models/message-types';
export class SettingsBase {
  formSection: string;
  message: any;
  submitted: boolean;

  constructor() {
    this.submitted = false;
    this.message = null;
    this.formSection = null;
  }

  getMessage() {
    switch (this.formSection) {
      case 'profile-image':
        return MessageTypes.profileImageSuccess;
      case 'password':
        return MessageTypes.passwordSuccess;
      case 'authentication':
        return MessageTypes.authenticationSuccess;
      case 'message-open':
        return MessageTypes.messageOpenInSuccess;
      case 'message-delete':
        return MessageTypes.messageDeletionSuccess;
      case 'message-purge':
        return MessageTypes.messagePurgeSuccess;
      case 'huddle-room-open':
        return MessageTypes.huddleRoomOpenSuccess;
      case 'huddle-room-unlock':
        return MessageTypes.huddleRoomUnlockSuccess;
      case 'huddle-room-autolock':
        return MessageTypes.huddleRoomAutolockSuccess;
      case 'alerts':
        return MessageTypes.alertsSettingsSuccess;
      case 'theme':
        return MessageTypes.themeSettingsSuccess;
      case 'newsfeed':
        return MessageTypes.newsfeedSettingsSuccess;
      case 'other-features':
        return MessageTypes.otherFeaturesSettingsSuccess;
      case 'showcase':
        return MessageTypes.showcaseSettingsSuccess;
      case 'notifications':
        return MessageTypes.notificationsSettingsSuccess;
      case 'friendslist':
        return MessageTypes.friendslistSettingsSuccess;
    }
  }
}
