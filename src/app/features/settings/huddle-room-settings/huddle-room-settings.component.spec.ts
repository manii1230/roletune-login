import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HuddleRoomSettingsComponent } from './huddle-room-settings.component';

describe('HuddleRoomSettingsComponent', () => {
  let component: HuddleRoomSettingsComponent;
  let fixture: ComponentFixture<HuddleRoomSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HuddleRoomSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HuddleRoomSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
