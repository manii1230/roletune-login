import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HuddleRoomSettingsRoutingModule } from './huddle-room-settings-routing.module';
import { HuddleRoomSettingsComponent } from './huddle-room-settings.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [HuddleRoomSettingsComponent],
  imports: [
    CommonModule,
    HuddleRoomSettingsRoutingModule,
    ReactiveFormsModule
  ]
})
export class HuddleRoomSettingsModule { }
