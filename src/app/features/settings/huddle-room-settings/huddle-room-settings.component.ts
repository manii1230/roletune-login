import { SettingsBase } from './../settings.base';
import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../settings.service';

@Component({
  selector: 'app-huddle-room-settings',
  templateUrl: './huddle-room-settings.component.html',
  styleUrls: ['./huddle-room-settings.component.css']
})
export class HuddleRoomSettingsComponent extends SettingsBase implements OnInit {
  user: any;
  huddleRoomSettingsForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private appService: AppService, private settingsService: SettingsService) {
    super();
  }

  ngOnInit() {
    this.huddleRoomSettingsForm = this.formBuilder.group({
      openIn: ['P', Validators.required],
      unlockType: ['W', Validators.required],
      password: ['', [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(128),
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!#%*?&._]{8,128}$/)
        ]
      ],
      confirmpassword: ['', [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(128),
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d@$!#%*?&._]{8,128}$/)
      ]
      ],
      otp: ['', [
        Validators.required,
        Validators.minLength(8)
      ]
      ],
      lock: ['', Validators.required],
      lockAfter: [null]
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.updateForm();
      }
    });
  }

  get password() {
    return this.huddleRoomSettingsForm.get('password');
  }

  get confirmpassword() {
    return this.huddleRoomSettingsForm.get('confirmpassword');
  }

  get otp() {
    return this.huddleRoomSettingsForm.get('otp');
  }

  updateForm() {
    this.huddleRoomSettingsForm.patchValue({
      openIn: this.user.huddleRoomSettings.openIn,
      unlockType: this.user.huddleRoomSettings.unlockType,
      lock: this.user.huddleRoomSettings.lock ? 'true' : 'false',
      lockAfter: this.user.huddleRoomSettings.lockAfter ? this.user.huddleRoomSettings.lockAfter : 15
    });
  }

  updateHuddleRoomSettings(fields?: any): void {
    this.formSection = fields;
    this.submitted = true;
    this.message = null;
    const payload: any = {};
    switch (fields) {
      case 'huddle-room-open':
        payload.openIn = this.huddleRoomSettingsForm.value.openIn;
        break;
      case 'huddle-room-unlock':
        if (
          (this.huddleRoomSettingsForm.value.unlockType === 'P') &&
          (this.password.invalid || (this.password.value !== this.confirmpassword.value) || this.otp.invalid)
        ) {
          return;
        }
        payload.unlockType = this.huddleRoomSettingsForm.value.unlockType,
        payload.password = btoa(this.huddleRoomSettingsForm.value.password);
        payload.otp = btoa(this.huddleRoomSettingsForm.value.otp);
        break;
      case 'huddle-room-autolock':
        payload.lock = this.huddleRoomSettingsForm.value.lock,
        payload.lockAfter = parseInt(this.huddleRoomSettingsForm.value.lockAfter, 10);
    }
    this.settingsService.updateHuddleRoomSettings(payload).subscribe(
      (res: any) => {
        if (res.success) {
          // tslint:disable-next-line: max-line-length
          this.user.huddleRoomSettings = (res.user && res.user.huddleRoomSettings) ? res.user.huddleRoomSettings : this.user.huddleRoomSettings;
          this.appService.updateUser(this.user);
          this.updateForm();
          this.submitted = false;
        }
        this.message = {
          status: res.success,
          message: res.success ? this.getMessage() : res.message.errorMessage
        };
        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err) => {
        this.message = {
          status: false,
          message: err.message.errorMessage
        };
    });
  }
}
