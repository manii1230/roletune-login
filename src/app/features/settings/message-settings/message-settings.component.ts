import { SettingsBase } from './../settings.base';
import { MessageTypes } from './../../../core/models/message-types';
import { AppService } from './../../../app.service';
import { SettingsService } from './../settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-settings',
  templateUrl: './message-settings.component.html',
  styleUrls: ['./message-settings.component.css']
})
export class MessageSettingsComponent extends SettingsBase implements OnInit {
  user: any;
  messageSettingsForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private appService: AppService, private settingsService: SettingsService) {
    super();
  }

  ngOnInit() {
    this.messageSettingsForm = this.formBuilder.group({
      openIn: ['', Validators.required],
      deletePermanently: ['', Validators.required],
      purge: ['', Validators.required],
      purgeAfter: [null]
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.updateForm();
      }
    });
  }

  updateForm() {
    this.messageSettingsForm.patchValue({
      openIn: this.user.messageSettings.openIn,
      deletePermanently: this.user.messageSettings.deletePermanently ? 'true' : 'false',
      purge: this.user.messageSettings.purge ? 'true' : 'false',
      purgeAfter: this.user.messageSettings.purgeAfter ? this.user.messageSettings.purgeAfter : 1
    });
  }

  updateMessagSettings(fields?: any) {
    this.formSection = fields;
    this.message = null;
    const payload: any = {};
    switch (fields) {
      case 'message-open':
        payload.openIn = this.messageSettingsForm.value.openIn;
        break;
      case 'message-delete':
        payload.deletePermanently = this.messageSettingsForm.value.deletePermanently;
        break;
      case 'message-purge':
        payload.purge = this.messageSettingsForm.value.purge;
        // tslint:disable-next-line: max-line-length
        payload.purgeAfter = (this.messageSettingsForm.value.purge === 'true') ? parseInt(this.messageSettingsForm.value.purgeAfter, 10) : null;
    }

    this.settingsService.updateMessageSettings(payload).subscribe(
      (res: any) => {
        if (res.success) {
          this.user.messageSettings = (res.user && res.user.messageSettings) ? res.user.messageSettings : this.user.messageSettings;
          this.appService.updateUser(this.user);
          this.updateForm();
        }
        this.message = {
          status: res.success,
          message: res.success ? this.getMessage() : res.message.errorMessage
        };
        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err) => {
        this.message = {
          status: false,
          message: err.message.errorMessage
        };
      }
    );
  }
}
