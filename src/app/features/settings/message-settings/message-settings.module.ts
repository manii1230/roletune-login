import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessageSettingsRoutingModule } from './message-settings-routing.module';
import { MessageSettingsComponent } from './message-settings.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [MessageSettingsComponent],
  imports: [
    CommonModule,
    MessageSettingsRoutingModule,
    ReactiveFormsModule
  ]
})
export class MessageSettingsModule { }
