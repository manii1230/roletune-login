import { SharedModule } from './../../shared/shared.module';
import { HuddleRoomComponent } from './huddle-room.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HuddleRoomRoutingModule } from './huddle-room-routing.module';
import { FrontModule } from '../front.module';

@NgModule({
  declarations: [
    HuddleRoomComponent
  ],
  imports: [
    CommonModule,
    HuddleRoomRoutingModule,
    FrontModule,
    SharedModule
  ]
})
export class HuddleRoomModule { }
