import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HuddleRoomComponent } from './huddle-room.component';

describe('HuddleRoomComponent', () => {
  let component: HuddleRoomComponent;
  let fixture: ComponentFixture<HuddleRoomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HuddleRoomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HuddleRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
