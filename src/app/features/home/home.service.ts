import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  createTimeline(blog: any) {
    const blogFormData = new FormData();
    for (const key in blog) {
      if (blog.hasOwnProperty(key)) {
        blogFormData.append( key, blog[key]);
      }
    }
    return this.http.post(`${environment.API_URL}timeline-service/`, blogFormData);
  }

  updateTimelineBlog(blogId: any, action: string) {
    return this.http.patch(`${environment.API_URL}timeline-service/${blogId}`, { action });
  }

  createTimelineComment(comment: any) {
    return this.http.post(`${environment.API_URL}timeline-service/comments`, comment);
  }

  getUserTimeline(username: string = '', pageNumber: number = 1, blogType?: string) {
    if (blogType) {
      switch (blogType) {
        case 'bookmarked':
          return this.http.get(`${environment.API_URL}timeline-service/user/${username}?pageNumber=${pageNumber}&blogType=bookmark`);
        case 'saved':
          return this.http.get(`${environment.API_URL}timeline-service/user/${username}?pageNumber=${pageNumber}&blogType=save`);
      }
    }
    // tslint:disable-next-line: max-line-length
    return (username !== '') ? this.http.get(`${environment.API_URL}timeline-service/user/${username}?pageNumber=${pageNumber}`) : this.http.get(`${environment.API_URL}timeline-service/all?pageNumber=${pageNumber}`);
  }

  communityBlogReply(comment: any) {
    return this.http.post(`${environment.API_URL}timeline-service/replies`, comment);
  }

  saveTimelineAction(blog: any) {
    return this.http.post(`${environment.API_URL}timeline-service/blog-actions`, blog);
  }

  shareBlogMessage(blog: any) {
    return this.http.post(`${environment.API_URL}internet-service/internet-user`, blog);
  }

  getAlertMessages(username: string) {
    return this.http.get(`${environment.API_URL}user-alerts-service/${username}`);
  }

  updateAlertMessages(payload: any) {
    return this.http.patch(`${environment.API_URL}user-alerts-service/`, { alertsList : payload });
  }
}
