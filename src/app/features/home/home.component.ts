import { FrontService } from './../front.service';
import { MessageTypes } from './../../core/models/message-types';
import { Subject } from 'rxjs';
import { AppService } from './../../app.service';
import { HomeService } from './home.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
declare var MediaRecorder: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  alerts: any[];
  alertsAction: any;
  user: any;
  username: string;
  blogType: string;
  unsubscribe = new Subject();
  mediaRecorder: any;
  chunks: any;
  liveStreamFlag: boolean;
  groupList: any[];
  updateAll: boolean;

  // tslint:disable-next-line: max-line-length
  constructor(
    private route: ActivatedRoute,
    private homeService: HomeService,
    private frontService: FrontService,
    private appService: AppService,
    private formBuilder: FormBuilder,
    private zone: NgZone
  ) {}

  ngOnInit() {
    window['that'] = this;
    this.liveStreamFlag = false;
    this.blogType = '';
    this.alerts = [];
    this.alertsAction = null;
    this.updateAll = false;
    this.groupList = [];

    this.frontService.getServerSentUpdates('new-alert').pipe(takeUntil(this.unsubscribe)).subscribe((alert: any) => {
      this.alerts.unshift(alert);
    });

    this.appService.activeLoggedInUser
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(user => {
        if (user && Object.keys(user).length) {
          this.user = user;
          // tslint:disable-next-line: max-line-length
          document.documentElement.style.setProperty(
            '--background-color',
            this.user.homeSettings && this.user.homeSettings.theme
              ? this.user.homeSettings.theme
              : '#008670'
          );
          // tslint:disable-next-line: max-line-length
          document.documentElement.style.setProperty(
            '--link-color',
            this.user.homeSettings && this.user.homeSettings.theme
              ? this.user.homeSettings.theme
              : '#008670'
          );
          this.homeService.getAlertMessages(this.user.username).pipe(takeUntil(this.unsubscribe)).subscribe(
            (res: any) => {
              this.alerts = (res.success && res.result && res.result.length) ? res.result : [];
          });

          this.route.paramMap
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((param: any) => {
              this.username = param.get('username')
                ? param.get('username')
                : null;
              this.blogType = param.get('blogType')
                ? param.get('blogType')
                : null;
              this.appService.groupListObservable.pipe(takeUntil(this.unsubscribe)).subscribe(groups => {
                this.groupList = groups ? groups : [];
              });
            });
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  addAuthor() {
    return {
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      profileImage: this.user.profileImage,
      username: this.user.username
    };
  }

  captureLiveStream() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      this.liveStreamFlag = true;
      navigator.mediaDevices
        .getUserMedia({ video: true, audio: true })
        .then(stream => {
          const videoTag: HTMLVideoElement = document.querySelector(
            '#video-stream'
          );
          videoTag.srcObject = stream;
          this.mediaRecorder = new MediaRecorder(stream);
          this.chunks = [];
          this.mediaRecorder.ondataavailable = (event: any) => {
            window['that'].chunks.push(event.data);
          };
          this.mediaRecorder.onstop = (event: any) => {
            window['that'].saveVideo(event);
          };
        })
        .catch((err: any) => {
          console.log('Error: ', err);
        });
    } else {
    }
  }

  startRecording() {
    this.mediaRecorder.start();
    (<HTMLVideoElement>document.querySelector('#video-stream')).play();
  }

  stopRecording() {
    this.mediaRecorder.stop();
    (<HTMLVideoElement>document.querySelector('#video-stream')).pause();
  }

  
  showSharedGroupMessage(blog: any): boolean {
    if (!blog.sharedGroup) {
      return false;
    }
    const groupName = this.groupList.find(groupItem => groupItem.groupName === blog.sharedGroup);
    if (!groupName) {
      return false;
    }
    return (blog && blog.authorDetails && (blog.authorDetails.username === this.user.username));
  }

  updateAlertFlags(flag: boolean) {
    this.alerts.forEach(alert => {
      alert.read = flag;
    });
  }

  updateAlerts() {
    const readAlerts = this.alerts.filter(alert => alert.read === true);
    if (readAlerts.length === 0) {
      this.alertsAction = {
        status: false,
        message: MessageTypes.alertsNotFound
      };
      this.updateAll = false;
      return this.appService.removeMessage(this, 'alertsAction');
    }

    const alertsList = readAlerts.map(alert => alert._id);
    this.homeService.updateAlertMessages(alertsList).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        if (res.success) {
          this.alerts = this.alerts.filter(alert => alert.read === false);
          this.alertsAction = {
            status: true,
            message: MessageTypes.alertsSuccess
          };
          this.updateAll = false;
          this.appService.removeMessage(this, 'alertsAction');
        }
      },
      (err: HttpErrorResponse) => {
        this.alertsAction = {
          status: false,
          message: MessageTypes.alertsFailed
        };
        this.appService.removeMessage(this, 'alertsAction');
      }
    );
  }

  checkUpdateAll(alert: any) {
    alert.read = !alert.read;
    this.updateAll = this.alerts.every(alert => alert.read === true);
  }
}
