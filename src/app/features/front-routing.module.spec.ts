import { FrontRoutingModule } from './front-routing.module';

describe('FrontRoutingModule', () => {
  let frontRoutingModule: FrontRoutingModule;

  beforeEach(() => {
    frontRoutingModule = new FrontRoutingModule();
  });

  it('should create an instance', () => {
    expect(frontRoutingModule).toBeTruthy();
  });
});
