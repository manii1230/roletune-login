import { ChatActionTypes } from './../core/models/chat-action-types.model';
import { RequestTypes } from 'src/app/core/models/request-types.model';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class FrontService {
  url: string;
  socket: any;
  constructor(private http: HttpClient) {
    this.url = environment.API_URL;
    this.socket = io(this.url);
  }

  searchFriends(searchForm: any, page: number, limit: number) {
    return this.http.get(`${environment.API_URL}user-service/user-search/${searchForm.name}?location=${searchForm.location}&community=${searchForm.community}&pageNumber=${page}&limit=${limit}`);
  }

  createGroup(groupDetails: any) {
    return this.http.post(`${environment.API_URL}user-group-service/user-group`, groupDetails);
  }

  updateGroup(groupDetails: any) {
    return this.http.patch(`${environment.API_URL}user-group-service/user-group`, groupDetails);
  }

  deleteGroup(groupId: string) {
    return this.http.delete(`${environment.API_URL}user-group-service/user-group/${groupId}`);
  }

  getGroupList(username?: string) {
    if (username) {
      return this.http.get(`${environment.API_URL}user-group-service/user-group`);
    } else {
      return this.http.get(`${environment.API_URL}user-group-service/user-group`);
    }
  }

  getFriendsList(userId?: string) {
    let params = new HttpParams();
    if (userId) {
      params = params.set('userId', userId);
    }
    return this.http.get(`${environment.API_URL}user-service/friend-request-list`, { params });
  }

  getUserDetails(username: string) {
    return this.http.get(`${environment.API_URL}user-service/guest-user/${username}`);
  }

  getRecommendations(username: string) {
    return this.http.get(`${environment.API_URL}user-service/friend-request-list`);
  }

  confirmRequest(username: string) {
    return this.http.get(`${environment.API_URL}user-service/approved-friend-request?fromUserId=${username}`);
  }

  ignoreRequest(username: string) {
    return this.http.get(`${environment.API_URL}user-service/ignore-friend-request?fromUserId=${username}`);
  }

  deleteRequest(username: string) {
    return this.http.delete(`${environment.API_URL}user-service/friend-request?fromUserId=${username}`);
  }

  unfriendRequest(username: string) {
    return this.http.patch(`${environment.API_URL}user-service/approved-friend-request?fromUserId=${username}`, {});
  }

  getUserCommunities(username: string) {
    return this.http.get(`${environment.API_URL}community-service/communities/${username}`);
  }

  emitAction(action: string, user: any) {
    this.socket.emit(action, user);
  }

  uploadFile(file: any) {
    const fileData = new FormData();
    for (const key in file) {
      if (file.hasOwnProperty(key)) {
        fileData.append( key, file[key]);
      }
    }
    return this.http.post(`${environment.API_URL}user-chat-service/`, fileData);
  }

  // getAlerts() {
  //   return Observable.create((observer) => {
  //       this.socket.on('new-alert', (alert) => {
  //           observer.next(alert);
  //       });
  //   });
  // }

  getServerSentUpdates(eventType: string) {
    return Observable.create((observer) => {
      this.socket.on(eventType, (alert) => {
          observer.next(alert);
      });
  });
  }

  getChatMessages(username: string) {
    return this.http.get(`${environment.API_URL}user-chat-service/${username}`);
  }

  getAllChatMessages(payload: any) {
    const { username, pageNumber = 1, pageSize = 10 } = payload;
    let queryString = `pageNumber=${pageNumber}`;
    queryString += `&&pageSize=${pageSize}`;
    return this.http.get(`${environment.API_URL}user-chat-service/all/${username}?${queryString}`);
  }

  performChatMessageAction(payload: any) {
    return this.http.patch(`${environment.API_URL}user-chat-service/message-action`, payload);
  }

  getFriendsWithCommunities(username: string) {
    return this.http.get(`${environment.API_URL}friend-request-service/${username}`);
  }

  friendRequestAction(friendRequest: any) {
    return this.http.post(`${environment.API_URL}friend-request-service/`, friendRequest);
  }

  friendRequestResponse(status: RequestTypes, friendRequest: any) {
    switch (status) {
      case RequestTypes.approved:
      case RequestTypes.ignored:
        return this.http.patch(`${environment.API_URL}friend-request-service/`, { ...friendRequest, status });
      case RequestTypes.delete:
        return this.http.delete(`${environment.API_URL}friend-request-service/${friendRequest.username}`);
      case RequestTypes.unfriend:
        return this.http.delete(`${environment.API_URL}friend-request-service/unfriend/${friendRequest.username}`);
    }
  }
}
