import { RoleTypes } from './../../../core/models/role-types.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { NavItem } from './../../../core/models/nav-item';

@Component({
  selector: 'app-community-navbar',
  templateUrl: './community-navbar.component.html',
  styleUrls: ['./community-navbar.component.css']
})
export class CommunityNavbarComponent implements OnInit {
  @Input() communityDetails: any;
  activeLink: string;
  communityId: string;
  navItems: NavItem[];

  constructor(private router: Router, private route: ActivatedRoute) {
    this.router.events.subscribe(() => {
      this.initializeNavItems();
    });
  }

  ngOnInit() {
    this.communityId = this.route.snapshot.params.communityId;
    this.initializeNavItems();
  }

  initializeNavItems() {
    this.activeLink = this.router.url;
    this.navItems = [
      new NavItem(
        `/main/communities/${this.communityId}/dashboard`,
        'Dashboard',
        (this.activeLink.indexOf('dashboard') >= 0) ? true : false,
        []
      ),
      new NavItem(
        `/main/communities/${this.communityId}/library`,
        'Library',
        (this.activeLink.indexOf('library') >= 0) ? true : false,
        []
      ),
      new NavItem(
        `/main/communities/${this.communityId}/threads`,
        'Threads',
        (this.activeLink.indexOf('threads') >= 0) ? true : false,
        []
      ),
      new NavItem(
        `/main/communities/${this.communityId}/blogs`,
        'Blogs',
        ((this.activeLink.indexOf('blogs') >= 0) && (this.activeLink.indexOf('library') < 0)) ? true : false,
        []
      ),
      new NavItem(
        `/main/communities/${this.communityId}/training`,
        'Training',
        (this.activeLink.indexOf('training') >= 0) ? true : false,
        []
      ),
      new NavItem(
        `/main/communities/${this.communityId}/stats`,
        'Stats',
        (this.activeLink.indexOf('stats') >= 0) ? true : false,
        []
      )
    ];

    if ((this.communityDetails.role as any[]).includes(RoleTypes.communityModerator)) {
      this.navItems.push(
        new NavItem(
          `/main/communities/${this.communityId}/moderator`,
          'Moderator',
          (this.activeLink.indexOf('moderator') >= 0) ? true : false,
          []
        )
      );
    }
  }

}
