import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-community-stats',
  templateUrl: './community-stats.component.html',
  styleUrls: ['./community-stats.component.css']
})
export class CommunityStatsComponent implements OnInit {
  communityStatsForm: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.communityStatsForm = this.formBuilder.group({

    });
  }

  communityStats() {
    console.log(this.communityStatsForm.value);
  }

}
