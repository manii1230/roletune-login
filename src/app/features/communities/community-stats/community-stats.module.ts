import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunityStatsRoutingModule } from './community-stats-routing.module';
import { CommunityStatsComponent } from './community-stats.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CommunityStatsComponent],
  imports: [
    CommonModule,
    CommunityStatsRoutingModule,
    ReactiveFormsModule
  ]
})
export class CommunityStatsModule { }
