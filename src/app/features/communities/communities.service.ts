import { CommunityViewType } from './../../core/models/community-view-type.model';
import { ReactionTypes } from './../../core/models/reaction-types.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from './../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommunitiesService {
  communityView: CommunityViewType;
  activeCommunity: BehaviorSubject<any>;
  public activeCommunityObservable: Observable<any>;

  constructor(private http: HttpClient) {
    this.communityView = null;
    this.activeCommunity = new BehaviorSubject({});
    this.activeCommunityObservable = this.activeCommunity.asObservable();
  }

  updateActiveCommunityObservable(community: any) {
    this.activeCommunity.next(community);
  }

  setCommunityView(view: string): void {
    switch (view) {
      case CommunityViewType.guestUser:
        this.communityView = CommunityViewType.guestUser;
        break;
      case CommunityViewType.communityUser:
        this.communityView = CommunityViewType.communityUser;
        break;
      default:
        this.communityView = null;
    }
  }

  getCommunityView() {
    return  this.communityView;
  }

  getAllExistingCommunityList() {
    return this.http.get(`${environment.API_URL}community-service/all-communities`);
  }

  getUserCommunityList() {
    return this.http.get(`${environment.API_URL}community-service/user-community-list?userDetails=true`);
  }

  getCommunityList() {
    return this.http.get(`${environment.API_URL}community-service/communities/user`);
  }

  getCommunityDetails(communityId: string) {
    return this.http.get(`${environment.API_URL}community-service/community-details/${communityId}`);
  }

  getCommunityUpdates(payload: any) {
    const { communityId, pageNumber, pageSize } = payload;
    let queryString = pageNumber ? `pageNumber=${pageNumber}` : ``;
    queryString += pageSize ? `&pageSize=${pageSize}` : ``;
    return this.http.get(`${environment.API_URL}community-service/community-updates/${communityId}?${queryString}`);
  }

  updateCommunityUpdates(communityId: string, communityUpdate: any) {
    return this.http.patch(`${environment.API_URL}community-service/community-updates/${communityId}`, communityUpdate);
  }

  getCommunityUserList(communityId: string, payload?: any) {
    const { mentor, level, name } = payload;
    let params = new HttpParams();
    params = params.append('name', name);
    params = params.append('level', level);
    params = params.append('mentor', mentor);

    return (mentor === 'YES') ?
      this.http.get(`${environment.API_URL}mentorship-service/mentors-list/${communityId}`, { params })
      :
      this.http.get(`${environment.API_URL}community-service/${communityId}/users`, { params });
  }

  communityBlogList(payload: any) {
    const { communityId, author, pageNumber, pageSize } = payload;
    let queryString = author ? `author=${author}` : ``;
    queryString += pageNumber ? `&pageNumber=${pageNumber}` : `&pageNumber=1`;
    queryString += pageSize ? `&pageSize=${pageSize}` : `&pageSize=10`;
    return this.http.get(`${environment.API_URL}community-blog-service/${communityId}/blogs?${queryString}`);
  }

  communityBlogDetails(communityId: string, blogId: string, mode?: string) {
    const queryString = mode ? `mode=${mode}` : ``;
    return this.http.get(`${environment.API_URL}community-blog-service/${communityId}/blogs/${blogId}?${queryString}`);
  }

  communityBlog(blog: any, communityId: string) {
    return this.http.post(`${environment.API_URL}community-blog-service/${communityId}/blogs`, blog);
  }

  updateCommunityBlog(payload: any) {
    const { communityId, blogId } = payload;
    return this.http.patch(`${environment.API_URL}community-blog-service/${communityId}/blogs/${blogId}`, payload);
  }

  communityBlogComment(comment: any) {
    return this.http.post(`${environment.API_URL}community-blog-service/${comment.communityId}/blogs/${comment.blogId}/comments`, comment);
  }

  communityBlogCommentVote(payload: any) {
    const { communityId, blogId, _id } = payload;
    return this.http.patch(
      `${environment.API_URL}community-blog-service/${communityId}/blogs/${blogId}/comments/${_id}/votes`,
      payload
    );
  }

  communityBlogReply(comment: any) {
    return this.http.patch(`${environment.API_URL}community-blog-service/${comment.communityId}/blogs/${comment.blogId}/comments/${comment.commentId}`, comment);
  }

  communityAllThreads(payload: any) {
    const { communityId, pageSize = 10, pageNumber = 1, author = null } = payload;
    let queryString = `pageNumber=${pageNumber}`;
    queryString += `&&pageSize=${pageSize}`;
    queryString += author ? `&&author=${author}` : ``;
    return this.http.get(`${environment.API_URL}community-thread-service/${communityId}/threads?${queryString}`);
  }

  communityThreadList(communityId: string, author?: string) {
    // tslint:disable-next-line: max-line-length
    return author ? this.http.get(`${environment.API_URL}community-thread-service/${communityId}/threads?authorId=${author}`) : this.http.get(`${environment.API_URL}community-thread-service/${communityId}/threads`);
  }

  communityThreadDetails(communityId: string, threadId: string) {
    return this.http.get(`${environment.API_URL}community-thread-service/${communityId}/threads/${threadId}`);
  }

  getRelatedThreads(communityId: string, threadId: string) {
    return this.http.get(`${environment.API_URL}community-thread-service/${communityId}/related-threads/${threadId}`);
  }

  updateCommunityThread(payload: any) {
    const { communityId, threadId } = payload;
    return this.http.patch(`${environment.API_URL}community-thread-service/${communityId}/threads/${threadId}`, payload);
  }

  communityThread(thread: any, communityId: string) {
    return this.http.post(`${environment.API_URL}community-thread-service/${communityId}/threads`, thread);
  }

  communityThreadComment(comment: any) {
    return this.http.post(
      `${environment.API_URL}community-thread-service/${comment.communityId}/threads/${comment.threadId}/comments`,
      comment
    );
  }

  communityThreadCommentVote(payload: any) {
    const { communityId, threadId, _id } = payload;
    return this.http.patch(
      `${environment.API_URL}community-thread-service/${communityId}/threads/${threadId}/comments/${_id}/votes`,
      payload
    );
  }

  communityThreadReply(comment: any) {
    return this.http.patch(
      `${environment.API_URL}community-thread-service/${comment.communityId}/threads/${comment.threadId}/comments/${comment._id}`,
      comment
    );
  }

  updateCommunityBlogReaction(blog: any, reaction: ReactionTypes) {
    return this.http.patch(
      `${environment.API_URL}community-blog-service/${blog.communityId}/blogs/${blog._id}/votes`, { reaction, ...blog }
    );
  }

  getInterestLevels() {
    return this.http.get(`${environment.API_URL}interest-graph-service/interest-levels`);
  }

  searchInterestByLevel(data: any) {
    const { level, pageNumber, pageSize, all } = data;
    let queryString = '';
    queryString += level ? `&level=${level}` : ``;
    queryString += all ? `&all=${all}` : ``;
    queryString += pageSize ? `&pageSize=${pageSize}` : ``;
    queryString += pageNumber ? `&pageNumber=${pageNumber}` : ``;
    return this.http.get(`${environment.API_URL}interest-graph-service/search-interest-levels?${queryString}`);
  }

  searchInterest(data: any) {
    const { keyword, matchType, level, pageNumber, pageSize } = data;
    let queryString = '';
    queryString += keyword ? `keyword=${keyword}` : ``;
    queryString += matchType ? `&matchType=${matchType}` : ``;
    queryString += level ? `&level=${level}` : ``;
    queryString += pageSize ? `&pageSize=${pageSize}` : ``;
    queryString += pageNumber ? `&pageNumber=${pageNumber}` : ``;
    return this.http.get(
      `${environment.API_URL}interest-graph-service/search-interest?${queryString}`
    );
  }

  addInterest(interest: any) {
    return this.http.post(`${environment.API_URL}community-service/initial-community`, interest);
  }
}
