import { LoaderTypes } from './../../shared/models/loader-types.enum';
import { CommunitiesBase } from './communities.base';
import { HttpErrorResponse } from '@angular/common/http';
import { CommunitiesService } from './communities.service';
import { AppService } from './../../app.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CommunityViewType } from '../../core/models/community-view-type.model';

@Component({
  selector: 'app-communities',
  templateUrl: './communities.component.html',
  styleUrls: ['./communities.component.css']
})
export class CommunitiesComponent extends CommunitiesBase implements OnInit {
  guestUser: boolean;
  newCommunity: boolean;
  communityId: string;
  loaderTypes = LoaderTypes;

  constructor(private route: ActivatedRoute, private appService: AppService, private communitiesService: CommunitiesService) {
    super();
  }

  ngOnInit() {
    this.guestUser = true;
    this.communityId = this.route.snapshot.params.communityId;
    this.loadCommunityDetails();
  }

  loadCommunityDetails() {
    this.communitiesService.getUserCommunityList().subscribe(
      (res: any) => {
        this.communities = res.result ? res.result.map(userCommunity => userCommunity.communityUser) : [];
        this.appService.updateUserCommunityListObservable(this.communities);

        this.activeCommunity = this.communities ?
          this.communities.find(community => community.communityId === this.communityId) : null;
        console.log(`Com User: `, this.activeCommunity);
        this.communitiesService.updateActiveCommunityObservable(this.activeCommunity);
        this.guestUser = this.activeCommunity ? false : true;
        this.newCommunity = this.activeCommunity ? false : true;
        this.communitiesService.setCommunityView(this.guestUser ? 'GUEST_USER' : 'COMMUNITY_USER');
        if (this.guestUser) {
          this.communitiesService.getCommunityDetails(this.communityId).subscribe(
            (communityDetailsResponse: any) => {
              this.activeCommunity = { communityDetails: communityDetailsResponse.result };
              console.log(`Guest User: `, this.activeCommunity);
              this.communitiesService.updateActiveCommunityObservable(communityDetailsResponse.result);
              this.newCommunity = this.activeCommunity ? false : true;
              this.showLoader = false;
              if (!communityDetailsResponse.success) {
                this.message = {
                  status: communityDetailsResponse.success,
                  message: communityDetailsResponse.message.errorMessage
                };
              }
            },
            (err: HttpErrorResponse) => {
              this.showLoader = false;
              this.message = {
                status: false,
                message: err.message
              };
            }
          );
        } else {
          this.showLoader = false;
        }
      },
      (err: HttpErrorResponse) => {
        this.showLoader = false;
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  addInterest() {
    this.showLoader = true;
    this.communitiesService.addInterest(
      (this.activeCommunity && this.activeCommunity.communityDetails ) ?
      { communityName: this.activeCommunity.communityDetails.name } :
      { interestId: this.communityId }
    ).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        this.activeCommunity = res.result ? { ...res.result.communityUserResult, communityDetails: res.result.community } : null;
        this.communitiesService.updateActiveCommunityObservable(this.activeCommunity);
        this.guestUser = this.activeCommunity ? false : true;
        this.newCommunity = this.activeCommunity ? false : true;
        this.communitiesService.setCommunityView(this.guestUser ? 'GUEST_USER' : 'COMMUNITY_USER');
        this.showLoader = false;
        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
        this.showLoader = false;
      }
    );
  }
}
