export class CommunitiesBase {
    showLoader: boolean;
    message: any;
    activeCommunity: any;
    communities: any[];

    constructor() {
        this.showLoader = true;
        this.message = null;
        this.activeCommunity = null;
        this.communities = [];
    }
}