import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-community-training',
  templateUrl: './community-training.component.html',
  styleUrls: ['./community-training.component.css']
})
export class CommunityTrainingComponent implements OnInit {
  communityTrainingForm: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.communityTrainingForm = this.formBuilder.group({

    });
  }

  communityTraining() {
    console.log(this.communityTrainingForm.value);
  }

}
