import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityTrainingComponent } from './community-training.component';

describe('CommunityTrainingComponent', () => {
  let component: CommunityTrainingComponent;
  let fixture: ComponentFixture<CommunityTrainingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityTrainingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
