import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunityTrainingRoutingModule } from './community-training-routing.module';
import { CommunityTrainingComponent } from './community-training.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CommunityTrainingComponent],
  imports: [
    CommonModule,
    CommunityTrainingRoutingModule,
    ReactiveFormsModule
  ]
})
export class CommunityTrainingModule { }
