import { CommunityModeratorGuard } from './../../core/guards/community-moderator.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommunitiesComponent } from './communities.component';

const routes: Routes = [
  {
    path: ':communityId',
    component: CommunitiesComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './community-dashboard/community-dashboard.module#CommunityDashboardModule',
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'blogs',
        loadChildren: './community-blogs/community-blogs.module#CommunityBlogsModule',
        data: {
          title: 'Blogs'
        }
      },
      {
        path: 'library',
        loadChildren: './community-library/community-library.module#CommunityLibraryModule',
        data: {
          title: 'Library'
        }
      },
      {
        path: 'threads',
        loadChildren: './community-threads/community-threads.module#CommunityThreadsModule',
        data: {
          title: 'Threads'
        }
      },
      {
        path: 'training',
        loadChildren: './community-training/community-training.module#CommunityTrainingModule',
        data: {
          title: 'Training'
        }
      },
      {
        path: 'stats',
        loadChildren: './community-stats/community-stats.module#CommunityStatsModule',
        data: {
          title: 'Stats'
        }
      },
      {
        path: 'moderator',
        canActivateChild: [ CommunityModeratorGuard ],
        loadChildren: './community-moderator/community-moderator.module#CommunityModeratorModule',
        data: {
          title: 'Community Moderator'
        }
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    loadChildren: './communities-list/communities-list.module#CommunitiesListModule'
  },
  {
    path: '**',
    redirectTo: 'error/404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunitiesRoutingModule { }
