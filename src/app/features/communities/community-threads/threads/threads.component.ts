import { MessageTypes } from './../../../../core/models/message-types';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { LoadMore } from './../../../../core/base/load-more.base';
import { CommunitiesService } from './../../communities.service';
import { AppService } from './../../../../app.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReactionTypes } from 'src/app/core/models/reaction-types.model';

@Component({
  selector: 'app-threads',
  templateUrl: './threads.component.html',
  styleUrls: ['./threads.component.css']
})
export class ThreadsComponent extends LoadMore implements OnInit {
  user: any;
  threadFilter: string;
  communityId: string;
  threads: any[];

  message: any;
  replyForm: FormGroup;

  constructor(
    private appService: AppService,
    private router: Router,
    private communitiesService: CommunitiesService,
    private formBuilder: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
    this.threadFilter = '';
    this.threads = [];
    this.message = null;
    this.communityId = this.router.url.split('/')[3];

    this.replyForm = this.formBuilder.group({
      comment: ['', [Validators.required, Validators.minLength(1)] ],
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.pageNumber = 1;
        this.loadThreads();
      }
    });
  }

  searchThread(searchText: string) {
    this.threadFilter = searchText;
  }


  loadThreads() {
    this.config.loading = true;
    const payload = {
      communityId: this.communityId,
      pageSize: this.pageSize,
      pageNumber: this.pageNumber
    };
    this.communitiesService.communityAllThreads(payload).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.config.loadMore = !(res.result && res.result.length < this.pageSize);
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          this.threads = this.threads.concat(res.result);
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.config.loading = false;
        this.config.loadMore = false;
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  hideThread(index: number) {
    const payload: any = {
      ...this.threads[index]
    };
    payload.communityId = this.communityId,
    payload.threadId = payload._id;
    if (this.user.username === payload.author.username) {
      payload.displayFlag = false;
    } else {
      payload.hideBlog = true;
    }

    this.communitiesService.updateCommunityThread(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          this.threads.splice(index, 1);
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
      }
    );
  }

  openThread(thread: any): void {
    window.open(`#/main/communities/${this.communityId}/threads/thread-details/${thread._id}`);
  }

  createCommentReply(comment: any) {
    const payload = { ...comment, replier: this.user, ...this.replyForm.value };
    this.communitiesService.communityThreadReply(payload).subscribe(
      (res: any) => {
        comment.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          comment.replies = res.result;
          this.replyForm.reset();
          this.appService.removeMessage(comment, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        comment.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
      }
    );
  }

  getCommentVotes(comment: any, reactionType: ReactionTypes): number | string {
    return comment.votes && comment.votes.filter(vote => vote.vote === reactionType).length ?
      comment.votes.filter(vote => vote.vote === reactionType).length : '';
  }

  commentReaction(comment: any, reaction: ReactionTypes) {
    if (comment.votes) {
      const existingReaction = comment.votes.find(vote => vote.vote === reaction && vote.username === this.user.username);
      if (existingReaction) {
        return;
      }
    }
    const payload = { ...comment, reaction };
    this.communitiesService.communityThreadCommentVote(payload).subscribe(
      (res: any) => {
        comment.reactionMessage = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.result && res.result.votes) {
          comment.votes = res.result.votes;
          this.appService.removeMessage(comment, 'reactionMessage');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
    });
  }
}
