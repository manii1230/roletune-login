import { ReactiveFormsModule } from '@angular/forms';
import { NgxSummernoteModule } from 'ngx-summernote';
import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThreadsRoutingModule } from './threads-routing.module';
import { ThreadsComponent } from './threads.component';

@NgModule({
  declarations: [ThreadsComponent],
  imports: [
    CommonModule,
    ThreadsRoutingModule,
    NgxSummernoteModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class ThreadsModule { }
