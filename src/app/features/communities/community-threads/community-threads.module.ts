import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunityThreadsRoutingModule } from './community-threads-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CommunityThreadsRoutingModule
  ]
})
export class CommunityThreadsModule { }
