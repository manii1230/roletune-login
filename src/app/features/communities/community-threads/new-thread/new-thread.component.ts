import { MessageTypes } from './../../../../core/models/message-types';
import { CommunityContentStatus } from '../../../../core/models/community-content.model';
import { CommunityViewType } from './../../../../core/models/community-view-type.model';
import { AppService } from './../../../../app.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { CommunitiesService } from './../../communities.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-thread',
  templateUrl: './new-thread.component.html',
  styleUrls: ['./new-thread.component.css']
})
export class NewThreadComponent implements OnInit {
  addThreadForm: FormGroup;
  communityId: string;
  message: any;
  viewType: CommunityViewType;

  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    private router: Router,
    private communitiesService: CommunitiesService
  ) { }

  ngOnInit() {
    this.message = null;
    this.viewType = this.communitiesService.getCommunityView();
    this.communityId = this.router.url.split('/')[3];
    this.addThreadForm = this.formBuilder.group({
      type: ['', Validators.required],
      status: [CommunityContentStatus.open],
      content: ['', Validators.required],
      tags: ['']
    });
  }

  addThread() {
    if (this.viewType === CommunityViewType.guestUser) {
      return this.message = {
        status: false,
        message: MessageTypes.guestUserActionFailed
      };
    }
    this.addThreadForm.patchValue({ status: CommunityContentStatus.open });
    this.communitiesService.communityThread(this.addThreadForm.value, this.communityId).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          this.addThreadForm.reset();
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }
}
