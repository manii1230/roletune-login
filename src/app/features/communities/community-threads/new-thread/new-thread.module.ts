import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSummernoteModule } from 'ngx-summernote';
import { NewThreadRoutingModule } from './new-thread-routing.module';
import { NewThreadComponent } from './new-thread.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [NewThreadComponent],
  imports: [
    CommonModule,
    NgxSummernoteModule,
    NewThreadRoutingModule,
    ReactiveFormsModule
  ]
})
export class NewThreadModule { }
