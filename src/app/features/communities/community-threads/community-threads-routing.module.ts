import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './threads/threads.module#ThreadsModule'
  },
  {
    path: 'new-thread',
    loadChildren: './new-thread/new-thread.module#NewThreadModule'
  },
  {
    path: 'thread-list',
    loadChildren: './thread-list/thread-list.module#ThreadListModule'
  },
  {
    path: 'thread-details/:threadId',
    loadChildren: './thread-details/thread-details.module#ThreadDetailsModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunityThreadsRoutingModule { }
