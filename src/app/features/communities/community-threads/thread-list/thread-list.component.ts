import { CommunitiesService } from './../../communities.service';
import { AppService } from './../../../../app.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-thread-list',
  templateUrl: './thread-list.component.html',
  styleUrls: ['./thread-list.component.less']
})
export class ThreadListComponent implements OnInit {
  user: any;
  threadFilter: string;
  communityId: string;
  threadAuthor: string;
  threadList: any[];

  // tslint:disable-next-line: max-line-length
  constructor(private router: Router, private route: ActivatedRoute, private appService: AppService, private communitiesService: CommunitiesService) {}

  ngOnInit() {
    this.threadFilter = '';
    this.threadList = [];
    this.communityId = this.router.url.split('/')[3];
    this.appService.activeLoggedInUser.subscribe((user: any) => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.router.events.subscribe(event => {
          if (event instanceof NavigationEnd) {
            this.loadThreads();
          }
        });
        this.loadThreads();
      }
    });
  }

  searchThread(searchText) {
    this.threadFilter = searchText;
  }

  loadThreads() {
    this.threadAuthor = this.route.snapshot.params.threadAuthor;
    this.communitiesService.communityThreadList(this.communityId, (this.threadAuthor === 'all') ? null : this.user.username).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.threadList = res.result;
        }
      },
      (err) => {

      }
    );
  }

  viewThreadDetails(idx: number) {
    this.router.navigate([`/main/communities/${this.communityId}/threads/thread-details/${this.threadList[idx]._id}`]);
  }
}
