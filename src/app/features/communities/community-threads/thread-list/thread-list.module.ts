import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThreadListRoutingModule } from './thread-list-routing.module';
import { ThreadListComponent } from './thread-list.component';

@NgModule({
  declarations: [ThreadListComponent],
  imports: [
    CommonModule,
    ThreadListRoutingModule,
    SharedModule
  ]
})
export class ThreadListModule { }
