import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThreadListComponent } from './thread-list.component';

const routes: Routes = [
  {
    path: ':threadAuthor',
    component: ThreadListComponent
  },
  {
    path: '',
    redirectTo: 'all',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThreadListRoutingModule { }
