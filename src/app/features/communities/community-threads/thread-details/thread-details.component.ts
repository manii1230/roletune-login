import { ReactionTypes } from 'src/app/core/models/reaction-types.model';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { CommunitiesService } from './../../communities.service';
import { AppService } from './../../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CommunityThreadType } from './../../../../core/models/community-content.model';
import { MessageTypes } from './../../../../core/models/message-types';

@Component({
  selector: 'app-thread-details',
  templateUrl: './thread-details.component.html',
  styleUrls: ['./thread-details.component.css']
})
export class ThreadDetailsComponent implements OnInit {
  user: any;
  commentForm: FormGroup;
  replyForm: FormGroup;
  thread: any;
  threadId: string;
  communityId: string;
  message: any;

  // tslint:disable-next-line: max-line-length
  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private route: ActivatedRoute,
    private communitiesService: CommunitiesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.message = null;

    this.commentForm = this.formBuilder.group({
      comment: ['', [Validators.required, Validators.minLength(1)] ],
      communityId: [''],
      threadId: ['']
    });

    this.replyForm = this.formBuilder.group({
      comment: ['', [Validators.required, Validators.minLength(1)] ],
    });

    this.threadId = this.route.snapshot.params.threadId;
    this.communityId = this.router.url.split('/')[3];
    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.loadThreadDetails();
      }
    });
  }

  loadThreadDetails() {
    this.communitiesService.communityThreadDetails(this.communityId, this.threadId).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.thread = res.result;
          // tslint:disable-next-line: max-line-length
          this.thread.comments = (this.thread.comments && (this.thread.comments.length === 1) && (JSON.stringify(this.thread.comments[0]) === '{}')) ? [] : this.thread.comments;
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
      }
    );
  }

  createComment() {
    if (this.thread.userId === this.user.username) {
      this.message = {
        status: false,
        message: MessageTypes.authorPreventAction
      };
      this.appService.removeMessage(this, 'message');
      return;
    }
    this.commentForm.patchValue({
      communityId: this.communityId,
      threadId: this.threadId
    });
    this.communitiesService.communityThreadComment(this.commentForm.value).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success && res.result) {
          res.result.commenter = this.user;
          this.thread.comments.push(res.result);
          this.commentForm.reset();
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.message ? err.error.message : err.message
        };
      }
    );
  }

  createReply(idx: number) {
    this.replyForm.patchValue({
      communityId: this.communityId,
      threadId: this.threadId,
      commentId: this.thread.comments[idx]._id,
      replier: {
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        username: this.user.username
      }
    });
    this.communitiesService.communityThreadReply(this.replyForm.value).subscribe(
      (res: any) => {
        this.thread.comments[idx].message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success && res.result) {
          if (this.thread.comments[idx].replies && this.thread.comments[idx].replies.length) {
            this.thread.comments[idx].replies.push(res.result);
          } else {
            this.thread.comments[idx].replies = [res.result];
          }
          this.thread.comments[idx].showReplyForm = false;
          this.replyForm.reset();
          this.appService.removeMessage(this.thread.comments[idx], 'message');
        }
      },
      (err) => {
        this.thread.comments[idx].message = {
          status: false,
          message: err.error && err.error.message ? err.error.message : err.message
        };
      }
    );
  }

  createCommentReply(comment: any) {
    const payload = { ...comment, replier: this.user, ...this.replyForm.value };
    this.communitiesService.communityThreadReply(payload).subscribe(
      (res: any) => {
        comment.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          comment.replies = res.result;
          comment.showReplies = true;
          this.replyForm.reset();
          this.appService.removeMessage(comment, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        comment.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
      }
    );
  }

  getVotesCount(comment: any, reactionType: ReactionTypes): number | string {
    return comment.votes && comment.votes.filter(vote => vote.vote === reactionType).length ?
      comment.votes.filter(vote => vote.vote === reactionType).length : '';
  }

  commentReaction(comment: any, reaction: ReactionTypes) {
    if (comment.votes) {
      const existingReaction = comment.votes.find(vote => vote.vote === reaction && vote.username === this.user.username);
      if (existingReaction) {
        return;
      }
    }
    const payload = { ...comment, reaction };
    this.communitiesService.communityThreadCommentVote(payload).subscribe(
      (res: any) => {
        comment.reactionMessage = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.result && res.result.votes) {
          comment.votes = res.result.votes;
          this.appService.removeMessage(comment, 'reactionMessage');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
    });
  }

  threadReaction(thread: any, reaction: ReactionTypes) {
    const payload = {
      ...thread,
      threadId: thread._id,
      reaction
    };
    this.communitiesService.updateCommunityThread(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          thread.votes = (res.result && res.result.votes) ? res.result.votes : [];
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
      }
    );
  }
}
