import { HttpErrorResponse } from '@angular/common/http';
import { CommunitiesService } from './../../../communities.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-related-threads',
  templateUrl: './related-threads.component.html',
  styleUrls: ['./related-threads.component.css']
})
export class RelatedThreadsComponent implements OnInit {
  threadId: string;
  communityId: string;
  relatedThreads: any[];
  message: any;
  constructor(private route: ActivatedRoute, private router: Router, private communitiesService: CommunitiesService) { }

  ngOnInit() {
    this.message = null;
    this.relatedThreads = [];
    this.threadId = this.route.snapshot.params.threadId;
    this.communityId = this.router.url.split('/')[3];
    this.loadRelatedThreads();
  }

  loadRelatedThreads() {
    this.communitiesService.getRelatedThreads(this.communityId, this.threadId).subscribe(
      (res: any) => {
        if (res.success) {
          this.relatedThreads = res.result;
        } else {
          this.message = {
            status: res.success,
            message: res.success ? res.message : res.message.errorMessage
          };
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.error && err.error.message ? err.error.message : err.message
        };
      }
    );
  }

  openRelatedThread(thread: any) {
    window.open(`#/main/communities/${thread.communityId}/threads/thread-details/${thread._id}`);
  }
}
