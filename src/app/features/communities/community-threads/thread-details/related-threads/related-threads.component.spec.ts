import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedThreadsComponent } from './related-threads.component';

describe('RelatedThreadsComponent', () => {
  let component: RelatedThreadsComponent;
  let fixture: ComponentFixture<RelatedThreadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedThreadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedThreadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
