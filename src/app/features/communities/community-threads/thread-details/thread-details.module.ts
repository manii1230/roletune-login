import { NgxSummernoteModule } from 'ngx-summernote';
import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThreadDetailsRoutingModule } from './thread-details-routing.module';
import { ThreadDetailsComponent } from './thread-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RelatedThreadsComponent } from './related-threads/related-threads.component';

@NgModule({
  declarations: [ThreadDetailsComponent, RelatedThreadsComponent],
  imports: [
    CommonModule,
    ThreadDetailsRoutingModule,
    ReactiveFormsModule,
    NgxSummernoteModule,
    SharedModule
  ]
})
export class ThreadDetailsModule { }
