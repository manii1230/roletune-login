import { CommunitiesService } from './../communities.service';
import { AppService } from './../../../app.service';
import { NavItem } from './../../../core/models/nav-item';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-community-sidebar',
  templateUrl: './community-sidebar.component.html',
  styleUrls: ['./community-sidebar.component.css']
})
export class CommunitySidebarComponent implements OnInit {
  @Input() communityDetails: any;
  colors: any;
  mentorshipData: any;
  showMentorshipTree: boolean;
  communityId: string;
  activeLink: string;
  linkName: string;
  sidebarList: Array<any>;

  constructor(private router: Router, private route: ActivatedRoute, private appService: AppService) {
    this.router.events.subscribe(() => {
      this.activeLink = this.router.url;
      this.initializeSidebarItems();
    });
   }

  ngOnInit() {
    this.showMentorshipTree = false;
    this.colors = null;
    this.sidebarList = [];
    this.mentorshipData = {};
    this.communityId = this.route.snapshot.params.communityId;
    this.activeLink = this.router.url;
    this.initializeSidebarItems();
    if (this.communityDetails && Object.keys(this.communityDetails).length) {
      this.generateMentorshipTree();
    }
  }

  initializeSidebarItems() {
    if (this.router.url.indexOf('threads') >= 0) {
      this.linkName = 'Threads';
      this.sidebarList = [
        new NavItem(
          `/main/communities/${this.communityId}/threads/new-thread`,
          'Start New Thread',
          (this.activeLink.indexOf('new-thread') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/threads/thread-list/my-threads`,
          'My Threads',
          (this.activeLink.indexOf('my-threads') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/threads/thread-list/all`,
          'All',
          (this.activeLink.indexOf('all') >= 0) ? true : false,
          []
        )
      ];
    } else
    if (this.router.url.indexOf('blogs') >= 0 && this.router.url.indexOf('library') < 0) {
      this.linkName = 'Blogs';
      this.sidebarList = [
        new NavItem(
          `/main/communities/${this.communityId}/blogs/new-blog`,
          'Write a Blog',
          (this.activeLink.indexOf('new-blog') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/blogs/blog-list/my-blogs`,
          'My Blogs',
          (this.activeLink.indexOf('my-blogs') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/blogs/blog-list/all`,
          'All',
          (this.activeLink.indexOf('all') >= 0) ? true : false,
          []
        )
      ];
    } else
    if (this.router.url.indexOf('library') >= 0) {
      this.linkName = 'Library';
      this.sidebarList = [
        new NavItem(
          `/main/communities/${this.communityId}/library/contribute`,
          'Contribute',
          (this.activeLink.indexOf('contribute') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/library/search`,
          'Search',
          (this.activeLink.indexOf('search') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/library/category/books`,
          'Books',
          (this.activeLink.indexOf('books') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/library/category/videos`,
          'Videos',
          (this.activeLink.indexOf('videos') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/library/category/audio-books`,
          'Audio Books',
          (this.activeLink.indexOf('audio-books') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/library/category/blogs`,
          'Blogs',
          (this.activeLink.indexOf('blogs') >= 0) ? true : false,
          []
        ),
        new NavItem(
          `/main/communities/${this.communityId}/library/category/others`,
          'Others',
          (this.activeLink.indexOf('others') >= 0) ? true : false,
          []
        )
      ];
    } else
    if (this.router.url.indexOf('dashboard') >= 0) {
      this.linkName = 'Dashboard';
      this.sidebarList = [];
    } else
    if (this.router.url.indexOf('stats') >= 0) {
      this.linkName = 'Stats';
      this.sidebarList = [
        new NavItem(
          `/main/communities/${this.communityId}/stats`,
          'Stats - Coming Soon',
          (this.activeLink.indexOf('stats') >= 0) ? true : false,
          []
        )
      ];
    } else
    if (this.router.url.indexOf('training') >= 0) {
      this.linkName = 'Training';
      this.sidebarList = [
        new NavItem(
          `/main/communities/${this.communityId}/training`,
          'Training Sidebar - Coming Soon',
          (this.activeLink.indexOf('training') >= 0) ? true : false,
          []
        )
      ];
    } else
    if (this.router.url.indexOf('moderator') >= 0) {
      this.linkName = 'Moderator';
      this.sidebarList = [
        new NavItem(
          `/main/communities/${this.communityId}/moderator`,
          'Moderator Sidebar - Coming Soon',
          (this.activeLink.indexOf('moderator') >= 0) ? true : false,
          []
        )
      ];
    }
  }

  generateMentorshipTree() {
    this.mentorshipData = {
      username: this.communityDetails.userId,
      mentor: this.communityDetails.mentor ? this.communityDetails.mentor : '',
      mentees: this.communityDetails.mentees ? this.communityDetails.mentees : []
    };
    this.colors = this.appService.getMentorshipTreeColors();
    this.showMentorshipTree = true;
  }

  getUserRole(role: any) {
    return this.appService.getRoleName(role);
  }
}
