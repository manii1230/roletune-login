import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewBlogComponent } from './new-blog.component';

const routes: Routes = [
  {
    path: '',
    component: NewBlogComponent
  },
  {
    path: ':blogId',
    component: NewBlogComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewBlogRoutingModule { }
