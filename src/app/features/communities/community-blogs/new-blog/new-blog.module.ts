import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSummernoteModule } from 'ngx-summernote';
import { NewBlogRoutingModule } from './new-blog-routing.module';
import { NewBlogComponent } from './new-blog.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [NewBlogComponent],
  imports: [
    CommonModule,
    NewBlogRoutingModule,
    NgxSummernoteModule,
    ReactiveFormsModule
  ]
})
export class NewBlogModule { }
