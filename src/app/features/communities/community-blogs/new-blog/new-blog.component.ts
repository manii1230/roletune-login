import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { CommunitiesService } from './../../communities.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-blog',
  templateUrl: './new-blog.component.html',
  styleUrls: ['./new-blog.component.css']
})
export class NewBlogComponent implements OnInit {
  addBlogForm: FormGroup;
  communityId: string;
  
  message: any;

  blogId: string;
  blogDetails: any;

  constructor(
    private formBuilder: FormBuilder,
    private communitiesService: CommunitiesService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.blogId = null;
    this.blogDetails = null;

    this.addBlogForm = this.formBuilder.group({
      title: ['', Validators.required],
      status: ['PUBLISHED'],
      content: ['']
    });

    this.communityId = this.router.url.split('/')[3];

    this.route.paramMap.subscribe(params => {
      if (params.has('blogId')) {
        this.blogId = params.get('blogId');
        this.communitiesService.communityBlogDetails(this.communityId, this.blogId, 'EDIT').subscribe(
          (res: any) => {
            if (res.success && res.result) {
              this.blogDetails = res.result;
              this.addBlogForm.patchValue({
                title: this.blogDetails.title,
                status: this.blogDetails.status,
                content: this.blogDetails.content
              });
            }

            if (!res.success) {
              this.message = {
                status: res.success,
                message: res.message.errorMessage
              };
            }
          },
          (err: HttpErrorResponse) => {
            this.message = {
              status: false,
              message: err.message
            };
          }
        );
      }
    });
  }

  resetForm() {
    this.addBlogForm.reset();
    if (this.blogId && this.blogDetails) {
      this.addBlogForm.patchValue({
        title: this.blogDetails.title,
        status: this.blogDetails.status,
        content: this.blogDetails.content
      });
    }
  }

  addBlog() {
    if ( this.blogId ) {
      const payload = { ...this.addBlogForm.value, _id: this.blogId, communityId: this.communityId };
      this.communitiesService.updateCommunityBlog(payload).subscribe(
        (res: any) => {
          this.message = {
            status: res.success,
            message: res.success ? res.message : res.message.errorMessage
          };
          if (res.success && res.result) {
            this.addBlogForm.reset();
            this.blogId = null;
          }
        },
        (err: HttpErrorResponse) => {
          this.message = {
            status: false,
            message: err.message
          };
        }
      );
    } else {
      this.communitiesService.communityBlog(this.addBlogForm.value, this.communityId).subscribe(
        (res: any) => {
          this.message = {
            status: res.success,
            message: res.success ? res.message : res.message.errorMessage
          };
          if (res.success && res.result) {
            this.addBlogForm.reset();
          }
        },
        (err: HttpErrorResponse) => {
          this.message = {
            status: false,
            message: err.message
          };
        }
      );
    }
  }

}
