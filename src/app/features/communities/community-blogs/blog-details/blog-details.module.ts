import { SharedModule } from './../../../../shared/shared.module';
import { NgxSummernoteModule } from 'ngx-summernote';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogDetailsRoutingModule } from './blog-details-routing.module';
import { BlogDetailsComponent } from './blog-details.component';

@NgModule({
  declarations: [BlogDetailsComponent],
  imports: [
    CommonModule,
    BlogDetailsRoutingModule,
    ReactiveFormsModule,
    NgxSummernoteModule,
    SharedModule
  ]
})
export class BlogDetailsModule { }
