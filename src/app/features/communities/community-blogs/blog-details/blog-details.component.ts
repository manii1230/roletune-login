import { CommunityViewType } from './../../../../core/models/community-view-type.model';
import { ReactionTypes } from './../../../../core/models/reaction-types.model';
import { CommunityPost } from './../../community-post.base';
import { AppService } from './../../../../app.service';
import { CommunitiesService } from './../../communities.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.css']
})
export class BlogDetailsComponent extends CommunityPost implements OnInit {
  user: any;
  commentForm: FormGroup;
  replyForm: FormGroup;
  blogId: string;
  communityId: string;
  reactionTypes: any;

  viewType: CommunityViewType;

  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private route: ActivatedRoute,
    public communitiesService: CommunitiesService,
    private router: Router
  ) {
    super(communitiesService);
   }

  ngOnInit() {
    this.reactionTypes = ReactionTypes;
    this.viewType = this.communitiesService.getCommunityView();
    this.commentForm = this.formBuilder.group({
      comment: ['', [Validators.required, Validators.minLength(1)] ],
      communityId: [''],
      blogId: ['']
    });

    this.replyForm = this.formBuilder.group({
      communityId: [''],
      blogId: [''],
      comment: ['', [Validators.required, Validators.minLength(1)] ],
      commentId: [''],
      replier: [null]
    });

    this.blogId = this.route.snapshot.params.blogId;
    this.communityId = this.router.url.split('/')[3];
    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.loadBlogDetails();
      }
    });
  }

  loadBlogDetails() {
    this.communitiesService.communityBlogDetails(this.communityId, this.blogId).subscribe(
      (res: any) => {
        this.blog = res.result;
        // tslint:disable-next-line: max-line-length
        this.blog.comments = (this.blog.comments && (this.blog.comments.length === 1) && (JSON.stringify(this.blog.comments[0]) === '{}')) ? [] : this.blog.comments;
      },
      (err) => {

      }
    );
  }

  createComment() {
    this.commentForm.patchValue({
      communityId: this.communityId,
      blogId: this.blogId
    });
    this.communitiesService.communityBlogComment(this.commentForm.value).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          res.result.commenter = this.user;
          this.blog.comments.push(res.result);
          this.commentForm.reset();
        }
      },
      (err) => {

      }
    );
  }

  createReply(idx: number) {
    this.replyForm.patchValue({
      communityId: this.communityId,
      blogId: this.blogId,
      commentId: this.blog.comments[idx]._id,
      replier: {
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        username: this.user.username
      }
    });
    this.communitiesService.communityBlogReply(this.replyForm.value).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          if (this.blog.comments[idx].replies && this.blog.comments[idx].replies.length) {
            this.blog.comments[idx].replies.push(res.result);
          } else {
            this.blog.comments[idx].replies = [res.result];
          }
          this.blog.comments[idx].showReplyForm = false;
          this.replyForm.reset();
        }
      },
      (err) => {

      }
    );
  }

  voteComment() {

  }

  shareThread() {
  }
  downloadThread() {
  }

  getVotes(rection: ReactionTypes): number {
    const voteList = this.blog.votes ? this.blog.votes.filter(blogVote => blogVote.vote === rection) : [];
    return voteList.length ? voteList.length : null;
  }
}
