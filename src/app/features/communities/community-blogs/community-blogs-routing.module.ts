import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './blogs/blogs.module#BlogsModule'
  },
  {
    path: 'new-blog',
    loadChildren: './new-blog/new-blog.module#NewBlogModule'
  },
  {
    path: 'blog-list',
    loadChildren: './blog-list/blog-list.module#BlogListModule'
  },
  {
    path: 'blog-details/:blogId',
    loadChildren: './blog-details/blog-details.module#BlogDetailsModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunityBlogsRoutingModule { }
