import { SharedModule } from './../../../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogListRoutingModule } from './blog-list-routing.module';
import { BlogListComponent } from './blog-list.component';

@NgModule({
  declarations: [BlogListComponent],
  imports: [
    CommonModule,
    BlogListRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class BlogListModule { }
