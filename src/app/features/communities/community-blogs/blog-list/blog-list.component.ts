import { HttpErrorResponse } from '@angular/common/http';
import { LoadMore } from './../../../../core/base/load-more.base';
import { CommunitiesService } from './../../communities.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AppService } from './../../../../app.service';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent extends LoadMore implements OnInit {
  user: any;
  blogAuthor: string;
  blogs: any[];
  blogFilter: string;
  communityId: string;

  message: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private appService: AppService,
    private communitiesService: CommunitiesService
  ) {
    super();
  }

  ngOnInit() {
    this.blogFilter = '';
    this.blogs = [];
    this.communityId = this.router.url.split('/')[3];
    this.message = null;
    this.pageNumber = 1;
    this.appService.activeLoggedInUser.subscribe((user: any) => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.loadBlogs();
        this.router.events.subscribe(event => {
          if (event instanceof NavigationEnd) {
            this.blogs = [];
            this.loadBlogs();
          }
        });
      }
    });
  }

  searchBlog(searchText: string) {
    this.blogFilter = searchText;
  }

  loadBlogs() {
    this.blogAuthor = this.route.snapshot.params.blogAuthor;
    const payload = {
      communityId: this.communityId,
      author: ( this.blogAuthor === 'all' ) ? null : this.user.username,
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };
    this.communitiesService.communityBlogList(payload).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.config.loadMore = !(res.result && res.result.length < this.pageSize);
        this.blogs = this.blogs.concat(res.result ? res.result : []);

        if (!res.success) {
          this.message = {
            status: false,
            message: res.message.errorMessage
          };
        }
      },
      (err: HttpErrorResponse) => {
        this.config.loading = false;
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  getBlogTitle(blog: any): string {
    return (blog.title.split(' ').length > 5) ? `${blog.title.split(' ').slice(0, 5).join(' ')}...` : blog.title;
  }

  getBlogVotes(blog: any): string {
    if (!blog.votes || blog.votes.length === 0) {
      return '- / -';
    }
    const upvotes = blog.votes.filter(vote => vote.vote === 'UPVOTE');
    return `${upvotes.length} / ${blog.votes.length - upvotes.length}`;
  }

  viewBlogDetails(i) {
    this.blogs[i].status === 'PUBLISHED' ?
    window.open(`#/main/communities/${this.communityId}/blogs/blog-details/${this.blogs[i]._id}`) :
    this.router.navigate([`/main/communities/${this.communityId}/blogs/new-blog/${this.blogs[i]._id}`]);
  }
}
