import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunityBlogsRoutingModule } from './community-blogs-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CommunityBlogsRoutingModule
  ]
})
export class CommunityBlogsModule { }
