import { ReactionTypes } from './../../../../core/models/reaction-types.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { LoadMore } from './../../../../core/base/load-more.base';
import { CommunitiesService } from './../../communities.service';
import { Router } from '@angular/router';
import { AppService } from './../../../../app.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent extends LoadMore implements OnInit {
  user: any;
  blogFilter: string;
  communityId: string;
  blogs: any[];

  message: any;
  replyForm: FormGroup;

  constructor(
    private appService: AppService,
    private router: Router,
    private communitiesService: CommunitiesService,
    private formBuilder: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
    this.blogFilter = '';
    this.blogs = [];
    this.message = null;
    this.communityId = this.router.url.split('/')[3];

    this.replyForm = this.formBuilder.group({
      comment: ['', [Validators.required, Validators.minLength(1)] ],
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.pageNumber = 1;
        this.loadBlogs();
      }
    });
  }

  searchBlog(searchText: string) {
    this.blogFilter = searchText;
  }

  loadBlogs() {
    this.config.loading = true;
    const payload = {
      communityId: this.communityId,
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };
    this.communitiesService.communityBlogList(payload).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.config.loadMore = !(res.result && res.result.length < this.pageSize );
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          this.blogs = this.blogs.concat(res.result);
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.config.loading = false;
        this.config.loadMore = false;
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  hideBlog(index: number) {
    const payload: any = {
      ...this.blogs[index]
    };
    payload.communityId = this.communityId,
    payload.blogId = payload._id;
    if (this.user.username === payload.author.username) {
      payload.displayFlag = false;
    } else {
      payload.hideBlog = true;
    }

    this.communitiesService.updateCommunityBlog(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          this.blogs.splice(index, 1);
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
      }
    );
  }

  openBlog(blog: any): void {
    window.open(`#/main/communities/${this.communityId}/blogs/blog-details/${blog._id}`);
  }

  getBlogVotes(blog: any, reactionType: ReactionTypes): number | string {
    return blog.votes && blog.votes.filter(vote => vote.vote === reactionType).length ?
      blog.votes.filter(vote => vote.vote === reactionType).length : '';
  }

  blogReaction(blog: any, reaction: ReactionTypes) {
    if (blog.votes) {
      const existingReaction = blog.votes.find(vote => vote.vote === reaction && vote.username === this.user.username);
      if (existingReaction) {
        return;
      }
    }
    this.communitiesService.updateCommunityBlogReaction(blog, reaction).subscribe(
      (res: any) => {
        blog.reactionMessage = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.result && res.result.votes) {
          blog.votes = res.result.votes;
          this.appService.removeMessage(blog, 'reactionMessage');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: (err.error && err.error.message) ? err.error.message : err.message
        };
    });
  }
}
