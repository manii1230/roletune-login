import { CommunitiesService } from './communities.service';
import { ReactionTypes } from './../../core/models/reaction-types.model';
export class CommunityPost {
  blog: any;
  constructor(public communitiesService: CommunitiesService) {
    this.blog = {};
  }

  blogReaction(blog: any, reaction: ReactionTypes) {
    this.communitiesService.updateCommunityBlogReaction(blog, reaction).subscribe(
      (res: any) => {
        if (res.success) {
          this.blog.votes = (res.result && res.result.votes) ? res.result.votes : [];
        }
      },
      (err) => {

    });
  }
}
