import { SharedModule } from './../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunitiesRoutingModule } from './communities-routing.module';
import { CommunitiesComponent } from './communities.component';
import { CommunitySidebarComponent } from './community-sidebar/community-sidebar.component';
import { CommunityNavbarComponent } from './community-navbar/community-navbar.component';

@NgModule({
  declarations: [
    CommunitiesComponent,
    CommunitySidebarComponent,
    CommunityNavbarComponent
  ],
  imports: [
    CommonModule,
    CommunitiesRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  providers: [
  ]
})
export class CommunitiesModule { }
