import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunityModeratorRoutingModule } from './community-moderator-routing.module';
import { CommunityModeratorComponent } from './community-moderator.component';

@NgModule({
  declarations: [CommunityModeratorComponent],
  imports: [
    CommonModule,
    CommunityModeratorRoutingModule
  ]
})
export class CommunityModeratorModule { }
