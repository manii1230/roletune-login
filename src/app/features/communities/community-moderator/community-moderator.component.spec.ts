import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityModeratorComponent } from './community-moderator.component';

describe('CommunityModeratorComponent', () => {
  let component: CommunityModeratorComponent;
  let fixture: ComponentFixture<CommunityModeratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityModeratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityModeratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
