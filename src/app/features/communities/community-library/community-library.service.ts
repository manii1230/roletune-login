import { environment } from './../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommunityLibraryService {

  constructor(private http: HttpClient) { }

  communityLibrary(communityId: string, communityContent: any) {
    if (communityContent.file) {
      const libraryFormData = new FormData();
      // tslint:disable-next-line: forin
      for (const key in communityContent) {
        if (communityContent.hasOwnProperty(key)) {
          libraryFormData.append( key, communityContent[key]);
        }
      }
      libraryFormData.append('communityId', communityId);
      // tslint:disable-next-line: max-line-length
      return this.http.post(`${environment.API_URL}community-service/user-library`, libraryFormData);
    } else {
      return this.http.post(`${environment.API_URL}community-service/user-library`, { communityId, ...communityContent});
    }
  }

  communityLibrarySearch(communityId: string, formData: any, group?: boolean) {
    if (formData.keyword) {
      // tslint:disable-next-line: max-line-length
      return group ? this.http.get(`${environment.API_URL}community-service/community-library/${communityId}?term=${formData.keyword}&category=${formData.category}&matchType=${formData.matchType}`) : this.http.get(`${environment.API_URL}community-service/library-search/${communityId}?term=${formData.keyword}&category=${formData.category}&matchType=${formData.matchType}`);
    } else {
      // tslint:disable-next-line: max-line-length
      return group ? this.http.get(`${environment.API_URL}community-service/community-library/${communityId}?category=${formData.category}&matchType=${formData.matchType}`) : this.http.get(`${environment.API_URL}community-service/library-search/${communityId}?category=${formData.category}&matchType=${formData.matchType}`);
    }
  }
}
