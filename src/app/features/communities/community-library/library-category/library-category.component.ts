import { CommunityLibraryService } from './../community-library.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-library-category',
  templateUrl: './library-category.component.html',
  styleUrls: ['./library-category.component.css']
})
export class LibraryCategoryComponent implements OnInit {
  communityId: string;
  libraryCategory: string;
  searchLibraryCategoryForm: FormGroup;
  showCategoryResults: boolean;
  categoryList: any[];

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private communityLibraryService: CommunityLibraryService, private router: Router) { }

  ngOnInit() {
    this.libraryCategory = '';
    this.showCategoryResults = false;
    this.searchLibraryCategoryForm = this.formBuilder.group({
      keyword: ['', Validators.required],
      matchType: ['', Validators.required]
    });

    this.communityId = this.router.url.split('/')[3];

    this.route.paramMap.subscribe((route: any) => {
      this.libraryCategory = (route && route.params && route.params.category)? route.params.category : 'books';
      this.categoryList = [];
      this.showCategoryResults = false;
      this.searchLibraryCategoryForm.reset();
    });
  }

  searchLibraryCategory() {
    const payload: any = {};
    payload.category = this.libraryCategory;
    payload.matchType = this.searchLibraryCategoryForm.value.matchType;
    if (this.searchLibraryCategoryForm.value.keyword) {
      payload.keyword = this.searchLibraryCategoryForm.value.keyword;
    }
    this.communityLibraryService.communityLibrarySearch(this.communityId, payload).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.categoryList = res.result;
          this.showCategoryResults = true;
        }
      },
      (err) => {

      }
    );
  }

  downloadFile(idx: number) {
    console.log(this.categoryList[idx]);
  }
}
