import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibraryCategoryRoutingModule } from './library-category-routing.module';
import { LibraryCategoryComponent } from './library-category.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [LibraryCategoryComponent],
  imports: [
    CommonModule,
    LibraryCategoryRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class LibraryCategoryModule { }
