import { CommunityLibraryService } from './community-library.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunityLibraryRoutingModule } from './community-library-routing.module';
import { CommunityLibraryComponent } from './community-library.component';

@NgModule({
  declarations: [CommunityLibraryComponent ],
  imports: [
    CommonModule,
    CommunityLibraryRoutingModule
  ],
  providers: [
    CommunityLibraryService
  ]
})
export class CommunityLibraryModule { }
