import { TestBed } from '@angular/core/testing';

import { CommunityLibraryService } from './community-library.service';

describe('CommunityLibraryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CommunityLibraryService = TestBed.get(CommunityLibraryService);
    expect(service).toBeTruthy();
  });
});
