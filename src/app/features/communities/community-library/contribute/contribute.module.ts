import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContributeRoutingModule } from './contribute-routing.module';
import { ContributeComponent } from './contribute.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ContributeComponent],
  imports: [
    CommonModule,
    ContributeRoutingModule,
    ReactiveFormsModule
  ]
})
export class ContributeModule { }
