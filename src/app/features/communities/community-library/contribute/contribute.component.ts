import { CommunityLibraryService } from './../community-library.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contribute',
  templateUrl: './contribute.component.html',
  styleUrls: ['./contribute.component.css']
})
export class ContributeComponent implements OnInit {
  communityId: string;
  contributeLibraryForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private communityLibraryService: CommunityLibraryService, private router: Router) { }

  ngOnInit() {
    this.contributeLibraryForm = this.formBuilder.group({
      category: ['', Validators.required],
      title: ['', Validators.required],
      author: ['', Validators.required],
      description: ['', Validators.required],
      // tslint:disable-next-line: no-construct
      link: [new String('')],
      // tslint:disable-next-line: no-construct
      filename: [new String('')],
      file: [null],
      agreement: [false, Validators.required]
    });

    this.communityId = this.router.url.split('/')[3];
  }

  uploadFile(event: any) {
    if (event.target && event.target.files[0]) {
      this.contributeLibraryForm.patchValue({
        // tslint:disable-next-line: no-construct
        filename: new String(event.target.files[0].name),
        file: event.target.files[0],
        // tslint:disable-next-line: no-construct
        link: new String('')
      });
    }
  }

  resetFile() {
    this.contributeLibraryForm.patchValue({
      // tslint:disable-next-line: no-construct
      filename: new String(''),
      file: null
    });
  }

  contributeToLibrary() {
    const payload: any = {};
    payload.category = this.contributeLibraryForm.value.category;
    payload.title = this.contributeLibraryForm.value.title;
    payload.author = this.contributeLibraryForm.value.author;
    payload.description = this.contributeLibraryForm.value.description;
    if (this.contributeLibraryForm.value.file) {
      payload.file = this.contributeLibraryForm.value.file;
      payload.filename = this.contributeLibraryForm.value.filename.valueOf();
    } else {
      payload.link = this.contributeLibraryForm.value.link;
    }

    this.communityLibraryService.communityLibrary(this.communityId, payload).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.contributeLibraryForm.reset();
          this.resetFile();
        }
      },
      (err) => {

      }
    );
  }
}
