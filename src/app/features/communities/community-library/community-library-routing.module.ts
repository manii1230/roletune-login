import { CommunityLibraryComponent } from './community-library.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: CommunityLibraryComponent,
    children: [
      {
        path: 'contribute',
        loadChildren: './contribute/contribute.module#ContributeModule'
      },
      {
        path: 'search',
        loadChildren: './search/search.module#SearchModule'
      },
      {
        path: 'category',
        loadChildren: './library-category/library-category.module#LibraryCategoryModule'
      },
      {
        path: '',
        redirectTo: 'contribute',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommunityLibraryRoutingModule { }
