import { CommunityLibraryService } from './../community-library.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  showSearchResults: boolean;
  displaySearchItem: boolean;
  searchForm: FormGroup;
  communityId: string;
  categories: any[];
  libraryCategories: string[];
  searchItem: any;

  constructor(private formBuilder: FormBuilder, private router: Router, private communityLibraryService: CommunityLibraryService) { }

  ngOnInit() {
    this.showSearchResults = false;
    this.searchItem = null;
    this.displaySearchItem = false;
    this.categories = [];
    this.searchForm = this.formBuilder.group({
      keyword: ['', Validators.required],
      matchType: ['', Validators.required],
      category: ['', Validators.required]
    });
    this.communityId = this.router.url.split('/')[3];
    this.categories = [
      {
        id: 'audio-books',
        items: []
      },
      {
        id: 'books',
        items: []
      },
      {
        id: 'blogs',
        items: []
      },
      {
        id: 'videos',
        items: []
      },
      {
        id: 'others',
        items: []
      }
    ];
  }

  search() {
    this.communityLibraryService.communityLibrarySearch(this.communityId, this.searchForm.value, true).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.showSearchResults = true;
          res.result.forEach(category => {
            const idx = this.categories.findIndex(item => item.id === category._id);
            if (idx >= 0) {
              this.categories[idx].items = category.items;
            }
          });
        }
      },
      (err) => {

      }
    );
  }

  generateId(elementId: string, index: number, isHash: boolean): string {
    return isHash ? `#${elementId}-${index}` : `${elementId}-${index}`;
  }

  resetCategoryItems(): void {
    this.categories.forEach(category => category.items = []);
  }

  showSearchPopup(item: any) {
    this.displaySearchItem = true;
    this.searchItem = item;
  }
}
