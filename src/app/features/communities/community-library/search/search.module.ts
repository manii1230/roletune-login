import { SharedModule } from './../../../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { SearchPopupComponent } from './search-popup/search-popup.component';

@NgModule({
  declarations: [SearchComponent, SearchPopupComponent],
  imports: [
    CommonModule,
    SearchRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class SearchModule { }
