import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-popup',
  templateUrl: './search-popup.component.html',
  styleUrls: ['./search-popup.component.css']
})
export class SearchPopupComponent implements OnInit {
  @Input() searchItem: any;
  @Output() hidePopup = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  togglePopup() {
    this.hidePopup.emit(false);
  }
}
