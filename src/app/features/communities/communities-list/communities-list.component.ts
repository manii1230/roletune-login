import { forkJoin } from 'rxjs';
import { MessageTypes } from './../../../core/models/message-types';
import { LoadMore } from './../../../core/base/load-more.base';
import { HttpErrorResponse } from '@angular/common/http';
import { CommunitiesService } from './../communities.service';
import { AppService } from './../../../app.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-communities-list',
  templateUrl: './communities-list.component.html',
  styleUrls: ['./communities-list.component.css']
})
export class CommunitiesListComponent extends LoadMore implements OnInit {
  showResults: boolean;
  keySearchForm: FormGroup;
  communities: any[];
  displayedInterests: any[];
  allCommunities: any[];
  message: any;
  searchFormType: string;
  levels: any[];
  selectedLevel: string;
  totalCount: number;
  showProposeForm: boolean;
  interestSearchLevels: any;
  existingCommunities: any[];

  selectSearch: any;
  proposeInterest: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private appService: AppService,
    private communitiesService: CommunitiesService
  ) {
    super();
  }

  ngOnInit() {
    this.showResults = false;
    this.message = null;
    this.searchFormType = null;
    this.selectedLevel = null;
    this.totalCount = null;
    this.selectedLevel = null;
    this.interestSearchLevels = null;
    this.communities = [];
    this.existingCommunities = [];

    this.selectSearch = null;

    this.keySearchForm = this.formBuilder.group({
      keyword: ['', Validators.required],
      matchType: ['', Validators.required]
    });

    this.showProposeForm = false;

    this.appService.communityListObservable.subscribe(list => {
      this.allCommunities = (list && list.length) ? list : [];
      this.communities = (list && list.length) ? list.filter(item => item.status === 'ACTIVE') : [];
    });

    this.communitiesService.getInterestLevels().subscribe(
      (res: any) => {
        if (res.success) {
          this.selectSearch = res.result.map(level => {
            return { level, selected: false };
          });
          this.formatLevels(res.result, 4);
          const [ firstLevel ] = res.result;
          this.searchInterestByLevel(firstLevel);
        } else {
          this.selectSearch = [];
          this.message = {
            status: res.success,
            message: res.message.errorMessage
          };
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );

    this.communitiesService.getAllExistingCommunityList().subscribe(
      (res: any) => {
        this.existingCommunities = res.success ? res.result : [];
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  formatLevels(levels: string[] | number[], num: number): void {
    const levelList = [];
    for (let i = 0, j = 0; i < levels.length; i++) {
      if (i >= num && i % num === 0) {
        j++;
      }
      levelList[j] = levelList[j] || [];
      levelList[j].push(levels[i]);
    }
    this.levels = levelList;
  }

  searchInterests(formType?: string, pageNumber?: number) {
    this.searchFormType = formType ? formType : this.searchFormType;
    this.pageNumber = pageNumber ? pageNumber : this.pageNumber;
    this.config.loading = true;
    const payload: any = ( formType === 'KEYWORD' ) ?
      { keyword: this.keySearchForm.value.keyword, matchType: this.keySearchForm.value.matchType } :
      { level: this.selectedLevel };
    payload.pageNumber = this.pageNumber;
    payload.pageSize = this.pageSize;
    this.communitiesService.searchInterest(payload).subscribe(
      (res: any) => {
        this.showResults = true;
        this.config.loading = false;
        this.displayedInterests = this.displayedInterests.concat(
          ( formType === 'KEYWORD' ) ? this.filterChildren(res.result, this.keySearchForm.value.matchType) : res.result
        );
        this.totalCount = res.totalCount;
        this.config.loadMore = this.displayedInterests.length < this.totalCount;
        if (!res.success) {
          this.message = {
            status: res.success,
            message: res.message.errorMessage
          };
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  searchInterestByLevel(level: number) {
    const payload = {
      level,
      all: 'true',
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };
    this.communitiesService.searchInterestByLevel(payload).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        const interest = this.selectSearch.find(interestItem => interestItem.level === level);
        if (interest) {
          interest.interests = res.result ? res.result : [];
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  filterChildren(interests: any[], matchType: string) {
    interests.forEach(interest => {
      interest.children = ( matchType === 'exact-match') ?
        interest.children.filter(item => item.name.toLowerCase() === this.keySearchForm.value.keyword.toLowerCase()) :
        interest.children.filter(item => item.name.toLowerCase().includes(this.keySearchForm.value.keyword.toLowerCase()));
    });
    return interests;
  }

  resetSearch(formType: string, level?: string) {
    this.pageNumber = 0;
    this.showResults = false;
    this.searchFormType = null;
    this.displayedInterests = [];
    this.config.loadMore = true;
    this.message = null;
    if (formType === 'SELECT_SEARCH') {
      this.selectedLevel = level;
      this.keySearchForm.reset();
    } else {
      this.selectedLevel = null;
    }
  }

  handleHideForm(flag) {
    this.showProposeForm = flag;
  }

  toggleProposeForm(idx) {
    this.showProposeForm = !this.showProposeForm;
    this.proposeInterest = idx;
  }

  // showCommunityDetails(communityName) {
  //   this.appService.updateCommunity(communityName);
  //   this.router.navigate([`/${communityName}/dashboard`]);
  // }

  viewCommunity(event: any) {
    const existingCommunity = this.existingCommunities.find(community => community.name === event.name);
    if (existingCommunity) {
      window.open(`#${this.router.url}/${existingCommunity._id}/dashboard`);
      return;
    }
    window.open(`#${this.router.url}/${event._id}/dashboard`);
  }

  addInterest(event: any) {
    if (this.communities.length >= 5) {
      this.message = {
        status: false,
        message: MessageTypes.interestLimitCrossed
      };
      this.appService.removeMessage(this, 'message');
      return;
    }
    const existingCommunity = this.allCommunities.find(
      (community: any) => community.community_docs.name.toLowerCase() === event.name.toLowerCase()
    );
    if (existingCommunity) {
      this.message = {
        status: false,
        message: MessageTypes.interestAlreadyJoined
      };
      return;
    }
    this.communitiesService.addInterest({ communityName: event.name }).subscribe(
      (res: any) => {
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          this.appService.removeMessage(this, 'message');
          setTimeout(() => {
            this.appService.updateCommunity(event.name);
            this.router.navigate([`/main/communities/${event._id}/dashboard`]);
          }, 3000);
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }
}
