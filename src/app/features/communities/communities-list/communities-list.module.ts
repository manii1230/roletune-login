import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CommunitiesListRoutingModule } from './communities-list-routing.module';
import { CommunitiesListComponent } from './communities-list.component';
import { ProposeInterestComponent } from './propose-interest/propose-interest.component';

@NgModule({
  declarations: [CommunitiesListComponent, ProposeInterestComponent],
  imports: [
    CommonModule,
    CommunitiesListRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CommunitiesListModule { }
