import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposeInterestComponent } from './propose-interest.component';

describe('ProposeInterestComponent', () => {
  let component: ProposeInterestComponent;
  let fixture: ComponentFixture<ProposeInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProposeInterestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposeInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
