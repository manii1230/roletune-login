import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-propose-interest',
  templateUrl: './propose-interest.component.html',
  styleUrls: ['./propose-interest.component.css']
})
export class ProposeInterestComponent implements OnInit {

  @Input() interest;
  @Output() hideForm = new EventEmitter();

  displaySuggestion: boolean;
  displayStatus: boolean;
  proposeInterestForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.displaySuggestion = false;
    this.displayStatus = false;
    this.proposeInterestForm = this.formBuilder.group({
      interest: ['', Validators.required],
      parent: ['', Validators.required]
    });
  }

  toggleForm(flag) {
    this.hideForm.emit(flag);
  }

  proposeInterest() {
    this.displayStatus = true;
    console.log(this.proposeInterestForm.value);
  }
}
