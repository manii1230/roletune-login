import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunityDashboardRoutingModule } from './community-dashboard-routing.module';
import { CommunityDashboardComponent } from './community-dashboard.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommunityUpdatesComponent } from './community-updates/community-updates.component';
import { CommunityTrainingsComponent } from './community-trainings/community-trainings.component';
import { CommunityUsersListComponent } from './community-users-list/community-users-list.component';
import { CommunityMentorsListComponent } from './community-mentors-list/community-mentors-list.component';

@NgModule({
  declarations: [CommunityDashboardComponent, CommunityUpdatesComponent, CommunityTrainingsComponent, CommunityUsersListComponent, CommunityMentorsListComponent],
  imports: [
    CommonModule,
    CommunityDashboardRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class CommunityDashboardModule { }
