import { AppService } from './../../../../app.service';
import { HttpErrorResponse } from '@angular/common/http';
import { CommunitiesService } from './../../communities.service';
import { LoadMore } from './../../../../core/base/load-more.base';
import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-community-updates',
  templateUrl: './community-updates.component.html',
  styleUrls: ['./community-updates.component.css']
})
export class CommunityUpdatesComponent extends LoadMore implements OnChanges {
  @Input() communityDetails: any;
  communityUpdates: any[];
  message: any;

  constructor(
    private appService: AppService,
    private communitiesService: CommunitiesService
  ) {
    super();
  }

  ngOnChanges() {
    this.communityUpdates = [];
    this.message = null;
    this.pageNumber = 1;
    if (this.communityDetails) {
      this.loadCommunityUpdates();
    }
  }

  loadCommunityUpdates() {
    this.config.loading = true;
    const payload = {
      communityId: this.communityDetails._id,
      pageNumber: this.pageNumber,
      pageSize: this.pageSize
    };
    this.communitiesService.getCommunityUpdates(payload).subscribe(
      (res: any) => {
        this.config.loading = false;
        this.config.loadMore = !(res.result && res.result.length < this.pageSize);
        this.communityUpdates = this.communityUpdates.concat(res.result ? res.result : []);
        if (!res.success) {
          this.message = {
            status: false,
            message: res.message.errorMessage
          };
        }
      },
      (err: HttpErrorResponse) => {
        this.config.loading = false;
        this.config.loadMore = false;
        this.communityUpdates = [];
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  getCommunityUpdatesMessage(message: string): string {
    return (message.split(' ').length > 25) ? `${message.split(' ').slice(0, 25).join(' ')}...` : message;
  }

  removeUpdate(idx: number) {
    const update = this.communityUpdates[idx];
    update.updateId = update._id;
    this.communitiesService.updateCommunityUpdates(this.communityDetails.communityId, update).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.communityUpdates.splice(idx, 1);
        }
        this.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };

        if (res.success) {
          this.appService.removeMessage(this, 'message');
        }
      },
      (err: HttpErrorResponse) => {
        this.message = {
          status: false,
          message: err.message
        };
      }
    );
  }

  openCommunityUpdate(update: any) {
    let link = `#/main/communities/${update.communityId}/`;
    switch (update.linkType) {
      case 'COMMUNITY_BLOG':
        link += `blogs/blog-details/${update.linkId}`;
        break;
      case 'COMMUNITY_THREAD':
        link += `threads/thread-details/${update.linkId}`;
        break;
      case 'COMMUNITY_TRAINING':
        break;
      case 'COMMUNITY_STAT':
        break;
      case 'COMMUNITY_LIBRARY':
        break;
    }
    window.open(link);
  }
}
