import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-community-trainings',
  templateUrl: './community-trainings.component.html',
  styleUrls: ['./community-trainings.component.css']
})
export class CommunityTrainingsComponent implements OnChanges {
  @Input() communityDetails: any;
  communityTrainings: any[];

  constructor() { }

  ngOnChanges() {
    this.communityTrainings = [];
  }

}
