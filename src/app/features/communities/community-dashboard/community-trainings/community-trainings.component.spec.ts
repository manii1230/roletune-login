import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityTrainingsComponent } from './community-trainings.component';

describe('CommunityTrainingsComponent', () => {
  let component: CommunityTrainingsComponent;
  let fixture: ComponentFixture<CommunityTrainingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityTrainingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityTrainingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
