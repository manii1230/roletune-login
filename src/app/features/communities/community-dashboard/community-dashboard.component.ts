import { LoadMore } from './../../../core/base/load-more.base';
import { HttpErrorResponse } from '@angular/common/http';
import { CommunitiesService } from './../communities.service';
import { AppService } from './../../../app.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-community-dashboard',
  templateUrl: './community-dashboard.component.html',
  styleUrls: ['./community-dashboard.component.css']
})
export class CommunityDashboardComponent implements OnInit {
  communityDetails: any;
  usersList: any[];
  friendsList: any[];
  selectedMentor: any;
  searchUserForm: FormGroup;
  showMentorshipForm: boolean;
  showSearchResults: boolean;
  communityTrainings: any[];

  message: any;

  constructor(
    private formBuilder: FormBuilder,
    private appService: AppService,
    private communitiesService: CommunitiesService
  ) {
  }

  ngOnInit() {
    this.showSearchResults = false;
    this.communityTrainings = [];
    this.usersList = [];

    this.communityDetails = null;
    this.message = null;

    this.searchUserForm  =  this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      mentor: ['ALL', Validators.required],
      level: ['ALL', Validators.required],
    });

    this.appService.friendsListObservable.subscribe(friendsList => {
      this.friendsList = friendsList ? friendsList : [];
      this.loadCommunityDetails();
    });
  }

  loadCommunityDetails() {
    this.communitiesService.activeCommunityObservable.subscribe(community => {
      if (community && Object.keys(community).length) {
        this.communityDetails = community;
      }
    });
  }

  searchCommunityUser() {
    this.showSearchResults = true;

    const payload: any = { ...this.searchUserForm.value };
    this.communitiesService.getCommunityUserList(this.communityDetails.communityId, payload).subscribe(
      (res: any) => {
        if (res.success && res.result && res.result.length) {
          this.usersList = res.result;
        }
      },
      (err) => {

      }
    );
  }

  handleHideForm(flag: boolean) {
    this.showMentorshipForm = flag;
  }

  toggleMentorshipForm(idx: number) {
    this.selectedMentor = this.usersList[idx];
    this.showMentorshipForm = !this.showMentorshipForm;
  }

  addFriend(idx: number) {
    this.appService.sendRequest(this.usersList[idx].userId).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          console.log(res);
        }
      },
      (err) => {

      }
    );
  }
}
