import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-community-mentors-list',
  templateUrl: './community-mentors-list.component.html',
  styleUrls: ['./community-mentors-list.component.css']
})
export class CommunityMentorsListComponent implements OnInit {
  @Input() usersList: any[];
  @Output() toggleMentorshipFormEmitter: EventEmitter<number> = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }

  toggleMentorshipForm(idx: number): void {
    this.toggleMentorshipFormEmitter.emit(idx);
  }

}
