import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityMentorsListComponent } from './community-mentors-list.component';

describe('CommunityMentorsListComponent', () => {
  let component: CommunityMentorsListComponent;
  let fixture: ComponentFixture<CommunityMentorsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityMentorsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityMentorsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
