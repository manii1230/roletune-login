import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-community-users-list',
  templateUrl: './community-users-list.component.html',
  styleUrls: ['./community-users-list.component.css']
})
export class CommunityUsersListComponent implements OnInit {
  @Input() usersList: any[];
  @Input() friendsList: any[];
  @Output() addFriendEmitter: EventEmitter<number> = new EventEmitter<number>();
  constructor() { }

  ngOnInit() {
  }

  isFriend(idx: number): boolean {
    // tslint:disable-next-line: max-line-length
    return this.friendsList.find(friend => (friend.fromUserId === this.usersList[idx].userId || friend.toUserId === this.usersList[idx].username)) ? true : false;
  }

  addFriend(idx: number): void {
    this.addFriendEmitter.emit(idx);
  }

}
