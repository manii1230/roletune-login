import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunityUsersListComponent } from './community-users-list.component';

describe('CommunityUsersListComponent', () => {
  let component: CommunityUsersListComponent;
  let fixture: ComponentFixture<CommunityUsersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityUsersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunityUsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
