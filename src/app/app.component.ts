import { forkJoin } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from './app.service';
import { Component, OnInit, HostListener, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {
  loading: boolean;
  @HostListener('window:scroll', ['$event'])
  onScroll() {
    this.checkScroll();
  }

  constructor(private router: Router, private appService: AppService, private cookieService: CookieService) {}

  ngOnInit() {
    this.loading = true;
    //const token = sessionStorage.getItem('token');
    document.documentElement.style.setProperty('--background-color', localStorage.getItem('theme'));
    document.documentElement.style.setProperty('--link-color', localStorage.getItem('theme'));
    const token = this.cookieService.get('token');
    if (token) {
      const userObservable = this.appService.getUserDetails();
      const communityListObservable = this.appService.getCommunityList();
      const friendListObservable =  this.appService.getFriendsList();
      const groupListObservable =  this.appService.getGroupList();

      forkJoin([userObservable, friendListObservable, communityListObservable, groupListObservable ]).subscribe(
        (res: any) => {
          if (res[0] && res[0].success) {
            this.appService.updateUser((res[0].result && res[0].result[0]) ? res[0].result[0] : {});
            // tslint:disable-next-line: max-line-length
            document.documentElement.style.setProperty('--background-color', (res[0].result && res[0].result[0] && res[0].result[0].homeSettings && res[0].result[0].homeSettings.theme) ? res[0].result[0].homeSettings.theme : '#008670');
            document.documentElement.style.setProperty('--link-color', (res[0].result && res[0].result[0] && res[0].result[0].homeSettings && res[0].result[0].homeSettings.theme) ? res[0].result[0].homeSettings.theme : '#008670');
          }

          if (res[1] && res[1].success) {
            this.appService.updateFriendListObservable(res[1].result ? res[1].result : []);
          }

          if (res[2] && res[2].success) {
            this.appService.updateCommunityListObservable(res[2].result ? res[2].result : []);
          }

          if (res[3] && res[3].success) {
            this.appService.updateGroupListObservable(res[3].result ? res[3].result : []);
          }
          setTimeout(() => {
            this.removeLoader();
          }, 3000);
        },
        (err) => {
          this.router.navigate(['auth/login']);
        }
      );
    } else {
      this.router.navigate(['auth/login']);
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.loading = false;
    }, 2000);
  }

  removeLoader() {
    document.body.classList.replace('loading', 'loaded');
  }

  checkScroll() {
    if (window.innerWidth <= 768) {
      return;
    }
    const topbarElement = (document.querySelector('#sticky-topbar') as HTMLDivElement);
    const mainSidebarElement = (document.querySelector('#main-menu-wrapper') as HTMLDivElement);
    const friendListElement = (document.querySelector('#friend-list-wrap') as HTMLDivElement);
    if (window.scrollY >= 70) {
      if (topbarElement) {
        topbarElement.classList.add('sticky');
      }
      if (mainSidebarElement) {
        mainSidebarElement.classList.add('sticky');
      }
      if (friendListElement) {
        friendListElement.classList.add('sticky');
      }
      return;
    }
    if (topbarElement) {
      topbarElement.classList.remove('sticky');
    }

    if (mainSidebarElement) {
      mainSidebarElement.classList.remove('sticky');
    }

    if (friendListElement) {
      friendListElement.classList.remove('sticky');
    }
  }
}
