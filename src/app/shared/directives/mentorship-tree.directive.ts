import {
  Directive,
  ElementRef,
  OnInit,
  Input,
  AfterContentInit,
  HostListener,
} from '@angular/core';
import { MentorshipTree } from 'src/app/core/interfaces';
import { Router } from '@angular/router';

@Directive({
  selector: '[appMentorshipTree]',
})
export class MentorshipTreeDirective implements AfterContentInit {
  @Input() appMentorshipTree: MentorshipTree;
  @Input() colors: any;
  margin: any;
  lineLength: number;
  midX: number;
  midY: number;
  strokeWidth: number;
  @HostListener('click', ['$event']) onClick($event) {
    if (
      $event.target &&
      $event.target.nodeName === 'text' &&
      $event.target.innerHTML
    ) {
      this.navigateToProfile($event.target.innerHTML);
    }
  }
  constructor(private element: ElementRef, private router: Router) {}

  ngAfterContentInit() {
    this.element.nativeElement.style.textAnchor = 'middle';
    this.midX = this.element.nativeElement.clientWidth / 2;
    this.midY = this.element.nativeElement.clientHeight / 2;

    this.lineLength = this.element.nativeElement.clientHeight / 5;
    this.strokeWidth = 3;
    this.margin = {
      top: this.lineLength / 5,
      right: this.lineLength / 5,
      bottom: this.lineLength / 5,
      left: this.lineLength / 5,
    };
    this.element.nativeElement.style.fontSize =
      (4 * this.margin.top) / 3 + 'px';
    this.element.nativeElement.style.padding = this.margin.top / 2 + 'px';

    this.appMentorshipTree.mentor =
      this.appMentorshipTree && this.appMentorshipTree.mentor
        ? this.appMentorshipTree.mentor
        : 'None';
    this.appMentorshipTree.mentees =
      this.appMentorshipTree &&
      this.appMentorshipTree.mentees &&
      this.appMentorshipTree.mentees.length
        ? this.appMentorshipTree.mentees
        : ['None', 'None'];

    let innerHTML = `<g class='wrap'>`; //`<g class='wrap' style="transform: translate(${(-1) * 2 * this.margin.top}px, ${2 * this.margin.top}px);">`;
    innerHTML += this.generateLines();
    innerHTML += this.generateNames();
    innerHTML += `</g>`;

    this.element.nativeElement.innerHTML = innerHTML;
  }

  generateLines(): string {
    let html = `<g>`;
    if (this.appMentorshipTree && this.appMentorshipTree.mentor) {
      // tslint:disable-next-line: max-line-length
      html += `<path fill='none' stroke='${this.colors.lines}' stroke-width='${
        this.strokeWidth
      }' d='M ${this.midX} ${this.margin.top} l 0 ${this.lineLength - 5}' />`;
    }
    if (
      this.appMentorshipTree &&
      this.appMentorshipTree.mentees &&
      this.appMentorshipTree.mentees.length
    ) {
      switch (this.appMentorshipTree.mentees.length) {
        case 1:
          // tslint:disable-next-line: max-line-length
          html += `<path fill='none' stroke='${
            this.colors.lines
          }' stroke-width='${this.strokeWidth}' d='M ${this.midX} ${
            2 * this.margin.top + this.lineLength
          } l 0 ${this.lineLength}' />`;
          // tslint:disable-next-line: max-line-length
          html += `<path fill='none' stroke='${
            this.colors.lines
          }' stroke-width='${this.strokeWidth}' d='M ${this.midX} ${
            2 * (this.margin.top + this.lineLength)
          } l -${this.midX / 3} ${this.midX / 3}' />`;
          break;
        case 2:
          // tslint:disable-next-line: max-line-length
          html += `<path fill='none' stroke='${
            this.colors.lines
          }' stroke-width='${this.strokeWidth}' d='M ${this.midX} ${
            2 * this.margin.top + this.lineLength
          } l 0 ${this.lineLength}' />`;
          // tslint:disable-next-line: max-line-length
          html += `<path fill='none' stroke='${
            this.colors.lines
          }' stroke-width='${this.strokeWidth}' d='M ${this.midX} ${
            2 * (this.margin.top + this.lineLength)
          } l -${this.midX / 3} ${this.midX / 3}' />`;
          // tslint:disable-next-line: max-line-length
          html += `<path fill='none' stroke='${
            this.colors.lines
          }' stroke-width='${this.strokeWidth}' d='M ${this.midX} ${
            2 * (this.margin.top + this.lineLength)
          } l ${this.midX / 3} ${this.midX / 3}' />`;
          break;
      }
    }
    return html + `</g>`;
  }

  generateNames(): string {
    let html = `<g class='names'>`;
    if (this.appMentorshipTree && this.appMentorshipTree.mentor) {
      // tslint:disable-next-line: max-line-length
      html += `<text x='${this.midX}' y='${this.margin.top}' stroke='${this.colors.names}' >${this.appMentorshipTree.mentor}</text>`;
    }
    if (this.appMentorshipTree && this.appMentorshipTree.username) {
      // tslint:disable-next-line: max-line-length
      html += `<text x='${this.midX}' y='${
        (3 * this.margin.top) / 2 + this.lineLength
      }' stroke='${this.colors.names}' >${
        this.appMentorshipTree.username
      }</text>`;
    }

    if (
      this.appMentorshipTree &&
      this.appMentorshipTree.mentees &&
      this.appMentorshipTree.mentees.length
    ) {
      switch (this.appMentorshipTree.mentees.length) {
        case 1:
          // tslint:disable-next-line: max-line-length
          html += `<text x='${(2 * this.midX) / 3}' y='${
            3 * (this.margin.top + this.lineLength) - 10
          }' stroke='${this.colors.names}' >${
            this.appMentorshipTree.mentees && this.appMentorshipTree.mentees[0]
              ? this.appMentorshipTree.mentees[0]
              : 'Mentee 1'
          }</text>`;
          break;
        case 2:
          // tslint:disable-next-line: max-line-length
          html += `<text x='${(2 * this.midX) / 3}' y='${
            3 * (this.margin.top + this.lineLength) - 10
          }' stroke='${this.colors.names}' >${
            this.appMentorshipTree.mentees && this.appMentorshipTree.mentees[0]
              ? this.appMentorshipTree.mentees[0]
              : 'Mentee 1'
          }</text>`;
          // tslint:disable-next-line: max-line-length
          html += `<text x='${(4 * this.midX) / 3}' y='${
            3 * (this.margin.top + this.lineLength) - 10
          }' stroke='${this.colors.names}' >${
            this.appMentorshipTree.mentees && this.appMentorshipTree.mentees[1]
              ? this.appMentorshipTree.mentees[1]
              : 'Mentee 2'
          }</text>`;
          break;
      }
    }
    return html + `</g>`;
  }

  navigateToProfile(username: string) {
    this.router.navigate([`main/profile/view/${username}`]);
  }
}
