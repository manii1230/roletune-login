import { Directive, Input, ElementRef, OnInit, HostListener, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[appExcerpt]'
})
export class ExcerptDirective implements AfterViewInit {
  @Input() appExcerpt: number;
  @Input() contentType: string;
  fulltext: string;

  constructor(private element: ElementRef) {}

  ngAfterViewInit() {
    this.trimText();
  }

  trimText() {
    switch (this.contentType) {
      case 'text':
        this.fulltext = this.element.nativeElement.innerText;
        // this.element.nativeElement.innerText = this.fulltext.split(' ').slice(0, this.appExcerpt).join(' ');
        this.element.nativeElement.innerText = this.element.nativeElement.innerText.split(' ').slice(0, this.appExcerpt).join(' ');
        if (this.fulltext  !== this.element.nativeElement.innerText) {
          this.addReadMoreLink();
        }
        break;
      case 'html':
        this.fulltext = this.element.nativeElement.innerHTML;
        this.element.nativeElement.innerHTML = this.element.nativeElement.innerHTML.split('</p>').slice(0, 1).join('</p>');
        if (this.fulltext  !== this.element.nativeElement.innerHTML) {
          this.addReadMoreLink();
        }
    }
  }

  addReadMoreLink() {
    const anchorTag = document.createElement('span');
    anchorTag.setAttribute('load-more', 'load-more');
    anchorTag.style.setProperty('cursor', 'pointer');
    anchorTag.style.setProperty('color', 'var(--background-color)');
    anchorTag.style.setProperty('clear', 'both');
    anchorTag.innerText = ' (more...)';
    this.element.nativeElement.appendChild(anchorTag);
  }

  @HostListener('click', ['$event.target']) onClick(target) {
    if (Object.values(target.attributes).map((item: any) => item.nodeValue === 'load-more').some(property => property === true)) {
      this.showAll();
    }
  }

  showAll() {
    switch (this.contentType) {
      case 'text':
        this.element.nativeElement.innerText = this.fulltext;
        break;
      case 'html':
        this.element.nativeElement.innerHTML = this.fulltext;
    }
  }
}
