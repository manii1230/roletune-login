import { Directive, ElementRef, OnInit, HostListener } from '@angular/core';

@Directive({
  selector: '[appDisableRightClick]'
})
export class DisableRightClickDirective implements OnInit {
  @HostListener('document:contextmenu', ['$event']) disableRightClick($event) {
    $event.preventDefault();
  }

  constructor(private element: ElementRef) { }

  ngOnInit() {
  }
}
