import { Directive, ElementRef, OnInit, Output, HostListener, Input, EventEmitter, OnChanges } from '@angular/core';

@Directive({
  selector: '[appLoadMore]'
})
export class LoadMoreDirective implements OnInit {
  @Output() loadMoreEvent = new EventEmitter();
  @Input() appLoadMore: any;
  @Input() config: any;
  listen: boolean;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.listen = true;
  }

  @HostListener('scroll', ['$event.target']) onScroll(target) {
    // tslint:disable-next-line: max-line-length
    const percentage = Math.ceil((this.element.nativeElement.scrollTop / this.element.nativeElement.clientHeight) * 100);
    switch (this.appLoadMore.condition) {
      case 'GREATER':
        if (percentage > this.appLoadMore.value) {
          this.loadMoreEvent.emit(`percentage`);
        }
        break;
      case 'LESSER':
        if (percentage < this.appLoadMore.value) {
          this.loadMoreEvent.emit(`percentage`);
        }
        break;
    }
  }
}
