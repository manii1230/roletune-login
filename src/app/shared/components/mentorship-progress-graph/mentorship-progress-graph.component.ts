import { Component, OnInit, Input, ElementRef, AfterViewInit, AfterContentInit } from '@angular/core';

@Component({
  selector: 'app-mentorship-progress-graph',
  templateUrl: './mentorship-progress-graph.component.html',
  styleUrls: ['./mentorship-progress-graph.component.css']
})
export class MentorshipProgressGraphComponent implements OnInit, AfterContentInit {
  @Input() appMentorshipProgressGraph: any;
  lines: any[];
  circles: any[];
  margin: any;

  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    this.lines = [];
    this.circles = [];

    this.margin = {
      topBottom: 100, rightLeft: 0
    };
  }

  ngAfterContentInit() {
    this.appMentorshipProgressGraph.startDate = new Date(this.appMentorshipProgressGraph.startDate);
    // tslint:disable-next-line: max-line-length
    this.appMentorshipProgressGraph.endDate = new Date(new Date(this.appMentorshipProgressGraph.startDate).setDate(new Date(this.appMentorshipProgressGraph.startDate).getDate() + (parseInt(this.appMentorshipProgressGraph.duration, 10) * 7)));
    this.organizeMilestones();
    this.generateLines();
    this.generateCircles();
  }

  organizeMilestones() {
    this.appMentorshipProgressGraph.mileStones = this.appMentorshipProgressGraph.mileStones.map((milestone, idx) => {
      // tslint:disable-next-line: max-line-length
      milestone.duration = 10 * ((new Date(milestone.date).getTime() - (this.appMentorshipProgressGraph.startDate as Date).getTime()) / (1000 * 3600 * 24));
      // milestone.color = (milestone.status === 'PENDING') ? 'YELLOW' : 'GREEN';
      milestone.color = (milestone.status === 'PENDING') ? 'yellow' : 'green';
      return milestone;
    });
  }

  generateLines() {
    this.appMentorshipProgressGraph.mileStones.forEach((milestone, idx) => {
      if (idx === 0) {
        milestone.path = `M ${this.margin.rightLeft} ${this.margin.topBottom} L ${milestone.duration} ${this.margin.topBottom}`;
      } else {
        const startPoint = this.appMentorshipProgressGraph.mileStones[idx - 1].duration + this.margin.rightLeft;
        milestone.path = `M ${startPoint} ${this.margin.topBottom} L ${milestone.duration} ${this.margin.topBottom}`;
      }
      this.lines.push(milestone);
    });
  }

  generateCircles() {
    this.circles.push({
      cx: 5,
      cy: this.margin.topBottom,
      r: 5,
      fill: '#000000'
    });

    this.appMentorshipProgressGraph.mileStones.forEach((milestone, idx) => {
      // if (idx === 0) {
      //   milestone.path = `M ${this.margin.rightLeft} ${this.margin.topBottom} L ${milestone.duration} ${this.margin.topBottom}`;
      // } else {
      //   const startPoint = this.appMentorshipProgressGraph.mileStones[idx - 1].duration + this.margin.rightLeft;
      //   milestone.path = `M ${startPoint} ${this.margin.topBottom} L ${milestone.duration} ${this.margin.topBottom}`;
      // }
      this.circles.push({
        cx: (milestone.duration),
        cy: this.margin.topBottom,
        r: 5,
        fill: '#000000'
      });
    });

    this.circles.push({
      // tslint:disable-next-line: max-line-length
      cx: 10 * ((new Date(this.appMentorshipProgressGraph.endDate).getTime() - (this.appMentorshipProgressGraph.startDate as Date).getTime()) / (1000 * 3600 * 24)),
      cy: this.margin.topBottom,
      r: 5,
      fill: '#000000'
    });
  }

  displaySegmentDetails(line: any) {
    console.log(line);
  }
}
