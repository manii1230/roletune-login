import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorshipProgressGraphComponent } from './mentorship-progress-graph.component';

describe('MentorshipProgressGraphComponent', () => {
  let component: MentorshipProgressGraphComponent;
  let fixture: ComponentFixture<MentorshipProgressGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorshipProgressGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorshipProgressGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
