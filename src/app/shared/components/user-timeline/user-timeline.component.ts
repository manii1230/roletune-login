import { Component, OnInit, NgZone, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { HomeService } from 'src/app/features/home/home.service';
import { FrontService } from 'src/app/features/front.service';
import { AppService } from 'src/app/app.service';
import { takeUntil } from 'rxjs/operators';
import { MessageTypes } from 'src/app/core/models/message-types';
import { HttpErrorResponse } from '@angular/common/http';
import { LoadMore } from 'src/app/core/base/load-more.base';

@Component({
  selector: 'app-user-timeline',
  templateUrl: './user-timeline.component.html',
  styleUrls: ['./user-timeline.component.css']
})
export class UserTimelineComponent extends LoadMore implements OnInit {
  @Input() user: any;
  @Input() heading: string;
  blogs: any[];
  username: string;
  blogType: string;
  blogForm: FormGroup;
  commentForm: FormGroup;
  replyForm: FormGroup;
  unsubscribe = new Subject();
  mediaRecorder: any;
  chunks: any;
  liveStreamFlag: boolean;
  showShareModal: boolean;
  shareBlogForm: FormGroup;
  activeBlog: any;
  groupList: any[];
  timelineAction: any;
  updateAll: boolean;

  // tslint:disable-next-line: max-line-length
  constructor(
    private route: ActivatedRoute,
    private homeService: HomeService,
    private appService: AppService,
    private formBuilder: FormBuilder,
    private zone: NgZone
  ) {
    super();
  }

  ngOnInit() {
    window['that'] = this;
    this.timelineAction = null;
    this.showShareModal = false;
    this.liveStreamFlag = false;
    this.pageNumber = 0;
    this.blogType = '';
    this.blogs = [];
    this.username = null;

    this.blogForm = this.formBuilder.group({
      content: ['', [Validators.required, Validators.minLength(3)]],
      file: [null],
      fileType: [''],
      fileName: ['']
    });

    this.commentForm = this.formBuilder.group({
      comment: ['', Validators.required],
      blog: ['']
    });

    this.replyForm = this.formBuilder.group({
      blogId: [''],
      comment: ['', [Validators.required, Validators.minLength(1)]],
      commentId: [''],
      replier: [null]
    });

    this.shareBlogForm = this.formBuilder.group({
      content: ['', [Validators.required, Validators.minLength(3)]],
      referenceBlog: ['', Validators.required],
      referenceContent: ['', Validators.required],
      referenceFile: [''],
      referenceAuthor: [null, Validators.required],
      sharedGroup: [''],
      sharedTo: ['']
    });

    this.activeBlog = null;
    this.groupList = [];

    this.route.paramMap
      .pipe(takeUntil(this.unsubscribe))
      .subscribe((param: any) => {
        this.blogType = param.get('blogType')
          ? param.get('blogType')
          : null;
        this.username = param.get('username');
        this.pageNumber = 0;
        this.blogs = [];
        this.loadTimeline();
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  loadTimeline() {
    this.config.loading = true;
    this.homeService
      .getUserTimeline(
        this.username ? this.username : '',
        ++this.pageNumber,
        this.blogType
      )
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(
        (res: any) => {
          if (res.success && res.result) {
            this.config.loadMore = res.result.length ? true : false;
            this.config.loading = false;
            this.blogs = this.blogs.concat(
              res.result.map((blog: any) => {
                // tslint:disable-next-line: max-line-length
                blog.comments =
                  blog.comments &&
                  blog.comments.length === 1 &&
                  JSON.stringify(blog.comments[0]) === '{}'
                    ? []
                    : blog.comments;
                return blog;
              })
            );
          }
        },
        err => {}
      );
  }

  createComment(blog: any) {
    this.commentForm.patchValue({ blog: blog._id });
    const comment = this.commentForm.value;
    comment.author = this.user;
    this.homeService
      .createTimelineComment(comment)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(
        (res: any) => {
          if (res.success && res.result && res.result.comment) {
            const idx = this.blogs.findIndex(
              blogItem => blogItem._id === blog._id
            );
            if (idx >= 0) {
              res.result.comment.commenter = this.user;
              if (this.blogs[idx] && this.blogs[idx].comments) {
                this.blogs[idx].comments.push(res.result.comment);
              } else {
                this.blogs[idx].comments = [res.result.comment];
              }
              this.commentForm.reset();
            }
          }
          blog.commentAction = {
            status: res.success,
            message: MessageTypes.commentSuccess
          };
          this.appService.removeMessage(blog, 'commentAction');
        },
        (err: HttpErrorResponse) => {
          blog.commentAction = {
            status: false,
            message: MessageTypes.commentFailed
          };
          this.appService.removeMessage(blog, 'commentAction');
        }
      );
  }

  createBlog(blogFormData?: any) {
    const payload = blogFormData ? blogFormData : this.blogForm.value;
    payload.for = this.user.username;
    this.homeService
      .createTimeline(payload)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(
        (res: any) => {
          if (res.success && res.result.blog && res.result.blog.authorDetails) {
            res.result.blog.authorDetails = this.addAuthor();
            this.blogs.unshift(res.result.blog);
            if (blogFormData) {
              this.activeBlog = null;
              this.showShareModal = false;
              this.shareBlogForm.reset();
            } else {
              this.blogForm.reset();
            }
          }
          this.timelineAction = {
            status: res.success,
            message: MessageTypes.blogSuccess
          };
          this.appService.removeMessage(this, 'timelineAction');
        },
        (err: HttpErrorResponse) => {
          this.timelineAction = {
            status: false,
            message: MessageTypes.blogFailed
          };
          this.appService.removeMessage(this, 'timelineAction');
        }
      );
  }

  addAuthor() {
    return {
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      profileImage: this.user.profileImage,
      username: this.user.username
    };
  }

  updateBlog(idx: number, action: string) {
    const existingReaction = this.blogs[idx].userActions.find(
      (reaction: any) => (reaction.action === action && reaction.username === this.user.username)
    );
    if (existingReaction) {
      return;
    }
    this.homeService
      .updateTimelineBlog(this.blogs[idx]._id, action)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(
        (res: any) => {
          if (res.success && res.result.blog) {
            this.blogs[idx] = Object.assign(this.blogs[idx], res.result.blog);
            this.blogForm.reset();
          }
          this.blogs[idx].action = {
            status: res.success,
            message: MessageTypes.reactionSuccess
          };
          this.appService.removeMessage(this.blogs[idx], 'action');
        },
        (err: HttpErrorResponse) => {
          this.blogs[idx].action = {
            status: false,
            message: MessageTypes.reactionFailed
          };
          this.appService.removeMessage(this.blogs[idx], 'action');
        }
      );
  }

  reactionExists(blog: any, action: string) {
    const currentUser = this.appService.getLoggedInUserDetails();
    return blog.userActions.find(reaction => reaction.action === action && reaction.username === currentUser.username);
  }

  uploadFile(event: any) {
    if (event.target && event.target.files[0]) {
      this.blogForm.patchValue({
        fileName: event.target.files[0].name,
        file: event.target.files[0],
        fileType: event.target.files[0].type
      });
    }
  }

  getReactionCount(blog: any, action?: string): number {
    if (action) {
      return blog.userActions
        ? blog.userActions.filter(
            (userAction: any) => userAction.action === action
          ).length
        : '';
    } else {
      return blog.userActions ? blog.userActions.length : null;
    }
  }

  createReply(idx: number, commentIdx: number) {
    this.replyForm.patchValue({
      blogId: this.blogs[idx]._id,
      commentId: this.blogs[idx].comments[commentIdx]._id,
      replier: {
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        username: this.user.username
      }
    });
    this.homeService.communityBlogReply(this.replyForm.value).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          if (
            this.blogs[idx].comments[commentIdx].replies &&
            this.blogs[idx].comments[commentIdx].replies.length
          ) {
            this.blogs[idx].comments[commentIdx].replies.push(
              res.result.commentReply
            );
          } else {
            this.blogs[idx].comments[commentIdx].replies = [
              res.result.commentReply
            ];
          }
          this.blogs[idx].comments[commentIdx].showReplyForm = false;
          this.replyForm.reset();
        }
        this.blogs[idx].comments[commentIdx].replyAction = {
          status: res.success,
          message: MessageTypes.replySuccess
        };
        this.appService.removeMessage(this.blogs[idx].comments[commentIdx], 'replyAction');
      },
      (err: HttpErrorResponse) => {
        this.blogs[idx].comments[commentIdx].replyAction = {
          status: false,
          message: MessageTypes.replyFailed
        };
        this.appService.removeMessage(this.blogs[idx].comments[commentIdx], 'replyAction');
      }
    );
  }

  blogAction(blog: any, action: string, group?: string) {
    const payload: any = {};
    payload.blogId = blog._id;
    payload.actionType = action;
    payload.sharedGroup = group;
    this.homeService.saveTimelineAction(payload).pipe(takeUntil(this.unsubscribe)).subscribe(
      (res: any) => {
        if (res && res.result) {
          const blogIdx = this.blogs.findIndex(blogItem => blogItem._id === blog._id);
          if (action === 'hide' && blog.authorDetails.username !== this.user.username) {
            this.blogs.splice(blogIdx, 1);
          } else if (action === 'hide' && blog.authorDetails.username === this.user.username) {
            this.blogs[blogIdx].displayFlag = false;
          } else if (action === 'unhide' && blog.authorDetails.username === this.user.username) {
            this.blogs[blogIdx].displayFlag = true;
          }
        }
        blog.action = {
          status: res.success,
          message: MessageTypes.actionSuccess
        };
        this.appService.removeMessage(blog, 'action');
      },
      (err: HttpErrorResponse) => {
        blog.action = {
          status: false,
          message: MessageTypes.actionFailed
        };
        this.appService.removeMessage(blog, 'action');
      }
    );
  }

  captureLiveStream() {
    // if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    //   this.liveStreamFlag = true;
    //   navigator.mediaDevices
    //     .getUserMedia({ video: true, audio: true })
    //     .then(stream => {
    //       const videoTag: HTMLVideoElement = document.querySelector(
    //         '#video-stream'
    //       );
    //       videoTag.srcObject = stream;
    //       this.mediaRecorder = new MediaRecorder(stream);
    //       this.chunks = [];
    //       this.mediaRecorder.ondataavailable = (event: any) => {
    //         window['that'].chunks.push(event.data);
    //       };
    //       this.mediaRecorder.onstop = (event: any) => {
    //         window['that'].saveVideo(event);
    //       };
    //     })
    //     .catch((err: any) => {
    //       console.log('Error: ', err);
    //     });
    // } else {
    // }
  }

  startRecording() {
    this.mediaRecorder.start();
    (<HTMLVideoElement>document.querySelector('#video-stream')).play();
  }

  stopRecording() {
    this.mediaRecorder.stop();
    (<HTMLVideoElement>document.querySelector('#video-stream')).pause();
  }

  saveVideo() {
    const videoTag: HTMLVideoElement = document.querySelector('#live-stream');
    const blob = new Blob(this.chunks, {
      type: this.mediaRecorder.stream.mimeType
    });

    this.chunks = [];
    videoTag.src = window.URL.createObjectURL(blob);
    videoTag.controls = true;
    this.zone.run(() => {
      this.blogForm.patchValue({
        fileName: this.user
          ? `${this.user.username}-live-stream-${new Date().getTime()}`
          : `username-live-stream-${new Date().getTime()}`,
        fileType: this.mediaRecorder.stream.mimeType,
        file: blob
      });

      this.liveStreamFlag = false;
    });
  }

  openShareBlogForm(blog: any, shareIn: string) {
    this.activeBlog = blog;
    this.showShareModal = true;
    switch (shareIn) {
      case 'timeline':
        this.activeBlog.shareType = 'TIMELINE';
        this.shareBlogForm.patchValue({
          referenceBlog: blog.referenceBlog ? blog.referenceBlog : blog._id,
          referenceContent: blog.referenceContent ? blog.referenceContent : blog.content,
          referenceFile: blog.referenceFile ? blog.referenceFile : blog.filePath,
          referenceAuthor: JSON.stringify(blog.authorDetails),
          sharedGroup: null
        });
        break;
      case 'group':
        this.activeBlog.shareType = 'GROUP';
        const [ group ] = this.groupList;
        this.shareBlogForm.patchValue({
          referenceBlog: blog.referenceBlog ? blog.referenceBlog : blog._id,
          referenceContent: blog.referenceContent ? blog.referenceContent : blog.content,
          referenceFile: blog.referenceFile ? blog.referenceFile : blog.filePath,
          referenceAuthor: JSON.stringify(blog.authorDetails),
          sharedGroup: group.groupName
        });
        break;
      case 'message':
        this.activeBlog.shareType = 'MESSAGE';
        this.shareBlogForm.patchValue({
          referenceBlog: blog.referenceBlog ? blog.referenceBlog : blog._id,
          referenceContent: blog.referenceContent ? blog.referenceContent : blog.content,
          referenceFile: blog.referenceFile ? blog.referenceFile : blog.filePath,
          referenceAuthor: JSON.stringify(blog.authorDetails),
          sharedGroup: null,
          sharedTo: ''
        });
    }
  }

  updateShareGroup(groupName: string): void {
    this.shareBlogForm.patchValue({
      sharedGroup: groupName
    });
  }

  shareBlog() {
    switch (this.activeBlog.shareType) {
      case 'TIMELINE':
      case 'GROUP':
        this.createBlog(this.shareBlogForm.value);
        break;
      case 'MESSAGE':
        const payload = {
          receiverIds: [this.shareBlogForm.get('sharedTo')],
          cc: [],
          senderCategory: 'sent',
          receiverCategory: 'inbox',
          subject: this.shareBlogForm.get('content'),
          message: this.shareBlogForm.get('referenceContent'),
          fileName: this.activeBlog.filePath,
          fileType: ''
        };
        this.homeService.shareBlogMessage(payload).pipe(takeUntil(this.unsubscribe)).subscribe(
          (res: any) => {
            if (res.result) {
            }
          },
          (err: HttpErrorResponse) => {
            console.log(`Error: `, err);
          }
        );
    }
  }

  showSharedGroupMessage(blog: any): boolean {
    if (!blog.sharedGroup) {
      return false;
    }
    const groupName = this.groupList.find(groupItem => groupItem.groupName === blog.sharedGroup);
    if (!groupName) {
      return false;
    }
    return (blog && blog.authorDetails && (blog.authorDetails.username === this.user.username));
  }
}
