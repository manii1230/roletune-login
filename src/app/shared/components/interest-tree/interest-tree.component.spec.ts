import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InterestTreeComponent } from './interest-tree.component';

describe('InterestTreeComponent', () => {
  let component: InterestTreeComponent;
  let fixture: ComponentFixture<InterestTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InterestTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InterestTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
