import { Component, Input, AfterViewInit, Output, EventEmitter } from '@angular/core';
import * as d3 from 'd3';

export interface InterestTree {
  _id: string;
  name: string;
  level: number;
  description: string;
  children: InterestTree[]
}

@Component({
  selector: 'app-interest-tree',
  templateUrl: './interest-tree.component.html',
  styleUrls: ['./interest-tree.component.css']
})
export class InterestTreeComponent implements AfterViewInit {
  @Input() interest: InterestTree;
  @Input() index: number;
  @Output() interestSelected = new EventEmitter();
  constructor() { }

  ngAfterViewInit() {
    this.generateGraph();
  }

  generateGraph() {
    const treeElement: HTMLDivElement = document.querySelector(`#interest-${this.interest._id}`);
    const margin = {
      top: 20,
      right: 120,
      bottom: 20,
      left: 120
    };
    const width = treeElement.clientWidth - margin.right - margin.left;
    const height = (
      treeElement.clientHeight ? treeElement.clientHeight : (this.interest.children.length ? this.interest.children.length * 75 : 75 )
    ) - margin.top - margin.bottom;

    const svgElement = d3.select(`#interest-${this.interest._id}`).append('svg');

    svgElement
      .attr('width', width + margin.right + margin.left)
      .attr('height', height + margin.top + margin.bottom);

    const interestGElement = svgElement.append('g');
    interestGElement
      .append('text')
      .text(`${this.index}. ${this.interest.name}`)
      .attr('id', () => `${this.interest.name.split(' ').join('-')}`)
      .style('stroke', '#505050')
      .style('cursor', 'pointer')
      .style('font-size', '20px')
      .style('text-decoration', 'underline')
      .attr('transform', () => {
        return `translate(0, ${margin.top})`;
      })
      .on('click', () => {
        this.clickedNode(this.interest);
      });

    if (this.interest.children && this.interest.children.length) {
      interestGElement
      .append('path')
      .style('stroke', '#505050')
      .attr(
        'd',
        `M ${margin.left / 2} ${margin.top + (height / this.interest.children.length) * ( 0.5)}
        L ${( width / 6) + margin.left} ${margin.top + (height / this.interest.children.length) * ( 0.5)}`
      );

      interestGElement
        .append('circle')
        .style('stroke', '#505050')
        .attr('cx', `${margin.left / 2}`)
        .attr('cy', `${margin.top + (height / this.interest.children.length) * ( 0.5)}`)
        .attr('r', '5');

      const gTextElement = svgElement.append('g');
      const gLineElement = svgElement.append('g');

      gTextElement.attr('transform', `translate(${( width / 4) + margin.left},${margin.top})`);

      this.interest.children.forEach((child: InterestTree, index: number) => {
        gTextElement.append('text')
          .text(child.name)
          .attr('id', `child-${child.name.split(' ').join('-')}`)
          .attr('class', 'node-text')
          .style('stroke', '#505050')
          .style('cursor', 'pointer')
          .style('text-decoration', 'underline')
          .attr('transform', () => {
            return `translate(0, ${(height / this.interest.children.length) * ( 0.5 + index )})`;
          }).on('click', () => {
            this.clickedNode(child);
          });
          // .on('mouseenter', () => {
          //   d3.select(`#child-${child.name.split(' ').join('-')}`).style('text-decoration', 'underline')
          // })
          // .on('mouseout', () => {
          //   d3.select(`#child-${child.name.split(' ').join('-')}`).style('text-decoration', 'none')
          // });

        gLineElement
          .append('path')
          .style('stroke', '#505050')
          .attr(
            'd', `
            M ${( width / 6) + margin.left} ${margin.top + (height / this.interest.children.length) * ( 0.5 + index)}
            L ${( width / 4) + margin.left - 10 } ${margin.top + (height / this.interest.children.length) * ( 0.5 + index)}`
          );

        if (index === this.interest.children.length - 1) {
          gLineElement
          .append('path')
          .style('stroke', '#505050')
          .attr(
            'd', `
            M ${( width / 6) + margin.left} ${margin.top + (height / this.interest.children.length) * ( 0.5)}
            L ${( width / 6) + margin.left} ${margin.top + (height / this.interest.children.length) * ( 0.5 + index)}`
          );
        }
      });
    }
  }

  clickedNode(interest: InterestTree) {
    this.interestSelected.emit(interest);
  }

}
