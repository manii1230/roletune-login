import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-live-stream',
  templateUrl: './live-stream.component.html',
  styleUrls: ['./live-stream.component.css']
})
export class LiveStreamComponent implements OnInit {
  options: MediaStreamConstraints;
  @Output() stopStream = new EventEmitter();
  constructor() { }

  ngOnInit() {
    this.options = { video: true, audio: true };
    this.configureStream();
  }

  configureStream() {
    console.log(navigator.mediaDevices);
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices
      .getUserMedia(this.options)
      .then(this.configurationSuccess)
      .catch(this.configurationFailed);
    }
  }

  configurationSuccess(stream: any) {
    const localVideo = document.querySelector(`#local-video`);
    if (localVideo) {
      (localVideo as HTMLVideoElement).srcObject = stream;
      (document.querySelector(`#remote-video`) as HTMLVideoElement).srcObject = stream;
    }
  }

  configurationFailed(err: any) {
    console.log(`Error: `, err);
  }

  toggleStream(flag: boolean) {
    this.stopStream.emit(flag);
  }

}
