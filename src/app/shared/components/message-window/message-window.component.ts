import { MessageWindowService } from './message-window.service';
import { AppService } from './../../../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-message-window',
  templateUrl: './message-window.component.html',
  styleUrls: ['./message-window.component.css']
})
export class MessageWindowComponent implements OnInit {
  @Output() messageOperation = new EventEmitter();
  user: any;
  message: any;
  replyForm: FormGroup;
  compose: boolean;
  messageCopy: any;
  userMessageAction: boolean;

  // tslint:disable-next-line:max-line-length
  constructor(
    private appService: AppService,
    private formBuilder: FormBuilder,
    private messagesService: MessageWindowService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.userMessageAction = false;
    this.message = null;
    this.messageCopy = null;
    this.replyForm = this.formBuilder.group({
      receiverIds: ['', Validators.required],
      cc: [''],
      senderCategory: ['sent'],
      receiverCategory: ['inbox'],
      subject: [''],
      message: [''],
      attachment: ['']
    });

    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        if (this.route.snapshot.params.messageId) {
          this.messagesService.fetchMessageDetails(this.route.snapshot.params.messageId).subscribe(
            (res: any) => {
              if (res.success) {
                this.message = res.result;
                this.userMessageAction = false;
                this.setReplyForm(this.message);
              }
            },
            (err) => {

          });
        } else {
          this.messagesService.activeMessageStatus.subscribe(message => {
            if (message && Object.keys(message).length) {
              this.message = message;
              this.userMessageAction = false;
              this.setReplyForm(this.message);
            }
          });
        }
      }
    });
  }

  get messageField() {
    return this.replyForm.get('message');
  }

  getAttachmentLink() {
    return this.replyForm.value.attachment;
  }

  setReplyForm(message) {
    this.replyForm.patchValue({
      cc: message.cc,
      receiverIds: message.userId,
      subject: message.message.subject,
      message: message.message.body,
      attachment: message.attachment
    });
  }

  messageAction(action: string) {
    if (this.userMessageAction) {
      this.message = JSON.parse(JSON.stringify(this.messageCopy));
    } else {
      this.userMessageAction = true;
      this.messageCopy = JSON.parse(JSON.stringify(this.message));
    }
    const message = this.message;
    const date = new Date();
    switch (action) {
      case 'reply':
        // tslint:disable-next-line:max-line-length
        message.message.createdAt = `${date.getDate()}-${(date.getMonth() + 1 > 9) ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)}-${date.getFullYear()}::${(date.getHours() > 9) ? date.getHours() : '0' + date.getHours()}:${(date.getMinutes() > 9) ? date.getMinutes() : '0' + date.getMinutes()}:${(date.getSeconds() > 9) ? date.getSeconds() : '0' + date.getSeconds()}`;
        // tslint:disable-next-line:max-line-length
        message.message.subject = (message.message.subject.indexOf('Re:') >= 0) ? message.message.subject : 'Re: ' + message.message.subject;
        // tslint:disable-next-line:max-line-length
        message.message.body = this.generateMessage(action, message);
        message.receiverIds = message.userId;
        message.cc = '',
        message.userId = message.message.fromId,
        this.message = message;
        this.setReplyForm(this.message);
        break;
      case 'reply-all':
        // tslint:disable-next-line:max-line-length
        message.message.createdAt = `${date.getDate()}-${(date.getMonth() + 1 > 9) ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)}-${date.getFullYear()}::${(date.getHours() > 9) ? date.getHours() : '0' + date.getHours()}:${(date.getMinutes() > 9) ? date.getMinutes() : '0' + date.getMinutes()}:${(date.getSeconds() > 9) ? date.getSeconds() : '0' + date.getSeconds()}`;
        // tslint:disable-next-line:max-line-length
        message.message.subject = (message.message.subject.indexOf('Re:') >= 0) ? message.message.subject : 'Re: ' + message.message.subject;
        // tslint:disable-next-line:max-line-length
        message.message.body = this.generateMessage(action, message);
        this.message = message;
        this.setReplyForm(this.message);
        break;
      case 'forward':
          // tslint:disable-next-line:max-line-length
          message.message.createdAt = `${date.getDate()}-${(date.getMonth() + 1 > 9) ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)}-${date.getFullYear()}::${(date.getHours() > 9) ? date.getHours() : '0' + date.getHours()}:${(date.getMinutes() > 9) ? date.getMinutes() : '0' + date.getMinutes()}:${(date.getSeconds() > 9) ? date.getSeconds() : '0' + date.getSeconds()}`;
          // tslint:disable-next-line:max-line-length
          message.message.subject = (message.message.subject.indexOf('Re:') >= 0) ? message.message.subject : 'Re: ' + message.message.subject;
          // tslint:disable-next-line:max-line-length
          message.message.body = this.generateMessage(action, message);
          message.receiverIds = '';
          message.cc = '',
          message.userId = '',
          this.message = message;
          this.setReplyForm(this.message);
          break;
      case 'delete':
        this.messageOperation.emit({ message, action });
    }
  }

  replyToMessage() {
    const payload = Object.assign({}, this.replyForm.value);
    // tslint:disable-next-line:max-line-length
    payload.receiverIds = payload.receiverIds.split(',').map(receiver => receiver.trim()).concat(payload.cc.split(',').map(ccItem => ccItem.trim()));
    this.messagesService.sendMessage(payload).subscribe(
      (res: any) => {
      // this.appService.showToast('SUCCESS', res.message, 'success');
        this.router.navigate(['main/messages/sent']);
      },
      (err) => {
      //  this.appService.showToast('ERROR', err.errorMessage, 'error');
      }
    );
  }

  generateMessage(type: string, message: any): string {
    switch (type) {
      case 'reply':
      case 'reply-all':
        // tslint:disable-next-line:max-line-length
        return `\n\n-------------------Reply To Message------------------\nOn ${message.message.createdAt.split('::')[0]} ${message.message.createdAt.split('::')[1]}\n\n${message.userId} wrote:\n${message.message.body}`;
      case 'forward':
        // tslint:disable-next-line:max-line-length
        return `\n\n-------------------Forwarded Message------------------\n${message.message.createdAt.split('::')[0]} ${message.message.createdAt.split('::')[1]}\n\n${message.userId} wrote:\n\n${message.message.body}`;
    }
  }

  getMessage(event) {
    console.log(event);
  }
}
