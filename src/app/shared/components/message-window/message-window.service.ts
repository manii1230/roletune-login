import { environment } from './../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageWindowService {
  private activeMessage: BehaviorSubject<any>;
  public activeMessageStatus: Observable<any>;

  constructor(private http: HttpClient) {
    this.activeMessage = new BehaviorSubject({});

    this.activeMessageStatus = this.activeMessage.asObservable();
  }

  updateActiveMessage(message: any) {
    this.activeMessage.next(message);
  }

  fetchMessages() {
    return this.http.get(`${environment.API_URL}internet-service/internet-user`);
  }

  fetchMessageDetails(id: string) {
    return this.http.get(`${environment.API_URL}internet-service/internet-user/${id}`);
  }

  sendMessage(message: any) {
    if (message.attachFile) {
      const blogFormData = new FormData();
      for (const key in message) {
        if (message.hasOwnProperty(key)) {
          blogFormData.append( key, message[key]);
        }
      }
      message.receiverIds = message.receiverIds.join(',');
      message = blogFormData;
    }
    return this.http.post(`${environment.API_URL}internet-service/internet-user`, message);
  }

  updateMessage(message: any) {
    return this.http.patch(`${environment.API_URL}internet-service/internet-user`, message);
  }

  deleteMessage(message: any) {
    return this.http.patch(`${environment.API_URL}internet-service/internet-user`, message);
  }
}
