import { Router } from '@angular/router';
import { RoleTypes } from '../../../core/models/role-types.model';
import { FrontService } from '../../../features/front.service';
import { AppService } from '../../../app.service';
import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showMenu: boolean;
  showSidebar: boolean;
  user: any;
  isModerator: boolean;
  isSiteAdmin: boolean;

  constructor(
    private appService: AppService,
    private cookieService: CookieService,
    private frontService: FrontService,
    private router: Router
  ) { }

  ngOnInit() {
    this.showMenu = false;
    this.showSidebar = false;
    this.isModerator = false;
    this.isSiteAdmin = false;
    this.appService.activeLoggedInUser.subscribe(user => {
      this.user = user && Object.keys(user).length ? user : null;
      this.isModerator = this.user && this.user.roles && this.user.roles.includes(RoleTypes.siteModerator);
      this.isSiteAdmin = this.user && this.user.roles && this.user.roles.includes(RoleTypes.siteAdmin);
    });
  }

  logout(): void {
    this.router.navigate(['/auth/login']);
    this.cookieService.delete('token');
    this.frontService.emitAction('logout-user', this.user);
    this.appService.updateStatus(false);
    this.appService.updateUser(null);
  }
}
