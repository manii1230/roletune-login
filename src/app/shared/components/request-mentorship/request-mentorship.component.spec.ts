import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestMentorshipComponent } from './request-mentorship.component';

describe('RequestMentorshipComponent', () => {
  let component: RequestMentorshipComponent;
  let fixture: ComponentFixture<RequestMentorshipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestMentorshipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestMentorshipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
