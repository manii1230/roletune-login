import { AppService } from './../../../app.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-request-mentorship',
  templateUrl: './request-mentorship.component.html',
  styleUrls: ['./request-mentorship.component.css']
})
export class RequestMentorshipComponent implements OnInit {

  @Input('mentor') mentor;
  @Output() hideForm = new EventEmitter();

  mentorshipForm: FormGroup;

  constructor(private appService: AppService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.mentorshipForm = this.formBuilder.group({
      topics: ['', Validators.required],
      duration: [null, Validators.required],
      startDate: [new Date(), Validators.required],
      additionalInfo: ['']
    });
  }

  toggleForm(response: any) {
    this.hideForm.emit(response);
  }

  requestMentorship() {
    const payload: any = {};
    payload.toUserId = this.mentor.userId;
    payload.communityId = this.mentor.communityId;
    payload.programDetails = {
      topics: this.mentorshipForm.value.topics,
      duration: this.mentorshipForm.value.duration,
      startDate: this.mentorshipForm.value.startDate,
      additionalInfo: this.mentorshipForm.value.additionalInfo
    };
    payload.mileStones = [];
    payload.message = [];
    this.appService.sendMentorshipRequest(payload).subscribe(
      (res: any) => {
        if (res.success && res.result) {
          this.toggleForm(res.result);
        }
      },
      (err) => {

      }
    );
  }

  getMinDate() {
    return new Date();
  }
}
