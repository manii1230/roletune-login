import { Router, NavigationEnd } from '@angular/router';
import { AppService } from './../../../app.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

export enum SidebarTypes {
  general = 'GENERAL',
  interest = 'INTEREST',
  mentorship = 'MENTORSHIP',
  profile = 'PROFILE',
  settings = 'SETTINGS'
};

@Component({
  selector: 'app-main-sidebar',
  templateUrl: './main-sidebar.component.html',
  styleUrls: ['./main-sidebar.component.css']
})
export class MainSidebarComponent implements OnInit, OnDestroy {
  user: any;
  showInterestSidebar: boolean;
  communities: any[];
  sidebarTypes = SidebarTypes;
  sidebarType: SidebarTypes;
  activeIndex: number;
  subscriptions: any[];
  constructor(private appService: AppService, private router: Router) { }

  ngOnInit() {
    this.communities = [];
    this.showInterestSidebar = false;
    this.subscriptions = [];
    this.appService.activeLoggedInUser.subscribe(user => {
      if (user && Object.keys(user).length) {
        this.user = user;
        this.setActiveSidebar();
        this.appService.communityListObservable.subscribe(list => {
          if (list && list.length) {
            this.communities = list.filter(item => item.status === 'ACTIVE');
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  setActiveSidebar() {
    if (this.router.url.indexOf('interest') >= 0) {
      this.sidebarType = SidebarTypes.interest;
      return;
    }

    if (this.router.url.indexOf('mentorship') >= 0) {
      this.sidebarType = SidebarTypes.mentorship;
      return;
    }

    if (this.router.url.indexOf('settings') >= 0) {
      this.sidebarType = SidebarTypes.settings;
      return;
    }

    if (this.router.url.indexOf('profile') >= 0) {
      this.activeIndex =
        (this.router.url.indexOf('manage') >= 0 || this.router.url.indexOf('find-friends') >= 0)
          ? 2 : ((this.router.url.indexOf('showcase') >= 0) ? 1 : null);
      this.checkActiveMenu();
      this.sidebarType = SidebarTypes.profile;
      return;
    }

    this.sidebarType = SidebarTypes.general;
  }

  navigateToMessages() {
    const urlParts = this.router.url.split('/').slice(0, 4);
    urlParts[2] = 'message-details';
    const link = `#${urlParts.join('/')}`;
    if (this.user && this.user.messageSettings && this.user.messageSettings.openIn) {
      switch (this.user.messageSettings.openIn) {
        case 'T':
          window.open(link);
          break;
        case 'W':
          window.open(link, '_blank', `height=${window.screen.height},width=${window.screen.width},toolbar=0,location=0,menubar=0`);
          break;
        case 'H':
        default:
          this.router.navigate(['main/messages']);
      }
    } else {
      this.router.navigate(['main/messages']);
    }
  }

  checkActiveMenu() {
    const routerEventsSubscription = this.router.events.subscribe(route => {
      if (route instanceof NavigationEnd) {
        if (route.url.indexOf('showcase') >= 0) {
          this.activeIndex = 1;
        } else if ((route.url.indexOf('manage') >= 0) || (route.url.indexOf('find-friends') >= 0)) {
          this.activeIndex = 2;
        } else {
          this.activeIndex = null;
        }
      }
    });
    this.subscriptions.push(routerEventsSubscription);
  }

  activateDropdown(idx: number): void {
    this.activeIndex = (this.activeIndex === idx) ? null : idx;
  }
}
