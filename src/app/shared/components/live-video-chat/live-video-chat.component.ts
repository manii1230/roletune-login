import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, AfterContentInit } from '@angular/core';

@Component({
  selector: 'app-live-video-chat',
  templateUrl: './live-video-chat.component.html',
  styleUrls: ['./live-video-chat.component.css']
})
export class LiveVideoChatComponent implements OnInit, AfterContentInit {
  @Input() videoChatFriend: any;
  @Output() stopStream: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
    const videoCameras = this.getConnectedDevices('videoinput');
    console.log('Cameras found:', videoCameras);
  }

  ngAfterContentInit() {
    // const configuration = {'iceServers': [{'urls': 'stun:stun.l.google.com:19302'}]};
    // const peerConnection = new RTCPeerConnection(configuration);
    // navigator.mediaDevices.getUserMedia({
    //   video: true, audio: true
    // }).then((strem: MediaStream) => {
    //   //peerConnection.addTrack(strem)
    // })
    setTimeout(() => {
      this.getMedia();
    }, 1000);
  }

  async getConnectedDevices(type) {
    const devices = await navigator.mediaDevices.enumerateDevices();
    return devices.filter(device => device.kind === type)
}


  async getMedia() {
    let stream = null;
    try {
      const constraints = {
        audio: {
          echoCancellation: true
        },
        video:true
      };
      stream = await navigator.mediaDevices.getUserMedia(constraints);
      /* use the stream */
      this.videoChatFriend.stream = stream;
      const video = (document.querySelector('#local-video') as HTMLVideoElement);
      console.log(video)
      video.srcObject = stream;
      (document.querySelector('#remote-video') as HTMLVideoElement).srcObject = stream;
      // video.onloadedmetadata = function(e) {
      //   video.play();
      // };
    } catch(err) {
      /* handle the error */
      this.videoChatFriend.stream = err;
    }
  }

  toggleStream(flag: boolean) {
    this.stopStream.emit(flag);
    (document.querySelector('.topbar') as HTMLDivElement).style.display = 'block';
  }

}
