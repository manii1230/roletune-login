import { LoaderTypes } from './../../models/loader-types.enum';
import { LoaderConfig } from './../../config/loader.config';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
  @Input() loaderType: LoaderTypes = LoaderTypes.large;
  constructor() { }

  ngOnInit() {
  }

  getStyles() {
    switch (this.loaderType) {
      case LoaderTypes.xsmall:
        return LoaderConfig[LoaderTypes.xsmall];
      case LoaderTypes.small:
        return LoaderConfig[LoaderTypes.small];
      case LoaderTypes.medium:
        return LoaderConfig[LoaderTypes.medium];
      case LoaderTypes.large:
        return LoaderConfig[LoaderTypes.large];
    }
  }
}
