import { HttpErrorResponse } from '@angular/common/http';
import { MessageTypes } from './../../../core/models/message-types';
import { ChatActionTypes } from './../../../core/models/chat-action-types.model';
import { Router } from '@angular/router';
import { FrontService } from './../../../features/front.service';
import { AppService } from './../../../app.service';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-chat-box',
  templateUrl: './chat-box.component.html',
  styleUrls: ['./chat-box.component.css']
})
export class ChatBoxComponent implements OnInit, OnDestroy {
  @Input() activeFriend: any;
  @Input() user: any;
  @Output() removeActiveFriend: EventEmitter<any> = new EventEmitter<any>();
  friendsChatForm: FormGroup;
  unsubscribe = new Subject();

  constructor(
    private appService: AppService,
    private frontService: FrontService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.friendsChatForm = this.formBuilder.group({
      message: ['', [Validators.required, Validators.minLength(1)]],
      fileName: [''],
      filePath: [''],
      attachedFile: [null]
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  checkMessageFile() {
    if (this.friendsChatForm.value.attachedFile) {
      this.frontService.uploadFile(this.friendsChatForm.value).subscribe(
        (res: any) => {
          if (res.result) {
            this.friendsChatForm.patchValue({
              fileName: res.result.fileName,
              filePath: res.result.filePath,
            });
          }
          return this.sendChatMessage();
        },
        (err: HttpErrorResponse) => {
          return this.activeFriend.message = {
            status: false,
            message: err.message
          };
        }
      );
      return;
    }
    this.sendChatMessage();
  }

  sendChatMessage() {
    if (this.friendsChatForm.valid) {
      const payload = {
        fromUserId: this.user.username,
        toUserId: this.activeFriend.username,
        message: this.friendsChatForm.value,
        fileName: this.friendsChatForm.value.fileName,
        filePath: this.friendsChatForm.value.filePath
      };
      this.frontService.emitAction('new-chat-message', payload);
      const today = new Date();
      this.activeFriend.messages.push({
        fromUserId: this.user.username,
        toUserId: this.activeFriend.username,
        message: this.friendsChatForm.value.message,
        fileName: this.friendsChatForm.value.fileName,
        filePath: this.friendsChatForm.value.filePath,
        sentAt:
        // tslint:disable-next-line: max-line-length
        `${(today.getDate() < 9) ? '0' : ''}${(today.getDate())}-${((today.getMonth() + 1) < 9) ? '0' : ''}${(today.getMonth() + 1)}-${today.getFullYear()}::${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}::${today.getHours() > 12 ? 'PM' : 'AM'}`
      });
      this.scrollChat();
      this.friendsChatForm.reset();
    }
  }

  // getOnlineStatus(friend: any): boolean {
  //   return (this.user.username === friend.toUserId) ?
  //     friend.fromUserId_user_docs.status :
  //     friend.toUserId_user_docs.status;
  // }

  nudgeFriend(friend: any) {
    const friendDetails = (this.user.username === friend.fromUserId_user_docs.username) ?
      friend.toUserId_user_docs :
      friend.fromUserId_user_docs;
    const payload = {
      toUserId: friendDetails.username,
      fromUserId: this.user.username,
      fromUserFirstName: this.user.firstName,
      fromUserLastName: this.user.lastName,
      alertType: 'NUDGE'
    };
    this.frontService.emitAction('nudge-user', payload);
  }

  navigateScreen(blogType: string) {
    this.router.navigate([`main/home/${this.user.username}/${blogType}`]);
  }

  scrollChat() {
    setTimeout(() => {
      const chatListTags = document.querySelectorAll('.chat-list > ul');
      chatListTags.forEach(chatListTag => {
        chatListTag.scrollTop = chatListTag.scrollHeight;
      });
    }, 0);
  }

  getSentDate(dateString: string) {
    const dateParts = dateString.substring(0, 10).split('-');
    return new Date(`${dateParts[1]}-${dateParts[0]}-${dateParts[2]}`);
  }

  getSenderDetails(message: any, prop: string): string {
    if (message.fromUserId === this.user.username) {
      switch (prop) {
        case 'IMAGE':
          return this.user.profileImage ? this.user.profileImage : `assets/images/resources/user.jpg`;
        case 'USERNAME':
          return this.user.username;
        case 'NAME':
          return this.user.firstName ? `${this.user.firstName} ${this.user.lastName}` : `Name`;
      }
    }

    switch (prop) {
      case 'IMAGE':
        return this.activeFriend.profileImage ? this.activeFriend.profileImage : `assets/images/resources/user.jpg`;
      case 'USERNAME':
        return this.activeFriend.username;
      case 'NAME':
        return this.activeFriend.firstName ? `${this.activeFriend.firstName} ${this.activeFriend.lastName}` : `Name`;
    }
  }

  appendEmoji(activeFriend, emojiCode: number) {
    activeFriend.showIcons = !activeFriend.showIcons;
    this.friendsChatForm.patchValue({
      message: `${this.friendsChatForm.value.message ? this.friendsChatForm.value.message + ' ' : ''}${String.fromCodePoint(emojiCode)}`
    });
  }

  chatAction(action: ChatActionTypes) {
    const payload = {
      username: this.activeFriend.username,
      action
    };

    this.frontService.performChatMessageAction(payload).subscribe(
      (res: any) => {
        this.activeFriend.message = {
          status: res.success,
          message: res.success ? res.message : res.message.errorMessage
        };
        if (res.success) {
          switch (action) {
            case ChatActionTypes.archieve:
            case ChatActionTypes.delete:
              this.activeFriend.messages = [];
              break;
            case ChatActionTypes.mute:
              this.user.muted = (this.user.muted && this.user.muted) ?
                this.user.muted.concat(this.activeFriend.username) :
                [this.activeFriend.username];
              this.appService.updateUser(this.user);
              break;
            case ChatActionTypes.report:
              /**
               * Action to be performed after a user reports a chat
               */
              break;
          }
          this.activeFriend.showDropdown = false;
        }
      },
      (err: HttpErrorResponse) => {
        this.activeFriend.message = {
          status: false,
          message: err.error && err.error.message ? err.error.message : err.message 
        };
      }
    );
  }

  removeFriend(activeFriend: any): void {
    this.removeActiveFriend.emit(activeFriend);
  }

  uploadFile(event: any): void {
    this.activeFriend.message = null;
    if (event.target && event.target.files.length) {
      const [fileToLoad] = event.target.files;
      if ((fileToLoad.size / 1024) < 10000) {
        this.friendsChatForm.patchValue({
          fileName: fileToLoad.name,
          attachedFile: fileToLoad
        });
      } else {
        this.activeFriend.message = {
          status: false,
          message: MessageTypes.chatFileSizeFailed
        };
      }
    }
  }
}
