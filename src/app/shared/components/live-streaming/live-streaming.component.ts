import { Component, OnInit } from '@angular/core';
let that: any;

@Component({
  selector: 'app-live-streaming',
  templateUrl: './live-streaming.component.html',
  styleUrls: ['./live-streaming.component.css']
})
export class LiveStreamingComponent implements OnInit {
  video: HTMLVideoElement;
  navigator: Navigator;
  constructor() { }

  ngOnInit() {
    that = this;
    this.navigator = navigator;

    const canvas = (document.getElementById('preview') as HTMLCanvasElement);
    const context = canvas.getContext('2d');

    canvas.width = 900;
    canvas.height = 700;

    // context. = canvas.width;
    // context.height = canvas.height;

    this.video = (document.getElementById('video') as HTMLVideoElement);

    this.navigator.getUserMedia = ( navigator.getUserMedia );
    //|| navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msgGetUserMedia );

    if (navigator.getUserMedia) {
        navigator.getUserMedia({video: true, audio: false}, this.loadCamera, this.loadFail);
    }

    setInterval(() => {
      this.viewVideo(this.video, context);
    }, 5);
  }

  logger(message: string) {
    document.querySelector('#logger').textContent += message;
  }

  loadCamera(stream: any) {
    try {
      that.video.srcObject = stream;
    } catch (error) {
      that.video.src = window.URL.createObjectURL(stream);
    }
    this.logger('Camera connected');
  }

  loadFail() {
    this.logger('Camera not connected');
  }

  viewVideo(video: any, context: any) {
    context.drawImage(video, 0, 0, context.width, context.height);
    // socket.emit('stream',canvas.toDataURL('image/webp'));
  }

  toggleStream(flag: boolean) {

  }
}
