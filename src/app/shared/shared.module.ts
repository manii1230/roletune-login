import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  ChatBoxComponent,
  EmptyLayoutComponent,
  ErrorLayoutComponent,
  FooterComponent,
  HeaderComponent,
  InterestTreeComponent,
  LiveStreamComponent,
  LiveStreamingComponent,
  LiveVideoChatComponent,
  MainLayoutComponent,
  MainSidebarComponent,
  MentorshipProgressGraphComponent,
  MessageWindowComponent,
  ProfileLayoutComponent,
  RequestMentorshipComponent,
  UserTimelineComponent,
  LoaderComponent
} from './components/';
import {
  DisableRightClickDirective,
  ExcerptDirective,
  LoadMoreDirective,
  MentorshipTreeDirective
} from './directives/';
import {
  FilterFriendsPipe,
  FilterListPipe,
  MomentDatePipe,
  SortListPipe,
  UrlSanitizerPipe
} from './pipes/';
import { MessageWindowService } from './components/message-window/message-window.service';

@NgModule({
  declarations: [
    EmptyLayoutComponent,
    ErrorLayoutComponent,
    MainLayoutComponent,
    ProfileLayoutComponent,
    FilterListPipe,
    SortListPipe,
    UrlSanitizerPipe,
    MomentDatePipe,
    FilterFriendsPipe,
    ExcerptDirective,
    LoadMoreDirective,
    MentorshipTreeDirective,
    MessageWindowComponent,
    RequestMentorshipComponent,
    MentorshipProgressGraphComponent,
    LiveStreamComponent,
    LiveStreamingComponent,
    InterestTreeComponent,
    ChatBoxComponent,
    ProfileLayoutComponent,
    MainSidebarComponent,
    HeaderComponent,
    FooterComponent,
    LiveVideoChatComponent,
    DisableRightClickDirective,
    UserTimelineComponent,
    LoaderComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule
  ],
  providers: [
    MessageWindowService
  ],
  exports: [
    EmptyLayoutComponent,
    ErrorLayoutComponent,
    MainLayoutComponent,
    ProfileLayoutComponent,
    MessageWindowComponent,
    FilterListPipe,
    SortListPipe,
    UrlSanitizerPipe,
    MomentDatePipe,
    FilterFriendsPipe,
    ExcerptDirective,
    LoadMoreDirective,
    MentorshipTreeDirective,
    RequestMentorshipComponent,
    MentorshipProgressGraphComponent,
    LiveStreamComponent,
    LiveStreamingComponent,
    InterestTreeComponent,
    ChatBoxComponent,
    ProfileLayoutComponent,
    MainSidebarComponent,
    HeaderComponent,
    FooterComponent,
    LiveVideoChatComponent,
    DisableRightClickDirective,
    UserTimelineComponent,
    LoaderComponent
  ]
})
export class SharedModule { }
