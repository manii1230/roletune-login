import { LoaderTypes } from './../models/loader-types.enum';
export const LoaderConfig = {
  [LoaderTypes.xsmall] : {
    width: LoaderTypes.xsmall,
    height: LoaderTypes.xsmall
  },
  [LoaderTypes.small] : {
    width: LoaderTypes.small,
    height: LoaderTypes.small
  },
  [LoaderTypes.medium] : {
    width: LoaderTypes.medium,
    height: LoaderTypes.medium
  },
  [LoaderTypes.large] : {
    width: LoaderTypes.large,
    height: LoaderTypes.large
  }
};

