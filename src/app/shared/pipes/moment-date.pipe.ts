import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'momentDate'
})
export class MomentDatePipe implements PipeTransform {

  transform(date: string, args?: any): any {
    if (!date) {
      return '';
    }
    let dateParts = null;
    if (args) {
      dateParts = date.substring(0, 10).split('-').concat(date.substring(12).split(':')).map(item => parseInt(item, 10));
      return new Date(`${(dateParts[1] - 1 < 10) ? '0' : '' }${dateParts[1]}/${dateParts[2]}/${dateParts[0]}`);
    }
    dateParts = date.substring(0, 10).split('-').concat(date.substring(12).split(':')).map(item => parseInt(item, 10));
    // tslint:disable-next-line:max-line-length
    return (dateParts[3].toString() !== 'undefined' || dateParts[3].toString() !== 'null') ?
      new Date(dateParts[2], dateParts[1] - 1, dateParts[0], dateParts[3], dateParts[4], dateParts[5]) :
      new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
  }

}
