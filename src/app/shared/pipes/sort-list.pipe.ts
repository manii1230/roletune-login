import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortList'
})
export class SortListPipe implements PipeTransform {

  transform(list: any, sortProperty: string, sortOrder: boolean): any {
    if (!list || !list.length) {
      return [];
    }

    if (!sortProperty) {
      return list;
    }

    // tslint:disable-next-line:max-line-length
    return  sortOrder ? list.sort((item1, item2) => item1[sortProperty] - item2[sortProperty]) : list.sort((item1, item2) => item2[sortProperty] - item1[sortProperty]);
  }

}
