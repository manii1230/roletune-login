import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterFriends'
})
export class FilterFriendsPipe implements PipeTransform {

  transform(list: any[], filterValue: string, filterKeys: string[], username:string): any {
    
    if (!list) {
      return [];
    }

    if (!filterValue) {
      return list;
    }

    const filteredList: any[] = [];
    list.map(item => {
      if (item.fromUserId_user_docs && item.toUserId_user_docs) {
        item = (item.fromUserId_user_docs.username === username) ? 
        Object.assign(item, item.toUserId_user_docs ? item.toUserId_user_docs : {}) :
        Object.assign(item, item.fromUserId_user_docs ? item.fromUserId_user_docs : {});
        if (
          filterKeys.some(key => item[key] && (item[key].toString().toLowerCase().indexOf(filterValue.toLowerCase()) > -1 ))
        ) {
          filteredList.push(item);
        }
      } else {
        if (
          filterKeys.some(key => item[key] && (item[key].toString().toLowerCase().indexOf(filterValue.toLowerCase()) > -1 ))
        ) {
          filteredList.push(item);
        }
      }
    });
    return filteredList;
  }
}
