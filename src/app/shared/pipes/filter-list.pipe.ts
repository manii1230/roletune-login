import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterList'
})
export class FilterListPipe implements PipeTransform {

  transform(list: any[], filterValue: string, fullMatch?: boolean): any {
    if (!list || !list.length) {
      return [];
    }

    if (!filterValue) {
      return list;
    }

    const filterObj = Object.keys(list[0]).concat(Object.keys(list[0].message ? list[0].message : {}));
    const filteredList: any[] = [];
    list.map(item => {
      item = Object.assign(item, item.message ? item.message : {});
      if (fullMatch) {
        if (filterObj.some(key => item[key] && (item[key].toString().toLowerCase() === filterValue.toLowerCase()))) {
          filteredList.push(item);
        }
      } else {
        if (
          filterObj.some(key => item[key] && (item[key].toString().toLowerCase().indexOf(filterValue.toLowerCase()) > -1 ))
        ) {
          filteredList.push(item);
        }
      }
    });
    return filteredList;
  }
}
