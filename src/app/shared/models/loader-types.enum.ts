export enum LoaderTypes {
  xsmall = '1rem',
  small = '2rem',
  medium = '3rem',
  large = '4rem'
}
