import { environment } from './../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
declare const $: any;

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private isLoggedIn: BehaviorSubject<boolean>;
  private activeCommunity: BehaviorSubject<string>;
  public loggedInUser: BehaviorSubject<any>;

  public loggedInStatus: Observable<boolean>;
  public activeCommunityStatus: Observable<string>;
  public activeLoggedInUser: Observable<any>;

  private interestList: BehaviorSubject<any[]>;
  public interestListObservable: Observable<any>;

  communityList: BehaviorSubject<any[]>;
  public communityListObservable: Observable<any>;

  friendsList: BehaviorSubject<any[]>;
  public friendsListObservable: Observable<any>;

  private groupList: BehaviorSubject<any[]>;
  public groupListObservable: Observable<any>;

  private communityDetails: BehaviorSubject<any>;
  public communityDetailsObservable: Observable<any>;

  private userCommunityList: BehaviorSubject<any[]>;
  public userCommunityListObservable: Observable<any>;

  locations: string[];
  pageSize: number;

  userDetails: any;

  constructor(private http: HttpClient) {
    this.locations = [
      'Andaman and Nicobar Islands', 'Andhra Pradesh', 'Arunachal Pradesh', 'Assam',
      'Bihar', 'Chandigarh', 'Chhattisgarh', 'Dadra and Nagar Haveli', 'Daman and Diu',
      'Delhi', 'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jammu and Kashmir',
      'Jammu and Kashmir', 'Jharkhand', 'Karnataka', 'Kerala', 'Ladakh', 'Lakshadweep',
      'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Meghalaya', 'Mizoram', 'Nagaland',
      'Odisha', 'Puducherry', 'Punjab', 'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana',
      'Tripura', 'Uttar Pradesh', 'Uttarakhand', 'West Bengal'
    ];
    this.pageSize = environment.PAGE_SIZE;

    this.isLoggedIn = new BehaviorSubject(false);
    this.activeCommunity = new BehaviorSubject('');
    this.loggedInUser = new BehaviorSubject({});

    this.loggedInStatus = this.isLoggedIn.asObservable();
    this.activeCommunityStatus = this.activeCommunity.asObservable();
    this.activeLoggedInUser = this.loggedInUser.asObservable();

    this.interestList = new BehaviorSubject([]);
    this.interestListObservable = this.interestList.asObservable();

    this.communityList = new BehaviorSubject([]);
    this.communityListObservable = this.communityList.asObservable();

    this.friendsList = new BehaviorSubject([]);
    this.friendsListObservable = this.friendsList.asObservable();

    this.groupList = new BehaviorSubject([]);
    this.groupListObservable = this.groupList.asObservable();

    this.communityDetails = new BehaviorSubject({});
    this.communityDetailsObservable = this.communityDetails.asObservable();

    this.userCommunityList = new BehaviorSubject([]);
    this.userCommunityListObservable = this.userCommunityList.asObservable();
  }

  updateStatus(status: boolean) {
    this.isLoggedIn.next(status);
  }

  getCommunityList(userId?: string) {
    let params = new HttpParams();
    if (userId) {
      params = params.set('userId', userId);
      params = params.set('userDetails', 'true');
    }
    return this.http.get(`${environment.API_URL}community-service/communities/user`, { params });
  }

  updateCommunity(community: string) {
    this.activeCommunity.next(community);
  }

  updateUser(user: any) {
    this.setLoggedInUserDetails(user);
    this.loggedInUser.next(user);
  }

  getLocations() {
    return this.locations;
  }

  removeMessage(obj: any, prop?: string) {
    setTimeout(() => {
      prop ? obj[prop] = null : obj = null;
    }, 3000);
  }

  updateInterestListObservable(interests: any[]) {
    this.interestList.next(interests);
  }

  updateCommunityListObservable(communities: any[] = []) {
    if (communities.length) {
      communities = communities.map(community => {
        community.role = this.getRoleName(community.role);
        return community;
      });
    }
    this.communityList.next(communities);
  }

  updateUserCommunityListObservable(communities: any[] = []) {
    this.userCommunityList.next(communities);
  }

  updateFriendListObservable(friendsList: any[]) {
    this.friendsList.next(friendsList);
  }

  updateGroupListObservable(groupList: any[]) {
    this.groupList.next(groupList);
  }

  updateCommunityDetailsObservable(community: any) {
    this.communityDetails.next(community);
  }

  getUserDetails() {
    return this.http.get(`${environment.API_URL}user-service/user-details`);
  }

  getFriendsList(username?: string) {
    if (username) {
      return this.http.get(`${environment.API_URL}user-service/friend-request-list/?username=${username}`);
    } else {
      return this.http.get(`${environment.API_URL}user-service/friend-request-list`);
    }
  }

  getGroupList() {
    return this.http.get(`${environment.API_URL}user-group-service/user-group`);
  }

  sendMentorshipRequest(payload: any) {
    return this.http.post(`${environment.API_URL}mentorship-service/send-mentorship-request`, payload);
  }

  sendRequest(username: string, requestType?: string) {
    let params = new HttpParams();
    params = params.set('toUserId', username);
    return this.http.get(`${environment.API_URL}user-service/send-friend-request`, { params });
  }

  getRoleName(role = null) {
    switch (role) {
      case 3001:
        return 'Community Admin';
      case 2001:
        return 'Community Moderator';
      case 1001:
      default:
        return 'Community User';
    }
  }

  searchInterest(data: any) {
    const { keyword, matchType, level, pageNumber, pageSize } = data;
    let queryString = '';
    queryString += keyword ? `keyword=${keyword}` : ``;
    queryString += matchType ? `&matchType=${matchType}` : ``;
    queryString += level ? `&level=${level}` : ``;
    queryString += pageSize ? `&pageSize=${pageSize}` : ``;
    queryString += pageNumber ? `&pageNumber=${pageNumber}` : ``;
    return this.http.get(
      `${environment.API_URL}interest-graph-service/search-interest?${queryString}`
    );
  }

  getIconLink(interest: string): string {
    return `${environment.INTEREST_ICON_URL}${interest}.svg`;
  }

  setLoggedInUserDetails(user) {
    this.userDetails = user;
  }
  getLoggedInUserDetails() {
    return this.userDetails;
  }

  getMentorshipTreeColors() {
    return {
      lines: '#505050',
      names: '#505050',
      circles: '#505050'
    };
  }
}
