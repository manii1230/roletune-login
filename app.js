//require("dotenv/config");
const http = require("http");
const path = require("path");
const express = require("express");

let SERVER_PORT = 6020;
const app = express();

app.use(express.static(path.join(__dirname, "dist/RoleTune")));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "dist/RoleTune", "index.html"), err => {
    // Fixing the "cannot GET /URL" error on refresh with React Router and Reach Router
    if (err) {
      res.status(500).send(err);
    }
  });
});

const httpServer = http.createServer(app);

httpServer.listen(SERVER_PORT, err => {
  return err
    ? console.log("app:server", `Failed to start server instance %O`, err)
    : console.log("app:server", `HTTP listening on port ${SERVER_PORT}`);
});
